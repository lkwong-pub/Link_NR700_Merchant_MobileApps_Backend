CREATE DATABASE `gt_cms` /*!40100 DEFAULT CHARACTER SET utf8 */;
grant all on gt_cms.* to gt_cms@localhost identified by 'gt_cms';

CREATE TABLE `gt_cms`.`system_setting` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `value` LONGTEXT NULL,
  PRIMARY KEY (`id`)
  )ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

  CREATE TABLE `gt_cms`.`message_resource` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `locale` VARCHAR(255) NULL,
  `name` VARCHAR(255) NULL,
  `value` VARCHAR(255) NULL,
  PRIMARY KEY (`id`)
  )ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

  CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `role` varchar(16) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `last_login_datetime` datetime DEFAULT NULL,
  `login_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `administrator_username_idx` (`username`),
  KEY `administrator_password_idx` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
 
CREATE TABLE `push_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `last_offset` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `push_status` int(11) DEFAULT NULL,
  `redirect_details` varchar(255) DEFAULT NULL,
  `schedule_time` datetime DEFAULT NULL,
  `target_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

CREATE TABLE `push_message_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `master_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_4e35uj45rucinqd0uva3it5he` (`master_id`),
  CONSTRAINT `FK_4e35uj45rucinqd0uva3it5he` FOREIGN KEY (`master_id`) REFERENCES `push_message` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

CREATE TABLE `push_message_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `android_expect_count` int(11) DEFAULT NULL,
  `android_fail_count` int(11) DEFAULT NULL,
  `android_success_count` int(11) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `ios_expect_count` int(11) DEFAULT NULL,
  `ios_fail_count` int(11) DEFAULT NULL,
  `ios_success_count` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `push_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `device_type` int(11) DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_p543sxuten4xkq3ki4u2nm6q0` (`device_id`),
  UNIQUE KEY `UK_2477xmi30ojbmbumeg73jlehv` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `push_message_token_mapping` (
  `push_message_id` int(11) NOT NULL,
  `push_token_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `FK_5nqn2l0il7x9x8379ov7xwp8d` (`push_token_id`),
  KEY `FK_m96gv8pykx9cd3fb4dmt5c5ga` (`push_message_id`),
  CONSTRAINT `FK_5nqn2l0il7x9x8379ov7xwp8d` FOREIGN KEY (`push_token_id`) REFERENCES `push_token` (`id`),
  CONSTRAINT `FK_m96gv8pykx9cd3fb4dmt5c5ga` FOREIGN KEY (`push_message_id`) REFERENCES `push_message` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Added on 20170306 for audit log table
CREATE TABLE `audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `module_code` varchar(100) DEFAULT NULL,
  `activity_type_id` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Added on 20170308 for activity_type
CREATE TABLE `activity_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Added on 20170425 for permission
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `last_update_time` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `action_type_id` int(11) NOT NULL,
  `module_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;