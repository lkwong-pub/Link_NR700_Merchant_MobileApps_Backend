<!-- System Setting -->
INSERT INTO system_setting (name, value) VALUES ('app.path', '/cms_demo');
INSERT INTO system_setting (name, value) VALUES ('res.path', '/cms_demo');
INSERT INTO system_setting (name, value) VALUES ('project.name', 'GT CMS');
INSERT INTO system_setting (name, value) VALUES ('project.url', 'http://localhost:8080/cms_demo');
INSERT INTO system_setting (name, value) VALUES ('status.active', '0');
INSERT INTO system_setting (name, value) VALUES ('status.inactive', '1');
INSERT INTO system_setting (name, value) VALUES ('status.deleted', '99');
INSERT INTO system_setting (name, value) VALUES ('upload.directory', 'C:\\\\Users\\\\don.cheung\\\\Desktop\\\\apache-tomcat-8.0.30\\\\data');
INSERT INTO system_setting (name, value) VALUES ('support.languages', 'en_US,zh_TW,zh_CN');
INSERT INTO system_setting (name, value) VALUES ('default.language', 'en_US');

<!-- Message Resource -->
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.STATUS', 'Status');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.ACTION', 'Action');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.HOME', 'HOME');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.SAVE', 'Save');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.CANCEL', 'Cancel');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'DEMO.TITLE', 'DEMO');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'DEMO.ADD', 'Add Demo');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'DEMO.UPDATE', 'Update Demo');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'DEMO.DELETE', 'Delete Demo');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.SEARCH', 'Search');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'en_US', 'Eng');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'zh_TW', 'Tch');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'zh_CN', 'Sch');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.STATUS.INACTIVE', 'Changing the record status to inactive?');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.STATUS.ACTIVE', 'Changing the record status to active?');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.CHANGEPASSWORD', 'Change Password');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ADMIN.TITLE', 'ADMIN');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ADMIN.ADD', 'Add Admin');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ADMIN.UPDATE', 'Update Admin');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ADMIN.DELETE', 'Delete Admin');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ADMIN.CHANGEPASSWORD', 'Change Password');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.INACTIVE', 'Inactive');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'GLOBAL.ACTIVE', 'Active');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ROLE.TITLE', 'ROLE');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ROLE.ADD', 'Add Role');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ROLE.UPDATE', 'Update Role');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ROLE.DELETE', 'Delete Role');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'ROLE.PERMISSION', 'Role Permission');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PERMISSION.TITLE', 'PERMISSION');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PERMISSION.ADD', 'Add Permission');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PERMISSION.UPDATE', 'Update Permission');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PERMISSION.DELETE', 'Delete Permission');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PUSHMESSAGE.TITLE', 'PUSHMESSAGE');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PUSHMESSAGE.ADD', 'Add Push Message');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PUSHMESSAGE.UPDATE', 'Update Push Message');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PUSHMESSAGE.DELETE', 'Delete Push Message');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'VERSION.TITLE', 'VERSION');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'VERSION.ADD', 'Add Version');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'VERSION.UPDATE', 'Update Version');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'VERSION.DELETE', 'Delete Version');

<!-- Admin -->
INSERT INTO admin
(id, create_datetime, last_login_datetime, login_count, name, password, status, type, username)
VALUES
(1, '2015-09-29 00:00:00', null, '0', 'superadmin', '202cb962ac59075b964b07152d234b70', '0', 'ROLE_SUPERADMIN', 'superadmin');

<!-- Role -->
INSERT INTO role (id, create_time, last_update_time, name, status, code, description)
VALUES
(1, '2015-09-29 00:00:00', '2015-09-29 00:00:00', 'Test Role', '0', 'R_TEST', 'Testing ROLE Record');

INSERT INTO admin_role_mapping (admin_id, role_id)
VALUES (1, 1);

<!-- Permission -->
INSERT INTO permission (id, create_time, last_update_time, name, status, code, description, link, action_type_id, module_type_id )
VALUES
(1,'2016-02-01 00:00:00','2016-02-24 21:48:31','Demo Module View',0,'PERMISSION_DEMO_VIEW','Demo Module View','/demo/list',1,4),
(2,'2016-02-23 23:57:23','2016-02-23 23:57:23','Demo Module Add',0,'PERMISSION_DEMO_ADD','Demo Module Add','/demo/',2,4),
(3,'2016-02-23 23:58:33','2016-02-23 23:58:33','Demo Module Edit',0,'PERMISSION_DEMO_EDIT','Demo Module Edit','/demo/{\\\\d+}',3,4),
(4,'2016-02-24 00:01:07','2016-02-24 00:01:07','Demo Module Delete',0,'PERMISSION_DEMO_DELETE','Demo Module Delete','/demo/',4,4),
(5,'2016-02-01 00:00:00','2016-02-23 23:43:28','User Module View',0,'PERMISSION_ADMIN_VIEW','Admin Module View','/admin/list',1,1),
(6,'2016-02-23 23:57:23','2016-02-23 23:57:23','User Module Add',0,'PERMISSION_ADMIN_ADD','Admin Module Add','/admin/',2,1),
(7,'2016-02-23 23:58:33','2016-02-23 23:58:33','User Module Edit',0,'PERMISSION_ADMIN_EDIT','Admin Module Edit','/admin/{\\\\d+}',3,1),
(8,'2016-02-24 00:01:07','2016-02-24 00:01:07','User Module Delete',0,'PERMISSION_ADMIN_DELETE','Admin Module Delete','/admin/',4,1),
(9,'2016-02-24 00:02:28','2016-02-24 00:02:28','Role Module View',0,'PERMISSION_ROLE_VIEW','Role Module View','/role/list',1,2),
(10,'2016-02-24 00:03:01','2016-02-24 00:03:01','Role Module Add',0,'PERMISSION_ROLE_ADD','Role Module Add','/role/',2,2),
(11,'2016-02-24 00:03:26','2016-02-24 00:03:26','Role Module Edit',0,'PERMISSION_ROLE_EDIT','Role Module Edit','/role/{\\\\d+}',3,2),
(12,'2016-02-24 00:04:07','2016-02-24 00:04:07','Role Module Delete',0,'PERMISSION_ROLE_DELETE','Role Module Delete','/role/',4,2),
(13,'2016-02-26 17:31:47','2016-02-26 17:36:25','Role Module Permission',0,'PERMISSION_ROLE_PERMISSION','Role Module Permission','/role/permission/{\\\\d+}',5,2),
(14,'2016-02-01 00:00:00','2016-02-24 21:48:31','Push Notification Module View',0,'PERMISSION_PUSH_VIEW','Push Notification Module View','/push/list',1,5),
(15,'2016-02-23 23:57:23','2016-02-23 23:57:23','Push Notification Module Add',0,'PERMISSION_PUSH_ADD','Push Notification Module Add','/push/',2,5),
(16,'2016-02-23 23:58:33','2016-02-23 23:58:33','Push Notification Module Edit',0,'PERMISSION_PUSH_EDIT','Push Notification Module Edit','/push/{\\\\d+}',3,5),
(17,'2016-02-24 00:01:07','2016-02-24 00:01:07','Push Notification Module Delete',0,'PERMISSION_PUSH_DELETE','Push Notification Module Delete','/push/',4,5),
(18,'2016-02-01 00:00:00','2016-02-24 21:48:31','Version Control Module View',0,'PERMISSION_VERSION_VIEW','Version Control Module View','/version/list',1,6),
(19,'2016-02-23 23:57:23','2016-02-23 23:57:23','Version Control Module Add',0,'PERMISSION_VERSION_ADD','Version Control Module Add','/version/',2,6),
(20,'2016-02-23 23:58:33','2016-02-23 23:58:33','Version Control Module Edit',0,'PERMISSION_VERSION_EDIT','Version Control Module Edit','/version/{\\\\d+}',3,6),
(21,'2016-02-24 00:01:07','2016-02-24 00:01:07','Version Control Module Delete',0,'PERMISSION_VERSION_DELETE','Version Control Module Delete','/version/',4,6);


#20160422 updated
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PUSHMESSAGE.TITLE', 'PUSHMESSAGE');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PUSHMESSAGE.ADD', 'Add Push Message');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PUSHMESSAGE.UPDATE', 'Update Push Message');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'PUSHMESSAGE.DELETE', 'Delete Push Message');

#20160426 updated
INSERT INTO system_setting (name, value) VALUES ('androidPushNotificationServiceApiKey', '');
INSERT INTO system_setting (name, value) VALUES ('iOSPushNotificationServiceCertificate', '/pushCert/Certificates_AIA_DIST_APNS.p12');
INSERT INTO system_setting (name, value) VALUES ('iOSPushNotificationServicePassword', 'password');
INSERT INTO system_setting (name, value) VALUES ('iOSPushNotificationServiceIsProduction', 'true');

#20160528 updated
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'VERSION.TITLE', 'VERSION');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'VERSION.ADD', 'Add Version');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'VERSION.UPDATE', 'Update Version');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'VERSION.DELETE', 'Delete Version');

-- Added on 20170308 for preset data for activity type
INSERT INTO activity_type VALUES (1,'ADD'),(2,'UPDATE'),(3,'DELETE'),(4,'VIEW');

#20170426 updated
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'AUDIT.LOG.TITLE', 'Audit Log');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'AUDIT.LOG.DOWNLOAD.TITLE', 'Download Audit Log');
INSERT INTO message_resource (locale, name, value) VALUES ('en_US', 'AUDIT.LOG.EXPORT', 'Export Audit Log');
INSERT INTO permission (id, create_time, last_update_time, name, status,  code, description, link, module_type_id, action_type_id) VALUES
(22,'2016-02-01 00:00:00','2016-02-24 21:48:31','Audit Log View',0,'PERMISSION_AUDIT_VIEW','Audit Log View','/auditlog/list',7,1);
