/*---------------------------------------------------------Common------------------------------------------------------------------------------*/

/**
 @apiDefine CommonFailAPIDef

 @apiError (Error 400) code:1000 Already Played
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Action Fail",
  "message": "Action Fail",
  "data": null
 }
 */

/**
 @apiDefine CommonCampaignPlayedAPIDef

 @apiError (Error 400) code:3333 Already Played
 @apiErrorExample code:3333
 HTTP/1.1 400 Bad Request
 {
  "code": 3333,
  "developerMessage": "Already Played",
  "message": "Already Played",
  "data": null
 }
 */


/**
 @apiDefine CommonCampaignExpiryAPIDef

 @apiError (Error 400) code:7777 Campaign Expired Error
 @apiErrorExample code:7777
 HTTP/1.1 400 Bad Request
 {
  "code": 7777,
  "developerMessage": "Campaign Is End",
  "message": "Campaign Is End",
  "data": null
 }
 */

/**
 @apiDefine CommonInvalidMemberSessionTokenAPIDef

 @apiHeader {String} session-token Member Session Token. Return by Login API
 @apiError (Error 400) code:8888 Member Invalid Session Token Error
 @apiErrorExample code:8888
 HTTP/1.1 400 Bad Request
 {
  "code": 8888,
  "developerMessage": "Please login again",
  "message": "Please login again",
  "data": null
 }
 */

/**
@apiDefine CommonAPIDef

@apiHeader {String} language     Language Name. If the language hasn't set before, English will be the default language in client side.
@apiError (Error 400) code:9999 General System Error
@apiErrorExample code:9999
HTTP/1.1 400 Bad Request
{
 "code": 9999,
 "developerMessage": "System Error! Please try again later",
 "message": "System Error! Please try again later",
 "data": null
}
 */

/**
@apiDefine CommonResponseAPIDef

@apiSuccess {Integer} code Response Status 0 – Success, Other values: errors or not fully success
@apiSuccess {String} message [Optional] The error to be displayed to the user
@apiSuccess {String} developerMessage [Optional] more explanation on the error if necessary, not for display
@apiSuccess {Object} data [Optional] Return Data Object
*/



/*---------------------------------------------------------1. Member------------------------------------------------------------------------------*/

/**
 @api {get} /member/status 1.1 Check Member Status
 @apiGroup 1_Member
 @apiName 1.1 Check Member Status
 @apiDescription 1.1 Check Member Status - Using for page flow 1.1 Landing Page
 @apiVersion 1.0.0


 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiSuccess {String} data.name Member Name
 @apiSuccess {String} data.gameStage Member Game Core Stage, only exist when userStatus = 3
 @apiSuccess {Integer} data.userStatus Member Status<br/>
 0=available to play -> Redirect to game core page<br/>
 1=no login -> Redirect to login page<br/>
 2=played -> popup alert<br/>
 3=playing -> Redirect to corresponding game core page<br/>
 4=no gift -> popup alert<br/>


 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "name" : "Test Member",
        "gameStage" : "",
        "userStatus" : 0
   }
 }
 @apiError (Error 400) code:1111 member not exist
 @apiErrorExample code:1111
 HTTP/1.1 400 Bad Request
 {
  "code": 1111,
  "developerMessage": "Member Not Exist",
  "message": "Member Not Exist",
  "data": null
 }
 */

/**
 @api {get} /member/status 1.1 Check Member Status
 @apiGroup 1_Member
 @apiName 1.1 Check Member Status
 @apiDescription 1.1 Check Member Status - Using for page flow 1.1 Landing Page
 @apiVersion 1.0.2

 @apiUse CommonCampaignPlayedAPIDef
 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiSuccess {String} data.name Member Name
 @apiSuccess {String} data.gameStage Member Game Core Stage, only exist when userStatus = 3
 @apiSuccess {Integer} data.userStatus Member Status<br/>
 0=available to play -> Redirect to game core page<br/>
 1=playing -> Redirect to corresponding game core page<br/>


 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "name" : "Test Member",
        "gameStage" : "",
        "userStatus" : 0
   }
 }
 @apiError (Error 400) code:1111 member not exist
 @apiErrorExample code:1111
 HTTP/1.1 400 Bad Request
 {
  "code": 1111,
  "developerMessage": "Member Not Exist",
  "message": "Member Not Exist",
  "data": null
 }
 @apiError (Error 400) code:2222 gift no stock
 @apiErrorExample code:2222
 HTTP/1.1 400 Bad Request
 {
  "code": 2222,
  "developerMessage": "Gift No Stock",
  "message": "gift no stock",
  "data": null
 }
 */

/**
 @api {get} /member/status 1.1 Check Member Status
 @apiGroup 1_Member
 @apiName 1.1 Check Member Status
 @apiDescription 1.1 Check Member Status - Using for page flow 1.1 Landing Page
 @apiVersion 1.0.3

 @apiUse CommonCampaignPlayedAPIDef
 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiSuccess {String} data.name Member Name
 @apiSuccess {String} data.gameStage Member Game Core Stage, only exist when userStatus = 3
 @apiSuccess {Integer} data.userStatus Member Status<br/>
 0=available to play -> Redirect to game core page<br/>
 1=playing -> Redirect to corresponding game core page<br/>


 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "name" : "Test Member",
        "gameStage" : "",
        "userStatus" : 0
   }
 }
 @apiError (Error 400) code:1001 member not exist
 @apiErrorExample code:1001
 HTTP/1.1 400 Bad Request
 {
  "code": 1001,
  "developerMessage": "Member Not Exist",
  "message": "Member Not Exist",
  "data": null
 }
 @apiError (Error 400) code:2001 gift no stock
 @apiErrorExample code:2001
 HTTP/1.1 400 Bad Request
 {
  "code": 2001,
  "developerMessage": "Gift No Stock",
  "message": "gift no stock",
  "data": null
 }
 */


/**
 @api {get} /member/profile 1.2 Get Member Profile
 @apiGroup 1_Member
 @apiName 1.2 Get Member Profile
 @apiDescription 1.2 Get Member Profile - Using for page flow 1.3.2 Menu (After log-in)
 @apiVersion 1.0.0

 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiSuccess {String} data.name Member Name
 @apiSuccess {Boolean} data.isActiveCampaign Campaign Status

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "name" : "Test Member",
        "isActiveCampaign" : true
   }
 }

 @apiError (Error 400) code:1111 member not exist
 @apiErrorExample code:1111
 HTTP/1.1 400 Bad Request
 {
  "code": 1111,
  "developerMessage": "Member Not Exist",
  "message": "Member Not Exist",
  "data": null
 }
 */
/**
 @api {get} /member/profile 1.2 Get Member Profile
 @apiGroup 1_Member
 @apiName 1.2 Get Member Profile
 @apiDescription 1.2 Get Member Profile - Using for page flow 1.3.2 Menu (After log-in)
 @apiVersion 1.0.3

 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiSuccess {String} data.name Member Name
 @apiSuccess {Boolean} data.isActiveCampaign Campaign Status

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "name" : "Test Member",
        "isActiveCampaign" : true
   }
 }

 @apiError (Error 400) code:1001 member not exist
 @apiErrorExample code:1001
 HTTP/1.1 400 Bad Request
 {
  "code": 1001,
  "developerMessage": "Member Not Exist",
  "message": "Member Not Exist",
  "data": null
 }
 */

/**
 @api {post} /member/signup 1.3 Member Signup
 @apiGroup 1_Member
 @apiName 1.3 Member Signup
 @apiDescription 1.3 Member Signup - Using for page flow 2.6 Form Page - Without input
 @apiVersion 1.0.0

 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} name Member Name
 @apiParam {String} email Member Email Address
 @apiParam {String} password Password
 @apiParam {Integer} age Age Range
 @apiParam {Integer} livingDistrict Living District
 @apiParam {Integer} workingDistrict Working District
 @apiParam {Integer} subWorkingDistrict Sub Working District (Only submit when Working District = Taikoo)
 @apiParam {String} contact Member Contact
 @apiParam {Boolean} isMarketInfo Is Ticked "do not send direct marketing information to me checkbox

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": null
 }

 @apiError (Error 400) code:1000 Missing parameter x
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Missing parameter x",
  "message": "Missing parameter x",
  "data": null
 }
 @apiError (Error 400) code:2000 email already is used
 @apiErrorExample code:2000
 HTTP/1.1 400 Bad Request
 {
  "code": 2000,
  "developerMessage": "Email already in used",
  "message": "Email already in used",
  "data": null
 }

 */


/**
 @api {post} /member/signup 1.3 Member Signup
 @apiGroup 1_Member
 @apiName 1.3 Member Signup
 @apiDescription 1.3 Member Signup - Using for page flow 2.6 Form Page - Without input
 @apiVersion 1.0.1

 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} name Member Name
 @apiParam {String} email Member Email Address
 @apiParam {String} password Password
 @apiParam {Integer} age Age Range
 @apiParam {Integer} livingDistrict Living District
 @apiParam {Integer} workingDistrict Working District
 @apiParam {Integer} subWorkingDistrict Sub Working District (Only submit when Working District = Taikoo)
 @apiParam {String} contact Member Contact
 @apiParam {Boolean} isPics Is Ticked "PICS" checkbox

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": null
 }

 @apiError (Error 400) code:1000 Missing parameter x
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Missing parameter x",
  "message": "Missing parameter x",
  "data": null
 }
 @apiError (Error 400) code:2000 email already is used
 @apiErrorExample code:2000
 HTTP/1.1 400 Bad Request
 {
  "code": 2000,
  "developerMessage": "Email already in used",
  "message": "Email already in used",
  "data": null
 }

 */

/**
 @api {post} /member/signup 1.3 Member Signup
 @apiGroup 1_Member
 @apiName 1.3 Member Signup
 @apiDescription 1.3 Member Signup - Using for page flow 2.6 Form Page - Without input
 @apiVersion 1.0.2

 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} name Member Name
 @apiParam {String} email Member Email Address
 @apiParam {String} password Password
 @apiParam {Integer} age Age Range
 @apiParam {Integer} gender Gender
 @apiParam {Integer} livingDistrict Living District
 @apiParam {Integer} workingDistrict Working District
 @apiParam {Integer} subWorkingDistrict Sub Working District (Only submit when Working District = Taikoo)
 @apiParam {String} contact Member Contact
 @apiParam {Boolean} isPics Is Ticked "PICS" checkbox

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": null
 }

 @apiError (Error 400) code:1000 Missing parameter x
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Missing parameter x",
  "message": "Missing parameter x",
  "data": null
 }
 @apiError (Error 400) code:2000 email already is used
 @apiErrorExample code:2000
 HTTP/1.1 400 Bad Request
 {
  "code": 2000,
  "developerMessage": "Email already in used",
  "message": "Email already in used",
  "data": null
 }

 */

/**
 @api {post} /member/signup 1.3 Member Signup
 @apiGroup 1_Member
 @apiName 1.3 Member Signup
 @apiDescription 1.3 Member Signup - Using for page flow 2.6 Form Page - Without input
 @apiVersion 1.0.3

 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} name Member Name
 @apiParam {String} email Member Email Address
 @apiParam {String} password Password
 @apiParam {Integer} age Age Range
 @apiParam {Integer} gender Gender
 @apiParam {Integer} livingDistrict Living District
 @apiParam {Integer} workingDistrict Working District
 @apiParam {Integer} subWorkingDistrict Sub Working District (Only submit when Working District = Taikoo)
 @apiParam {String} contact Member Contact
 @apiParam {Boolean} isPics Is Ticked "PICS" checkbox

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": null
 }

 @apiError (Error 400) code:1000 Missing parameter x
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Missing parameter x",
  "message": "Missing parameter x",
  "data": null
 }
 @apiError (Error 400) code:1002 email already is used
 @apiErrorExample code:1002
 HTTP/1.1 400 Bad Request
 {
  "code": 1002,
  "developerMessage": "Email already in used",
  "message": "Email already in used",
  "data": null
 }

 */

/**
 @api {post} /member/signup 1.3 Member Signup
 @apiGroup 1_Member
 @apiName 1.3 Member Signup
 @apiDescription 1.3 Member Signup - Using for page flow 2.6 Form Page - Without input
 @apiVersion 1.0.4

 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} name Member Name
 @apiParam {String} email Member Email Address
 @apiParam {String} password Password
 @apiParam {Integer} age Age Range
 @apiParam {Integer} gender Gender
 @apiParam {Integer} livingDistrict Living District
 @apiParam {Integer} workingDistrict Working District
 @apiParam {Integer} subWorkingDistrict Sub Working District (Only submit when Working District = Taikoo)
 @apiParam {String} contact Member Contact
 @apiParam {Boolean} isPics Is Ticked "PICS" checkbox
 @apiParam {Boolean} isLogin=true Is Auto Login Checking - Return sessionToken if true

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "sessionToken" : "uashoiashfp289f33d3d"
   }
 }

 @apiError (Error 400) code:1000 Missing parameter x
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Missing parameter x",
  "message": "Missing parameter x",
  "data": null
 }
 @apiError (Error 400) code:1002 email already is used
 @apiErrorExample code:1002
 HTTP/1.1 400 Bad Request
 {
  "code": 1002,
  "developerMessage": "Email already in used",
  "message": "Email already in used",
  "data": null
 }

 */

/**
 @api {post} /member/signup 1.3 Member Signup
 @apiGroup 1_Member
 @apiName 1.3 Member Signup
 @apiDescription 1.3 Member Signup - Using for page flow 2.6 Form Page - Without input
 @apiVersion 1.0.6

 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} name Member Name
 @apiParam {String} email Member Email Address
 @apiParam {String} password Password
 @apiParam {Integer} age Age Range
 @apiParam {Integer} gender Gender
 @apiParam {Integer} livingDistrict Living District
 @apiParam {Integer} workingDistrict Working District
 @apiParam {Integer} subWorkingDistrict Sub Working District (Only submit when Working District = Taikoo)
 @apiParam {String} contact Member Contact
 @apiParam {Boolean} isPics Is Ticked "PICS" checkbox
 @apiParam {Boolean} isLogin=true Is Auto Login Checking - Return sessionToken if true

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "sessionToken" : "uashoiashfp289f33d3d",
        "isActiveCampaign": true
   }
 }

 @apiError (Error 400) code:1000 Missing parameter x
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Missing parameter x",
  "message": "Missing parameter x",
  "data": null
 }
 @apiError (Error 400) code:1002 email already is used
 @apiErrorExample code:1002
 HTTP/1.1 400 Bad Request
 {
  "code": 1002,
  "developerMessage": "Email already in used",
  "message": "Email already in used",
  "data": null
 }

 */

/**
 @api {post} /member/signin 1.4 Member Signin
 @apiGroup 1_Member
 @apiName 1.4 Member Signin
 @apiDescription 1.4 Member Signin - Using for page flow 2.1 Log-in Page
 @apiVersion 1.0.0

 @apiUse CommonFailAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} username Username
 @apiParam {String} password Password

 @apiSuccess {String} data.sessionToken Member Session Token

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "sessionToken" : "uashoiashfp289f33d3d"
   }
 }

 */

/**
 @api {post} /member/signin 1.4 Member Signin
 @apiGroup 1_Member
 @apiName 1.4 Member Signin
 @apiDescription 1.4 Member Signin - Using for page flow 2.1 Log-in Page
 @apiVersion 1.0.6

 @apiUse CommonFailAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} username Username
 @apiParam {String} password Password

 @apiSuccess {String} data.sessionToken Member Session Token

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "sessionToken" : "uashoiashfp289f33d3d",
        "isActiveCampaign": true
   }
 }

 */


/**
 @api {post} /member/forgotPassword 1.5 Member Forgot Password
 @apiGroup 1_Member
 @apiName 1.5 Member Forgot Password
 @apiDescription 1.5 Member Forgot Password - Using for page flow 2.2 Forgot Password
 @apiVersion 1.0.0

 @apiUse CommonFailAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} email Member Email

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": null
 }

 */

/**
 @api {post} /member/resetPassword 1.6 Member Reset Password
 @apiGroup 1_Member
 @apiName 1.6 Member Reset Password
 @apiDescription 1.6 Member Reset Password - Using for page flow 2.4 Reset Password Page
 @apiVersion 1.0.0

 @apiUse CommonFailAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {String} password Member New Password
 @apiParam {String} secretKey Member Reset Password secretKey

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": null
 }

 */

/**
 @api {get} /member/registrationDetails 1.7 Get Registration Details
 @apiGroup 1_Member
 @apiName 1.7 Get Registration Details
 @apiDescription 1.7 Get Registration Details - Using for page flow 2.6 Form Page - Without input
 @apiVersion 1.0.0

 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiSuccess {Object[]} data Return Data Object
 @apiSuccess {Object[]} data.ages Ages Range Object Array
 @apiSuccess {Integer} data.ages.id Age Range ID
 @apiSuccess {String} data.ages.name Age Range name
 @apiSuccess {Object[]} data.gender Gender Object Array
 @apiSuccess {Integer} data.gender.id Gender ID
 @apiSuccess {String} data.gender.name Gender name
 @apiSuccess {Object[]} data.livingDistricts Living District Object Array
 @apiSuccess {Integer} data.livingDistricts.id Living District ID
 @apiSuccess {String} data.livingDistricts.name Living District name
 @apiSuccess {Object[]} data.workingDistricts Working District Object Array
 @apiSuccess {Integer} data.workingDistricts.id Working District ID
 @apiSuccess {String} data.workingDistricts.name Working District name
 @apiSuccess {Object[]} data.workingDistricts.subWorkingDistricts Sub-Working District Object Array
 @apiSuccess {Integer} data.workingDistricts.subWorkingDistricts.id Sub-Working District ID
 @apiSuccess {String} data.workingDistricts.subWorkingDistricts.name Sub-Working District name


 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "ages" : [
            {
                "id" : 1,
                "name" : ""
            }
        ],
        "gender" : [
            {
                "id" : 1,
                "name" : ""
            }
        ],
        "livingDistricts" : [
            {
                "id" : 1,
                "name" : ""
            }
        ],
        "workingDistricts" : [
            {
                "id" : 1,
                "name" : "Taikoo"
                "subWorkingDistricts" : [
                    {
                        "id" : 1,
                        "name" : ""
                    }
                ]
            },
            {
                "id" : 2,
                "name" : ""
                "subWorkingDistricts" : []
            }
        ]
   }
 }
 */

/**
 @api {post} /member/logout 1.8 Member Logout
 @apiGroup 1_Member
 @apiName 1.8 Member Logout
 @apiDescription 1.8 Member Logout
 @apiVersion 1.0.2

 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef


 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": null
 }
 */

/*---------------------------------------------------------2. Report Duty------------------------------------------------------------------------------*/
/**
 @api {get} /campaign/question 2.1 Get Campaign Question
 @apiGroup 2_Report_Duty
 @apiName 2.1 Get Campaign Question
 @apiDescription 2.1 Get Campaign Question - Using for page flow 3.2 Report Duty Page - Select Symbol & 3.3 Mission Page
 @apiVersion 1.0.0

 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiSuccess {Object} data Return Data Object
 @apiSuccess {Object} data.question Question Object
 @apiSuccess {Integer} data.question.id Question ID
 @apiSuccess {String} data.question.name Question
 @apiSuccess {String} data.question.image Question Image
 @apiSuccess {Integer} data.question.type Question Type (0=Report Duty, 1=Question)
 @apiSuccess {Integer} data.question.symbolId Question - Answer Symbol ID
 @apiSuccess {Integer} data.question.questionNo Question Number
 @apiSuccess {Integer} data.question.totalQuestionNo Total Question Number
 @apiSuccess {Object[]} data.locations Location Object Array
 @apiSuccess {Integer} data.locations.id Symbol ID
 @apiSuccess {String} data.locations.image Symbol Image Path
 @apiSuccess {Object[]} data.symbols Symbol Object Array
 @apiSuccess {Integer} data.symbols.id Symbol ID
 @apiSuccess {String} data.symbols.image Symbol Image Path

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "question" : {
            "id" : 1,
            "name" : "Report duty",
            "image" : "",
            "type" : 0,
            "symbolId" : 1,
            "questionNo" : null,
            "totalQuestionNo" : 3
        },
        "locations" : [
            {
                "id" : 1,
                "image" : "http://xxx.com/image1.png"
            },
            {
                "id" : 2,
                "image" : "http://xxx.com/image2.png"
            },
            {
                "id" : 3,
                "image" : "http://xxx.com/image3.png"
            }
        ],
        "symbols" : [
            {
                "id" : 1,
                "image" : "http://xxx.com/image1.png"
            },
            {
                "id" : 2,
                "image" : "http://xxx.com/image2.png"
            },
            {
                "id" : 3,
                "image" : "http://xxx.com/image3.png"
            }
        ]
   }
 }

 @apiError (Error 400) code:1000 Already Played
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Already Played",
  "message": "Already Played",
  "data": null
 }
 */

/**
 @api {get} /campaign/question 2.1 Get Campaign Question
 @apiGroup 2_Report_Duty
 @apiName 2.1 Get Campaign Question
 @apiDescription 2.1 Get Campaign Question - Using for page flow 3.2 Report Duty Page - Select Symbol & 3.3 Mission Page
 @apiVersion 1.0.3

 @apiUse CommonCampaignPlayedAPIDef
 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiSuccess {Object} data Return Data Object
 @apiSuccess {Object} data.question Question Object
 @apiSuccess {Integer} data.question.id Question ID
 @apiSuccess {String} data.question.name Question
 @apiSuccess {String} data.question.image Question Image
 @apiSuccess {Integer} data.question.type Question Type (0=Report Duty, 1=Question)
 @apiSuccess {Integer} data.question.questionNo Question Number
 @apiSuccess {Integer} data.question.totalQuestionNo Total Question Number
 @apiSuccess {Object[]} data.locations Location Object Array
 @apiSuccess {Integer} data.locations.id Symbol ID
 @apiSuccess {String} data.locations.image Symbol Image Path
 @apiSuccess {Object[]} data.symbols Symbol Object Array
 @apiSuccess {Integer} data.symbols.id Symbol ID
 @apiSuccess {String} data.symbols.image Symbol Image Path

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "question" : {
            "id" : 1,
            "name" : "Report duty",
            "image" : "",
            "type" : 0,
            "symbolId" : 1,
            "questionNo" : null,
            "totalQuestionNo" : 3
        },
        "locations" : [
            {
                "id" : 1,
                "image" : "http://xxx.com/image1.png"
            },
            {
                "id" : 2,
                "image" : "http://xxx.com/image2.png"
            },
            {
                "id" : 3,
                "image" : "http://xxx.com/image3.png"
            }
        ],
        "symbols" : [
            {
                "id" : 1,
                "image" : "http://xxx.com/image1.png"
            },
            {
                "id" : 2,
                "image" : "http://xxx.com/image2.png"
            },
            {
                "id" : 3,
                "image" : "http://xxx.com/image3.png"
            }
        ]
   }
 }

 */

/**
 @api {post} /campaign/submitQuestion 2.2 Submit Campaign Question
 @apiGroup 2_Report_Duty
 @apiName 2.2 Submit Question
 @apiDescription 2.2 Submit Question - Using for page flow 3.2 Report Duty Page - Select Symbol & 3.3 Mission Page
 @apiVersion 1.0.0

 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {Integer} id Question ID
 @apiParam {Integer} symbolId Answer Symbol ID
 @apiParam {Integer} questionType Question Type (0=Report Duty, 1=Question)

 @apiSuccess {Boolean} data.isCorrect Result of submission<br/>true -> Answer Next Question<br/>false ->Popup alert
 @apiSuccess {Boolean} data.isContinue Is Continue to play next question <br/>true -> Answer Next Question<br/>false -> Get to Result Page


 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "isCorrect" : true,
        "isContinue" : true
   }
 }
 */

/**
 @api {post} /campaign/submitQuestion 2.2 Submit Campaign Question
 @apiGroup 2_Report_Duty
 @apiName 2.2 Submit Question
 @apiDescription 2.2 Submit Question - Using for page flow 3.2 Report Duty Page - Select Symbol & 3.3 Mission Page
 @apiVersion 1.0.3

 @apiUse CommonCampaignPlayedAPIDef
 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {Integer} id Question ID
 @apiParam {Integer} symbolId Answer Symbol ID
 @apiParam {Integer} questionType Question Type (0=Report Duty, 1=Question)

 @apiSuccess {Boolean} data.isCorrect Result of submission<br/>true -> Answer Next Question<br/>false ->Popup alert
 @apiSuccess {Boolean} data.isContinue Is Continue to play next question <br/>true -> Answer Next Question<br/>false -> Get to Result Page

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "isCorrect" : true,
        "isContinue" : true
   }
 }
 */

/**
 @api {post} /campaign/submitQuestion 2.2 Submit Campaign Question
 @apiGroup 2_Report_Duty
 @apiName 2.2 Submit Question
 @apiDescription 2.2 Submit Question - Using for page flow 3.2 Report Duty Page - Select Symbol & 3.3 Mission Page
 @apiVersion 1.0.4

 @apiUse CommonCampaignPlayedAPIDef
 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiParam {Integer} id Question ID
 @apiParam {Integer} symbolId Answer Symbol ID
 @apiParam {Integer} questionType Question Type (0=Report Duty, 1=Question)
 @apiParam {Boolean} isReturnNextQuestion=true Return next question checking


 @apiSuccess {Boolean} data Return Data Object
 @apiSuccess {Boolean} data.isCorrect Result of submission<br/>true -> Answer Next Question<br/>false ->Popup alert
 @apiSuccess {Boolean} data.isContinue Is Continue to play next question <br/>true -> Answer Next Question<br/>false -> Get to Result Page
 @apiSuccess {Object} data.nextQuestion Next Question Object  - Return if isContinue = true
 @apiSuccess {Object} data.nextQuestion.question Question Object
 @apiSuccess {Integer} data.nextQuestion.question.id Question ID
 @apiSuccess {String} data.nextQuestion.question.name Question
 @apiSuccess {String} data.nextQuestion.question.image Question Image
 @apiSuccess {Integer} data.nextQuestion.question.type Question Type (0=Report Duty, 1=Question)
 @apiSuccess {Integer} data.nextQuestion.question.questionNo Question Number
 @apiSuccess {Integer} data.nextQuestion.question.totalQuestionNo Total Question Number
 @apiSuccess {Object[]} data.nextQuestion.symbols Symbol Object Array
 @apiSuccess {Integer} data.nextQuestion.symbols.id Symbol ID
 @apiSuccess {String} data.nextQuestion.symbols.image Symbol Image Path

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "isCorrect" : true,
        "isContinue" : true,
        "nextQuestion" : {
            "question" : {
                "id" : 1,
                "name" : "Question",
                "image" : "",
                "type" : 1,
                "questionNo" : 1,
                "totalQuestionNo" : 3
            },
            "symbols" : [
                {
                    "id" : 1,
                    "image" : "http://xxx.com/image1.png"
                },
                {
                    "id" : 2,
                    "image" : "http://xxx.com/image2.png"
                },
                {
                    "id" : 3,
                    "image" : "http://xxx.com/image3.png"
                }
            ]
        }
   }
 }
 */


/**
 @api {get} /campaign/status 2.3 Check Game Status
 @apiGroup 2_Report_Duty
 @apiName 2.3 Check Game Status
 @apiDescription 2.3 Check Game Status - Using for page flow 3.4 Game Result Page
 @apiVersion 1.0.0

 @apiUse CommonCampaignExpiryAPIDef
 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef


 @apiSuccess {Integer} data.status Game Status<br/> 0 = Played<br/> 1 = Not Play
 @apiSuccess {Integer} data.giftId Gift ID. Return is status = 0


 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "status" : 0,
        "giftId" : 1
   }
 }
 */


/*---------------------------------------------------------3. Redemption------------------------------------------------------------------------------*/

/**
 @api {get} /gift/list 3.1 Gift List
 @apiGroup 3_Redemption
 @apiName 3.1 Gift List
 @apiDescription 3.1 Gift List - Using for page flow 1.4 Campaign Gift List
 @apiVersion 1.0.0

 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef

 @apiSuccess {Object} data Return Data Object
 @apiSuccess {Object[]} data.normalGifts Normal Gift Object Array
 @apiSuccess {Integer} data.normalGifts.id Normal Gift ID
 @apiSuccess {String} data.normalGifts.name Normal Gift Name
 @apiSuccess {String} data.normalGifts.image Normal Gift Image Path
 @apiSuccess {Object[]} data.awardGifts Award Gift Object Array
 @apiSuccess {Integer} data.awardGifts.id Award Gift ID
 @apiSuccess {String} data.awardGifts.name Award Gift Name
 @apiSuccess {String} data.awardGifts.image Award Gift Image Path


 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "awardGifts" : [
            {
                "id" : 1,
                "name" : "Gift 1",
                "image" : "http://xxx.com/image.png"
            }
        ],
        "normalGifts" : [
            {
                "id" : 1,
                "name" : "Gift 1",
                "image" : "http://xxx.com/image.png"
            }
        ]
   }
 }
 */

/**
 @api {get} /gift/redemptionList 3.2 Redemption Gift List
 @apiGroup 3_Redemption
 @apiName 3.2 Redemption Gift List
 @apiDescription 3.2 Redemption Gift List - Using for page flow 4.2 Own Gift Redemption List
 @apiVersion 1.0.0

 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef


 @apiSuccess {Object[]} data.gifts Redemption Gift Object Array
 @apiSuccess {Integer} data.gifts.id Gift ID
 @apiSuccess {String} data.gifts.name Gift Name
 @apiSuccess {String} data.gifts.image Gift Image Path
 @apiSuccess {Integer} data.gifts.status Gift Status (0=Not Redeemed, 1=Redeemed, 2= Expired)
 @apiSuccess {Long} data.gifts.redemptionStartDate Gift Redemption Start Date Timestamp
 @apiSuccess {Long} data.gifts.redemptionEndDate Gift Redemption End Date Timestamp
 @apiSuccess {Long} data.gifts.redeemedDate Gift Redeemed Date Timestamp


 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "gifts" : [
            {
                "id" : 1,
                "name" : "Gift 1",
                "image" : "http://xxx.com/image.png",
                "status" : 0,
                "redemptionStartDate" : 1508944032,
                "redemptionEndDate" : 1514214432,
                "redeemedDate" : null
            }
        ]
   }
 }
 */

/**
 @api {get} /gift/redemptionDetails 3.3 Redemption Gift Details
 @apiGroup 3_Redemption
 @apiName 3.3 Redemption Gift Details
 @apiDescription 3.3 Redemption Gift Details - Using for page flow 4.1 Individual Gift Detail Page
 @apiVersion 1.0.0

 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef


 @apiParam {Integer} giftId Redemption Gift ID


 @apiSuccess {Object} data.gift Redemption Gift Object
 @apiSuccess {Integer} data.gift.id Gift ID
 @apiSuccess {String} data.gift.name Gift Name
 @apiSuccess {String} data.gift.image Gift Image Path
 @apiSuccess {String} data.gift.location Gift Redemption Location
 @apiSuccess {Integer} data.gift.status Gift Status (0=Not Redeemed, 1=Redeemed, 2= Expired)
 @apiSuccess {Long} data.gift.redemptionStartDate Gift Redemption Start Date Timestamp
 @apiSuccess {Long} data.gift.redemptionEndDate Gift Redemption End Date Timestamp
 @apiSuccess {Long} data.gift.redeemedDate Gift Redeemed Date Timestamp

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "gift" : {
            "id" : 1,
            "name" : "Gift 1",
            "image" : "http://xxx.com/image.png",
            "location" : "xxx",
            "status" : 0,
            "redemptionStartDate" : 1508944032,
            "redemptionEndDate" : 1514214432,
            "redeemedDate" : null
        }
   }
 }

 @apiError (Error 400) code:1000 Redemption Gift Not Found
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Redemption Gift Not Found",
  "message": "Redemption Gift Not Found",
  "data": null
 }

 @apiError (Error 400) code:2000 Redemption Gift Is Not Related to Member
 @apiErrorExample code:2000
 HTTP/1.1 400 Bad Request
 {
  "code": 2000,
  "developerMessage": "Redemption Gift Is Not Related to Member",
  "message": "Redemption Gift Is Not Related to Member",
  "data": null
 }
 */

/**
 @api {get} /gift/redemptionDetails 3.3 Redemption Gift Details
 @apiGroup 3_Redemption
 @apiName 3.3 Redemption Gift Details
 @apiDescription 3.3 Redemption Gift Details - Using for page flow 4.1 Individual Gift Detail Page
 @apiVersion 1.0.3

 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef


 @apiParam {Integer} giftId Redemption Gift ID


 @apiSuccess {Object} data.gift Redemption Gift Object
 @apiSuccess {Integer} data.gift.id Gift ID
 @apiSuccess {String} data.gift.name Gift Name
 @apiSuccess {String} data.gift.image Gift Image Path
 @apiSuccess {String} data.gift.location Gift Redemption Location
 @apiSuccess {Integer} data.gift.status Gift Status (0=Not Redeemed, 1=Redeemed, 2= Expired)
 @apiSuccess {Long} data.gift.redemptionStartDate Gift Redemption Start Date Timestamp
 @apiSuccess {Long} data.gift.redemptionEndDate Gift Redemption End Date Timestamp
 @apiSuccess {Long} data.gift.redeemedDate Gift Redeemed Date Timestamp

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "gift" : {
            "id" : 1,
            "name" : "Gift 1",
            "image" : "http://xxx.com/image.png",
            "location" : "xxx",
            "status" : 0,
            "redemptionStartDate" : 1508944032,
            "redemptionEndDate" : 1514214432,
            "redeemedDate" : null
        }
   }
 }

 @apiError (Error 400) code:2002 Redemption Gift Not Found
 @apiErrorExample code:2002
 HTTP/1.1 400 Bad Request
 {
  "code": 2002,
  "developerMessage": "Redemption Gift Not Found",
  "message": "Redemption Gift Not Found",
  "data": null
 }

 @apiError (Error 400) code:2003 Redemption Gift Is Not Related to Member
 @apiErrorExample code:2003
 HTTP/1.1 400 Bad Request
 {
  "code": 2003,
  "developerMessage": "Redemption Gift Is Not Related to Member",
  "message": "Redemption Gift Is Not Related to Member",
  "data": null
 }
 **/

 /**
 @api {get} /gift/redemptionDetails 3.3 Redemption Gift Details
 @apiGroup 3_Redemption
 @apiName 3.3 Redemption Gift Details
 @apiDescription 3.3 Redemption Gift Details - Using for page flow 4.1 Individual Gift Detail Page
 @apiVersion 1.0.5

 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef


 @apiParam {Integer} giftId Redemption Gift ID


 @apiSuccess {Object} data.gift Redemption Gift Object
 @apiSuccess {Integer} data.gift.id Gift ID
 @apiSuccess {String} data.gift.name Gift Name
 @apiSuccess {String} data.gift.image Gift Image Path
 @apiSuccess {String} data.gift.location Gift Redemption Location
 @apiSuccess {String} data.gift.tnc Gift Redemption TNC
 @apiSuccess {Integer} data.gift.status Gift Status (0=Not Redeemed, 1=Redeemed, 2= Expired)
 @apiSuccess {Long} data.gift.redemptionStartDate Gift Redemption Start Date Timestamp
 @apiSuccess {Long} data.gift.redemptionEndDate Gift Redemption End Date Timestamp
 @apiSuccess {Long} data.gift.redeemedDate Gift Redeemed Date Timestamp

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": {
        "gift" : {
            "id" : 1,
            "name" : "Gift 1",
            "image" : "http://xxx.com/image.png",
            "location" : "xxx",
            "tnc" : "xxx",
            "status" : 0,
            "redemptionStartDate" : 1508944032,
            "redemptionEndDate" : 1514214432,
            "redeemedDate" : null
        }
   }
 }

 @apiError (Error 400) code:2002 Redemption Gift Not Found
 @apiErrorExample code:2002
 HTTP/1.1 400 Bad Request
 {
  "code": 2002,
  "developerMessage": "Redemption Gift Not Found",
  "message": "Redemption Gift Not Found",
  "data": null
 }

 @apiError (Error 400) code:2003 Redemption Gift Is Not Related to Member
 @apiErrorExample code:2003
 HTTP/1.1 400 Bad Request
 {
  "code": 2003,
  "developerMessage": "Redemption Gift Is Not Related to Member",
  "message": "Redemption Gift Is Not Related to Member",
  "data": null
 }
 */

/**
 @api {post} /gift/redeem 3.4 Redeem Gift
 @apiGroup 3_Redemption
 @apiName 3.4 Redeem Gift
 @apiDescription 3.4 Redeem Gift - Using for page flow 4.1.2 Individual Gift Detail Page - Enter passcode
 @apiVersion 1.0.0

 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef


 @apiParam {Integer} giftId Redemption Gift ID
 @apiParam {String} passcode Redemption Gift Passcode

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": null
 }

 @apiError (Error 400) code:1000 Redemption Gift Not Found
 @apiErrorExample code:1000
 HTTP/1.1 400 Bad Request
 {
  "code": 1000,
  "developerMessage": "Redemption Gift Not Found",
  "message": "Redemption Gift Not Found",
  "data": null
 }

 @apiError (Error 400) code:2000 Redemption Gift Is Not Related to Member
 @apiErrorExample code:2000
 HTTP/1.1 400 Bad Request
 {
  "code": 2000,
  "developerMessage": "Redemption Gift Is Not Related to Member",
  "message": "Redemption Gift Is Not Related to Member",
  "data": null
 }
 @apiError (Error 400) code:3000 Invalid Passcode
 @apiErrorExample code:3000
 HTTP/1.1 400 Bad Request
 {
  "code": 3000,
  "developerMessage": "Invalid Passcode",
  "message": "Invalid Passcode",
  "data": null
 }
 @apiError (Error 400) code:4000 Gift Expired
 @apiErrorExample code:4000
 HTTP/1.1 400 Bad Request
 {
  "code": 4000,
  "developerMessage": "Gift Expired",
  "message": "Gift Expired",
  "data": null
 }
 @apiError (Error 400) code:5000 Gift Already Redeemed
 @apiErrorExample code:5000
 HTTP/1.1 400 Bad Request
 {
  "code": 5000,
  "developerMessage": "Gift Already Redeemed",
  "message": "Gift Already Redeemed",
  "data": null
 }3
 */

/**
 @api {post} /gift/redeem 3.4 Redeem Gift
 @apiGroup 3_Redemption
 @apiName 3.4 Redeem Gift
 @apiDescription 3.4 Redeem Gift - Using for page flow 4.1.2 Individual Gift Detail Page - Enter passcode
 @apiVersion 1.0.3

 @apiUse CommonInvalidMemberSessionTokenAPIDef
 @apiUse CommonResponseAPIDef
 @apiUse CommonAPIDef


 @apiParam {Integer} giftId Redemption Gift ID
 @apiParam {String} passcode Redemption Gift Passcode

 @apiSuccessExample Success-Response:
 HTTP/1.1 200 OK
 {
   "code": 0,
   "developerMessage": null,
   "message": null,
   "data": null
 }

 @apiError (Error 400) code:2002 Redemption Gift Not Found
 @apiErrorExample code:2002
 HTTP/1.1 400 Bad Request
 {
  "code": 2002,
  "developerMessage": "Redemption Gift Not Found",
  "message": "Redemption Gift Not Found",
  "data": null
 }

 @apiError (Error 400) code:2003 Redemption Gift Is Not Related to Member
 @apiErrorExample code:2003
 HTTP/1.1 400 Bad Request
 {
  "code": 2003,
  "developerMessage": "Redemption Gift Is Not Related to Member",
  "message": "Redemption Gift Is Not Related to Member",
  "data": null
 }
 @apiError (Error 400) code:2004 Invalid Passcode
 @apiErrorExample code:2004
 HTTP/1.1 400 Bad Request
 {
  "code": 2004,
  "developerMessage": "Invalid Passcode",
  "message": "Invalid Passcode",
  "data": null
 }
 @apiError (Error 400) code:2005 Gift Expired
 @apiErrorExample code:2005
 HTTP/1.1 400 Bad Request
 {
  "code": 2005,
  "developerMessage": "Gift Expired",
  "message": "Gift Expired",
  "data": null
 }
 @apiError (Error 400) code:2006 Gift Already Redeemed
 @apiErrorExample code:2006
 HTTP/1.1 400 Bad Request
 {
  "code": 2006,
  "developerMessage": "Gift Already Redeemed",
  "message": "Gift Already Redeemed",
  "data": null
 }
 */