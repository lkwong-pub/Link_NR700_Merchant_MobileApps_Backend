## Change Log in 1.0.6
1. Update API 1.3 Member Signup
2. Update API 1.4 Member SignIn

## Change Log in 1.0.5
1. Update API 3.3 Redemption Gift Details

## Change Log in 1.0.4
1. Update API 1.4 Member Signin
2. Update API 2.2 Submit Campaign Question

## Change Log in 1.0.3
1. Update API 2.1 Get Campaign Question
2. Update API 2.2 Submit Campaign Question
3. Update All error code

## Change Log in 1.0.2
1. Update API 1.1 Check Member Status
2. Update API 1.3 Member Signup
3. Added API 1.8 Member Logout 

## Change Log in 1.0.1
1. Update API 1.3 Member Signup
2. Rename request header - sessionToken to session-token

## Change Log in 1.0.0
1. Initial Commit
