#Swire Cityplaza XMas Campaign Site Backend

## Introduction
Please noted that the backend is related to [Swire Cityplaza XMas Campaign Site Base](https://gitlab.gtomato.com/gt-team/SwireCityplaza_XmasCampaignSite_Backend_Base.git)

## Required Specification
- JDK 1.8.0 or above
- Tomcat 8 or above
- Maven 3.2.x
- Mysql 5.6.0

## Definition of build configurations
Using Maven Build > install

## Git branches definitions
- master: Release branches ( For admin release only )
- develop: development branches ( For admin develop only )

## Included Module 
The Framework included following items:
- Administrator Module 
	- Admin user 
	- Role
	- Permission (Controlled by Superadmin )
- Base Configuration
	- DB Config
	- Web MVC Config
	- Security Config
- Audit Log Module
- Cityplaza Related Module

## API DOC Deploy procedure
apidoc -i doc/apidoc/V1/ -o doc/apidocOutput/
cd doc/apidocOutput/
zip -r apidoc.zip ./*
scp -r apidoc.zip don.cheung@192.168.1.16:/vol1/cityplaza/tomcat8/webapps/APIDOC/V1/
ssh don.cheung@192.168.1.16
cd /vol1/cityplaza/tomcat8/webapps/APIDOC/V1/
unzip -f apidoc.zip
rm -Rf apidoc.zip
