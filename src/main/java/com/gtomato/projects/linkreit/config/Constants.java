package com.gtomato.projects.linkreit.config;

//import com.gtomato.projects.base.config.Constant;

/**
 * Created by doncheung on 31/10/2017.
 */
public class Constants {

    public static final String MESSAGE_KEY_GLOBAL_FAILED = "ERROR.GLOBAL.FAIL";
    public static final String MESSAGE_KEY_MEMBER_ALREDAY_PLAYED = "ERROR.MEMBER.ALREADY.PLAYED";
    public static final String MESSAGE_KEY_CAMPAIGN_EXPIRED_ERROR = "ERROR.CAMPAIGN.EXPIRED";
    public static final String MESSAGE_KEY_MEMBER_EXPIRED_ERROR = "ERROR.MEMBER.EXPIRED";
    public static final String MESSAGE_KEY_GLOBAL_ERROR = "ERROR.GLOBAL.ERROR";

    public static final String MESSAGE_KEY_MEMBER_LOGIN_FAILED = "ERROR.MEMBER.LOGIN.FAILED";
    public static final String MESSAGE_KEY_MEMBER_NOT_FOUND = "ERROR.MEMBER.NOT.FOUND";
    public static final String MESSAGE_KEY_MEMBER_EMAIL_ALREADY_USED = "ERROR.MEMBER.EMAIL.ALREADY.USED";
    public static final String MESSAGE_KEY_GIFT_OUT_OF_STOCK = "ERROR.GIFT.OUT.OF.STOCK";
    public static final String MESSAGE_KEY_GIFT_NOT_FOUND = "ERROR.GIFT.NOT.FOUND";
    public static final String MESSAGE_KEY_GIFT_NOT_RELATED_TO_MEMBER = "ERROR.GIFT.NOT.RELATED.TO.MEMBER";
    public static final String MESSAGE_KEY_GIFT_INVALID_PASSCODE = "ERROR.GIFT.INVALID.PASSCODE";
    public static final String MESSAGE_KEY_GIFT_EXPIRED = "ERROR.GIFT.EXPIRED";
    public static final String MESSAGE_KEY_GIFT_ALREADY_REDEEMED = "ERROR.GIFT.ALREADY.REDEEMED";

    public static final String HEADER_PARAM_LANGUAGE = "language";

    public static final String HEADER_PARAM_SESSION_TOKEN = "session-token";

    public static String WEB_PREFIX = "/";

    public static final String LIST_PATH = "/list";

    public static final String INDEX_PATH = "/index";


    /*------------------------------------------------------Error Code------------------------------------------------------*/
    /*Common*/
    public static final Integer ERROR_FAIL = 1000;
    public static final Integer ERROR_ALREADY_PLAYED = 3333;
    public static final Integer ERROR_CAMPAIGN_EXPIRED = 7777;
    public static final Integer ERROR_INVALID_SESSION_TOKEN = 8888;
    public static final Integer ERROR_GLOBAL = 9999;

    public static final Integer ERROR_MEMBER_NOT_FOUND = 1001;
    public static final Integer ERROR_EMAIL_ALREADY_USED = 1002;
    public static final Integer ERROR_GIFT_OUT_OF_STOCK = 2001;
    public static final Integer ERROR_GIFT_NOT_FOUND = 2002;
    public static final Integer ERROR_GIFT_NOT_RELATED_TO_MEMBER = 2003;
    public static final Integer ERROR_INVALID_PASSCODE = 2004;
    public static final Integer ERROR_GIFT_EXPIRED = 2005;
    public static final Integer ERROR_GIFT_ALREADY_REDEEMED = 2006;


    public static final Integer GIFT_TYPE_AWARD = 1;
    public static final Integer GIFT_TYPE_NORMAL = 2;
    public static final String GIFT_TYPE_NAME_AWARD = "Award";
    public static final String GIFT_TYPE_NAME_NORMAL = "Normal";

    public static final String LOCALE_EN_US = "en_US";
    public static final String LOCALE_ZH_HK = "zh_HK";

    public static final String EMPTY_VALUES = "N/A";

    public static final String REDEEM_STATUS_YES = "Redeemed";
    public static final String REDEEM_STATUS_NO = "Pending";

    public static final Integer REWARD_STATUS_VALID = 0;
    public static final Integer REWARD_STATUS_EXPIRED = 1;

    public static final Integer REPORT_STATUS_DONE = 0;
    public static final Integer REPORT_STATUS_FAIL = 1;
    public static final Integer REPORT_STATUS_GENERATING = 2;

    public static final Integer IMPORT_STATUS_DONE = 0;
    public static final Integer IMPORT_STATUS_DONE_WITH_WARN = 1;
    public static final Integer IMPORT_STATUS_IMPORTING = 2;
    public static final Integer IMPORT_STATUS_FAILED = 3;

    public static final Integer STATUS_YES = 0;
    public static final Integer STATUS_NO = 1;

    public static final String STATUS_STR_YES = "Y";
    public static final String STATUS_STR_NO = "N";

}
