package com.gtomato.projects.linkreit.controller;

import com.gtomato.projects.linkreit.config.Constants;
import com.gtomato.projects.linkreit.exception.BaseException;
import com.gtomato.projects.linkreit.responseObject.api.common.BaseAPIRes;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@ControllerAdvice
public class DefaultExceptionHandler {

    @Resource
    private MessageSource messageSource;

    @ExceptionHandler({BaseException.class})
    public ResponseEntity<BaseAPIRes> handleBaseException(BaseException e){

        BaseAPIRes response = new BaseAPIRes();
        response.setDeveloperMessage(StringUtils.isBlank(e.getDeveloperMessage()) ? e.getMessage() : e.getDeveloperMessage());
        response.setMessage(messageSource.getMessage(e.getMessageKey(), null, e.getLocale()));
        response.setStatus(e.getStatus());

        return new ResponseEntity(response, e.getStatusCode());
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<BaseAPIRes> handleException(Exception e){
        e.printStackTrace();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String language = request.getHeader(Constants.HEADER_PARAM_LANGUAGE);
        Locale locale = StringUtils.isNotBlank(language) ? new Locale(language.split("_")[0],language.split("_")[1]) : null;

        BaseAPIRes response = new BaseAPIRes();
        response.setDeveloperMessage(e.getMessage());
        response.setMessage(messageSource.getMessage(Constants.MESSAGE_KEY_GLOBAL_ERROR, null, locale));
        response.setStatus(Constants.ERROR_GLOBAL);

        return new ResponseEntity(response, HttpStatus.BAD_GATEWAY);
    }

}
