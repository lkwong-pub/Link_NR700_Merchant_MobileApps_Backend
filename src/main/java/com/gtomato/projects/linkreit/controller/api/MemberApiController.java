package com.gtomato.projects.linkreit.controller.api;

import com.gtomato.projects.base.config.Path;
import com.gtomato.projects.base.config.Url;
import com.gtomato.projects.linkreit.requestObject.api.common.BaseMemberReq;
import com.gtomato.projects.linkreit.requestObject.api.member.*;
import com.gtomato.projects.linkreit.responseObject.api.common.BaseAPIRes;
import com.gtomato.projects.linkreit.service.MemberService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by doncheung on 1/11/2017.
 */
@Controller
@RequestMapping(Url.API_PREFIX + Path.VERSION_PATH_V1 + Path.MODULE_MEMBER_PATH)
public class MemberApiController {

    Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private MemberService memberService;

    @RequestMapping(value = Path.MODULE_CHECK_REFERRAL_NO_PATH, method = RequestMethod.POST)
    public ResponseEntity<BaseAPIRes> checkReferralno( @RequestBody MemberCheckReferralNoReq req ) {
        return new ResponseEntity (this.memberService.checkReferralno(req), HttpStatus.OK);
    }

}
