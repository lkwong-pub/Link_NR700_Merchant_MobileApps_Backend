package com.gtomato.projects.linkreit.service.impl;


import com.gtomato.projects.base.config.Path;
import com.gtomato.projects.base.config.Url;
import com.gtomato.projects.linkreit.requestObject.api.common.BaseAPIReq;
import com.gtomato.projects.linkreit.requestObject.api.common.BaseMemberReq;
import com.gtomato.projects.linkreit.requestObject.api.member.*;
import com.gtomato.projects.linkreit.responseObject.api.common.BaseAPIRes;

import com.gtomato.projects.linkreit.service.MemberService;
import com.gtomato.projects.linkreit.utils.HttpClientUtils;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by doncheung on 26/10/2017.
 */
@Service("MemberService")
@PropertySource("classpath:config/environment.properties")
public class MemberServiceImpl implements MemberService {

    Logger logger = Logger.getLogger(this.getClass());

    @Value("${crm.url}")
    private String url = "";

    @Value("${crm.apikey}")
    private String apikey = "52310F0F1D568A860827012B867B4543";

    public BaseAPIRes checkReferralno( MemberCheckReferralNoReq req ){
        String response = new HttpClientUtils().post(url + Path.MODULE_MEMBER_PATH + Path.MODULE_CHECK_REFERRAL_NO_PATH, apikey, req);
        return new BaseAPIRes(response);
    }



}
