package com.gtomato.projects.linkreit.service;

import com.gtomato.projects.linkreit.requestObject.api.common.BaseAPIReq;
import com.gtomato.projects.linkreit.requestObject.api.common.BaseMemberReq;
import com.gtomato.projects.linkreit.requestObject.api.member.*;
import com.gtomato.projects.linkreit.responseObject.api.common.BaseAPIRes;

/**
 * Created by doncheung on 26/10/2017.
 */
public interface MemberService {

    /**
     * Using for API 2.1 Check Member Referral Number
     * @param req
     * @return
     */
    BaseAPIRes checkReferralno( MemberCheckReferralNoReq req );

}
