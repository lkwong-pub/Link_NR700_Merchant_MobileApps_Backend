package com.gtomato.projects.linkreit.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Locale;

/**
 * Created by pius on 18/3/2016.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseException extends RuntimeException{

    private HttpStatus statusCode = HttpStatus.BAD_REQUEST;
    private Integer status;
    private String developerMessage;
    private Locale locale;
    private String messageKey;

}
