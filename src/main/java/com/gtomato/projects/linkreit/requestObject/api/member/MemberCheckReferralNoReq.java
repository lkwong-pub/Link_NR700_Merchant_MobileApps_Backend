package com.gtomato.projects.linkreit.requestObject.api.member;

import com.gtomato.projects.linkreit.requestObject.api.common.BaseAPIReq;
import lombok.Data;

/**
 * Created by doncheung on 26/10/2017.
 */
@Data
public class MemberCheckReferralNoReq extends BaseAPIReq {
    private String referralno;
}
