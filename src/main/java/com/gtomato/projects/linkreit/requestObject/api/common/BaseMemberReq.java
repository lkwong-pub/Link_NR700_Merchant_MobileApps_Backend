package com.gtomato.projects.linkreit.requestObject.api.common;

import lombok.Data;

/**
 * Created by doncheung on 26/10/2017.
 */
@Data
public class BaseMemberReq extends BaseAPIReq{
    private String sessionToken;
    private Integer memberId;
}
