package com.gtomato.projects.linkreit.utils;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GenExcelUtil {

	/**<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi</artifactId>
			<version>3.11</version>
		</dependency>

		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
			<version>3.11</version>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-collections4</artifactId>
			<version>4.1</version>
		</dependency>
	 */
	
	
	/**
	 * Export file path
	 * "your project location" + /excel/ + "Date Folder" + "File Name" .xls
	 * = filePath + /excel/ + getDateFolder() + "fileName" .xls
	 * 
	 */
	private String filePath;
	
	private String fileName;
	
	protected XSSFCellStyle topicFormat;

	protected XSSFCellStyle titleFormat;

	protected XSSFCellStyle headerFormat;

	protected XSSFCellStyle contentFormat;

	private SimpleDateFormat reportDSdf = new SimpleDateFormat("yyyy-MM-dd");

	private SimpleDateFormat reportTSdf = new SimpleDateFormat("HH:mm:ss");
	
	private SimpleDateFormat reportNameSdf = new SimpleDateFormat("yyyyMMddHHmmss");
	
	protected SXSSFWorkbook workbook;

	public static final int STYLE_TOPIC = 1;
	public static final int STYLE_TITLE = 2;
	public static final int STYLE_HEADER = 3;
	public static final int STYLE_CONTENT = 4;
	
	
	public void setFilePath( String filePath ){
		this.filePath = filePath;
	}
	
	public void setFileName( String fileName ){
		this.fileName = fileName + reportNameSdf.format(new Date()) +".xls";
	}
	
	public GenExcelUtil( String filePath, String fileName ) {

		this.filePath = filePath;
		this.fileName = fileName + reportNameSdf.format(new Date()) +".xls"; 
		
		// Create new workbook
		this.workbook = new SXSSFWorkbook(100);
		
		
		// Topic: 12, Bold, Underline
		topicFormat = (XSSFCellStyle) workbook.createCellStyle();
		XSSFFont topicFont = (XSSFFont) workbook.createFont();
		topicFont.setBoldweight( HSSFFont.BOLDWEIGHT_BOLD );
		topicFont.setUnderline( HSSFFont.U_SINGLE );
		topicFont.setFontHeightInPoints((short)12);
		topicFormat.setFont( topicFont );
		
		// Title: 10, Bold
		titleFormat = (XSSFCellStyle) workbook.createCellStyle();
		XSSFFont titleFont = (XSSFFont) workbook.createFont();
		titleFont.setBoldweight( HSSFFont.BOLDWEIGHT_BOLD );
		titleFont.setFontHeightInPoints((short)10);
		titleFormat.setFont( titleFont );
		
		// Table Header: 10, Bold, Border, Center, Gray
		headerFormat = (XSSFCellStyle) workbook.createCellStyle();
		XSSFFont headerFont = (XSSFFont) workbook.createFont();
		headerFont.setBoldweight( HSSFFont.BOLDWEIGHT_BOLD );
		headerFormat.setBorderBottom((short)1);
		headerFormat.setBorderTop((short)1);
		headerFormat.setBorderLeft((short)1);
		headerFormat.setBorderRight((short)1);
		headerFormat.setAlignment( HSSFCellStyle.ALIGN_CENTER );
		headerFormat.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		headerFormat.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		headerFont.setFontHeightInPoints((short)10);
		headerFormat.setFont( headerFont );
		

		// Table Content: 10, Border, Center
		contentFormat = (XSSFCellStyle) workbook.createCellStyle();
		XSSFFont contentFont = (XSSFFont) workbook.createFont();
		contentFormat.setBorderBottom((short)1);
		contentFormat.setBorderTop((short)1);
		contentFormat.setBorderLeft((short)1);
		contentFormat.setBorderRight((short)1);
		contentFormat.setAlignment( HSSFCellStyle.ALIGN_CENTER );
		contentFont.setFontHeightInPoints((short)10);
		contentFormat.setFont( contentFont );
		
	}

	public static double div(double v1, double v2, Integer scale) {
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		if (scale != null) {
			return b2.doubleValue() == 0D ? 0 : b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
		} else {
			return (double) v1 / v2;
		}
	}

	public static double round(double v1, int scale) {
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal one = new BigDecimal("1");
		return b1.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	public Sheet getSheet(String sheetName ){
		Sheet sheet = workbook.getSheet( sheetName );
		if( sheet == null ){
			sheet = workbook.createSheet( sheetName );
			int columns = 17;
			
			//Default columns width
			for(int i=0; i<columns; i++){
				sheet.setColumnWidth((short) i, (short) 255 * 33);
			}
		}
		return sheet;
		
	}
	
	public void insert(Sheet sheet, int rowNo, int colNo, Object content, int type ){
		XSSFCellStyle style = null;
		if( type == STYLE_TOPIC )
			style = topicFormat;
		else if( type == STYLE_TITLE )
			style = titleFormat;
		else if( type == STYLE_HEADER )
			style = headerFormat;
		else if( type == STYLE_CONTENT )
			style = contentFormat;
		
		Row row = sheet.getRow( rowNo ) == null ? sheet.createRow( rowNo ) : sheet.getRow( rowNo );
		Cell cell = row.createCell( colNo );
		
		if( content instanceof Double){
			style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.0"));
			cell.setCellStyle( style );
			cell.setCellValue(Float.valueOf(new DecimalFormat("#0.00").format((Double)content)));
		}else if( content instanceof Float){
			style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.0"));
			cell.setCellStyle( style );
			cell.setCellValue((Float)content);
		}
		else if( content instanceof Integer ){
			style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0"));
			cell.setCellStyle( style );
			cell.setCellValue((Integer)content);
		}
		else if( content instanceof Long ){
			style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0"));
			cell.setCellType(Cell.CELL_TYPE_NUMERIC);
			cell.setCellStyle( style );
			cell.setCellValue((Long)content);
		}
		else{
			cell.setCellStyle( style );
			style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.0"));
			cell.setCellType(Cell.CELL_TYPE_STRING);
			cell.setCellValue( content == null ? "" : content.toString() );
		}
	}
	
	public void createFolder(String folderPath){
		File folder = new File(folderPath);
		if(!folder.exists()){
			folder.mkdirs();
		}
	}
	
	public String getDateFolder(){
		Calendar cal=Calendar.getInstance();
		String folder=cal.get(Calendar.YEAR)
				+ "/" +(cal.get(Calendar.MONTH)+1)
				+ "/" +cal.get(Calendar.DATE) +"/";
		return folder;
	}

	public void exportFile(String filePath) {
		FileOutputStream out = null;

		try {

			out = FileUtils.openOutputStream(new File(filePath));
			this.workbook.write(out);
			out.close();

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		// dispose of temporary files backing this workbook on disk
		this.workbook.dispose();

	}

//	public void exportFile( HttpServletRequest request, HttpServletResponse response) throws Exception {
//
//		// Save excel to server
//		String fileFullPathNoName = filePath + getDateFolder();
//		this.createFolder( fileFullPathNoName );
//
//		String fileFullPath = fileFullPathNoName + fileName;
//		File file = new File( fileFullPath );
//		FileOutputStream fileOut = null;
//
//		try {
//			fileOut = new FileOutputStream(file);
//			workbook.write(fileOut);
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			if (fileOut != null) {
//				try {
//					fileOut.close();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		}
//
//		// out put by request
//		try {
//			RandomAccessFile raf = new RandomAccessFile(file, "r");
//			int size = (int) raf.length();
//			byte[] data = new byte[size];
//			raf.read(data);
//			response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
//			response.setHeader("Content-Length", String.valueOf(file.length()));
//			ServletOutputStream os = response.getOutputStream();
//			os.write(data);
//			os.flush();
//			os.close();
//			raf.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	public String exportDownloadableFile( HttpServletRequest request, HttpServletResponse response) throws Exception {

		// Save excel to server
		String fileFullPathNoName = filePath;
		this.createFolder( fileFullPathNoName );

		String fileFullPath = fileFullPathNoName + fileName;
		File file = new File( fileFullPath );
		FileOutputStream fileOut = null;

		try {
			fileOut = new FileOutputStream(file);
			workbook.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileOut != null) {
				try {
					fileOut.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return fileName;
	}
}
