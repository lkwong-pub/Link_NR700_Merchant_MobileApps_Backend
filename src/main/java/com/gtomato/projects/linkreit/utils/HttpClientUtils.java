package com.gtomato.projects.linkreit.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by doncheung on 4/12/2017.
 */
@Configuration
public class HttpClientUtils {

    Logger logger = Logger.getLogger(this.getClass());

    private final int connectionTimeout = 60000;

    private String charset = "UTF-8";


    public String post(String url, String apikey, Object requestObj) {
        ObjectMapper objectMapper = new ObjectMapper();

        HttpClient httpClient = getHttpsClient();
        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        String responseBody = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            httpPost.setConfig(getRequestConfig());
            StringEntity stringEntity = new StringEntity(objectMapper.writeValueAsString(requestObj));
            httpPost.setEntity(stringEntity);
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("apikey", apikey);
            responseBody = httpClient.execute(httpPost, responseHandler);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return responseBody;
    }


    private CloseableHttpClient getHttpsClient() {
        HttpClientBuilder builder = HttpClients.custom();
        return builder.build();
    }

    private RequestConfig getRequestConfig(){
        return RequestConfig.custom()
                .setSocketTimeout(connectionTimeout)
                .setConnectTimeout(connectionTimeout)
                .setConnectionRequestTimeout(connectionTimeout)
                .setExpectContinueEnabled(false).build();
    }

    public static void main (String[] args) throws Exception {
        String url = "https://linktopdev.azurewebsites.net/api/json/top/member/checkReferralno";
        String apikey = "52310F0F1D568A860827012B867B4543";
        Map<String, Object> requestMap = new HashMap<String, Object>();
        requestMap.put("language","EN");
        requestMap.put("referralno","7AXA5");
        HttpClientUtils httpClientUtils = new HttpClientUtils();
        System.out.println(httpClientUtils.post(url, apikey, requestMap));
    }

}
