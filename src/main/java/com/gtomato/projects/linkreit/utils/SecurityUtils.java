package com.gtomato.projects.linkreit.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;


/**
 * Created by pius on 21/4/2016.
 */
public class SecurityUtils {

    private static Base64 base64 = new Base64();

    public static String encryptStringUsingAes(String message, String password, int keySize, String hashAlgorithm) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        byte[] key = password.getBytes();
        MessageDigest sha = MessageDigest.getInstance(hashAlgorithm);
        key = sha.digest(key);
        key = Arrays.copyOf(key, keySize); // use only first 128 bit

//        System.out.println("key："+new String(key));
        SecretKeySpec spec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES");

        cipher.init(Cipher.ENCRYPT_MODE, spec);

        byte[] encryptData = cipher.doFinal(message.getBytes());

        return base64.encodeToString(encryptData);
    }

    public static String decryptStringUsingAesKey(String message, String keyStr, int keySize, String hashAlgorithm) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        byte[] key = (keyStr).getBytes();
        MessageDigest sha = MessageDigest.getInstance(hashAlgorithm);
        key = sha.digest(key);
        key = Arrays.copyOf(key, keySize); // use only first 128 bit

        SecretKeySpec spec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES");

        cipher.init(Cipher.DECRYPT_MODE, spec);

        return new String(cipher.doFinal(base64.decode(message)));

    }

    public static String encryptStringUsingAes128SHA1(String message, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        return encryptStringUsingAes(message, password, 16, "SHA-1");
    }

    public static String encryptStringUsingAes256SHA1(String message, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        return encryptStringUsingAes(message, password, 32, "SHA-1");
    }

    public static String encryptStringUsingAes12SHA2568(String message, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        return encryptStringUsingAes(message, password, 16, "SHA-256");
    }

    public static String encryptStringUsingAes256SHA256(String message, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        return encryptStringUsingAes(message, password, 32, "SHA-256");
    }

    public static String decryptStringUsingAes128KeySHA1(String message, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        return decryptStringUsingAesKey(message, password, 16, "SHA-1");
    }

    public static String decryptStringUsingAes256KeySHA1(String message, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        return decryptStringUsingAesKey(message, password, 32, "SHA-1");
    }

    public static String decryptStringUsingAes128KeySHA256(String message, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        return decryptStringUsingAesKey(message, password, 16, "SHA-256");
    }

    public static String decryptStringUsingAes256KeySHA256(String message, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        return decryptStringUsingAesKey(message, password, 32, "SHA-256");
    }

    public static String decryptStringUsingAes(byte[] message, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

//        System.out.println("key："+new String(password));
        SecretKeySpec spec = new SecretKeySpec(password.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");

        cipher.init(Cipher.DECRYPT_MODE, spec);

        return new String(cipher.doFinal(message));

    }

    public static String getSHA256String(String input) throws NoSuchAlgorithmException {
        return DigestUtils.sha256Hex(input.getBytes(StandardCharsets.UTF_8));
    }



    public static String getInitVector() {
        final char[] rowNumber = new char[]{(char)130, (char)5, (char)192, (char)173,
                134,(char)198,(char)112,(char)238,
                154,(char)254,(char)108,(char)13,
                101,(char)158,(char)182,(char)119};
        final Integer[] keyArr = new Integer[]{5, 6, 4, 2,
                3, 8, 10, 15,
                11, 13, 9, 1,
                7, 14, 12, 0};

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < rowNumber.length; i++) {
            sb = sb.append(rowNumber[keyArr[i]]);
        }
        return sb.toString();
    }


//    public static String encryptTextWithPassPhraseAndSaltValue( String input, String passPhrase, String saltValue) {
//
//        String initVector = getInitVector();
//        byte[] initVectorBytes = initVector.getBytes(StandardCharsets.US_ASCII);
//        byte[] saltValueBytes = saltValue.getBytes(StandardCharsets.US_ASCII);
//        byte[] plainTextBytes = input.getBytes(StandardCharsets.US_ASCII);
//
//
//        try {
//            PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(passPhrase, saltValueBytes, "SHA1", 2);
//
//            byte[] keyBytes = passwordDeriveBytes.getBytes(32);
//
//            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(keyBytes, "AES"), new IvParameterSpec(initVectorBytes));
//
//            byte[] encryptedTextBytes = cipher.doFinal(plainTextBytes);//UTF-8"));
//
//            return base64.encodeAsString(encryptedTextBytes);
//        } catch (IllegalBlockSizeException e) {
//            e.printStackTrace();
//        } catch (BadPaddingException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        } catch (InvalidAlgorithmParameterException e) {
//            e.printStackTrace();
//        } catch (NoSuchPaddingException e) {
//            e.printStackTrace();
//        } catch (DigestException e) {
//            e.printStackTrace();
//        }
//        return "";
//    }

    public static void main(String[] args) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        System.out.println("Decrypted message = " + SecurityUtils.decryptStringUsingAes128KeySHA1("Y3eeu+49Gul62z+832FO2w=="
                , "aia_vitality2016"));
    }

}
