package com.gtomato.projects.linkreit.responseObject.api.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class BaseAPIRes<T> {

	/**/
	private Integer status = 0;
	private String developerMessage = ""; //Debug Mode Alert Message
	private String message = ""; //Build Alert Message
	private T data;

	public BaseAPIRes (T data){
		this. data = data;
	}

	public BaseAPIRes (String responseString){
		ObjectMapper mapper = new ObjectMapper();

		try {
			Map<String, Object> response = mapper.readValue(responseString, HashMap.class);
			this.status = Integer.parseInt(response.get("httpstatuscode").toString());
			this.message = response.get("message").toString();
			this.data = (T)response.get("data");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
