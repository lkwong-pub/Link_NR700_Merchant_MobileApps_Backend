package com.gtomato.projects.linkreit.responseObject.api.member;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by doncheung on 26/10/2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberProfileRes {
    private String name;
    private Boolean isActiveCampaign;
}
