package com.gtomato.projects.base.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * Created by holong.tsang on 19/7/2017.
 */
@Configuration
@PropertySource("classpath:config/environment.properties")
public class MailConfig {

    @Autowired
    protected Environment environment;

    @Bean
    public JavaMailSender javaMailService() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        if(Boolean.valueOf(environment.getProperty("mail.smtp.status"))){
            javaMailSender.setHost(environment.getProperty("mail.smtp.host"));
            javaMailSender.setDefaultEncoding("UTF-8");
            if(StringUtils.isNotBlank(environment.getProperty("mail.smtp.password"))){
                javaMailSender.setPassword(environment.getProperty("mail.smtp.password"));
            }
        }
        return javaMailSender;
    }


}
