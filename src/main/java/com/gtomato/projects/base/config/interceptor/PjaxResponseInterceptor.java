package com.gtomato.projects.base.config.interceptor;

import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.gtomato.projects.base.config.Url;

public class PjaxResponseInterceptor extends HandlerInterceptorAdapter {

	private final Logger requestLogger = LoggerFactory.getLogger("requestLog");

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		return true;
	}
	
	@Override
    public void postHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
		
		/**
		 * Return template html if 
		 * 1. Request exclude "pjax"
		 * 2. "GET" Method
		 * 3. Exclude /index
		 * 4. Exclude /login
		 * 5. Exclude /logout
		 * 
		 */

		if (request.getRequestURI().contains("/report/download/")
		|| request.getRequestURI().contains("/import/download/")) {
			return;
		}

		if( StringUtils.isBlank( request.getHeader("x-pjax") ) && RequestMethod.GET.toString().equals( request.getMethod() ) && !request.getRequestURI().contains(Url.ADMIN_INDEX_PAGE) &&
			!request.getRequestURI().contains(Url.ADMIN_LOGIN_PAGE) && !request.getRequestURI().contains(Url.ADMIN_LOGOUT_PAGE) ){
			request.setAttribute("content", modelAndView.getViewName());
			modelAndView.setViewName("/admin/template");
		}
    }


}
