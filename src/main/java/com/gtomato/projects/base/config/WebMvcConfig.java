package com.gtomato.projects.base.config;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import com.gt.module.gcm.storage.BaseFileStorageAdaptor;
import com.gt.module.gcm.storage.FileStorage;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

import org.springframework.web.servlet.config.annotation.*;

import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;



import com.gtomato.projects.base.config.interceptor.PjaxResponseInterceptor;
import com.gtomato.projects.base.config.interceptor.PresetAttributeInterceptor;
import com.gtomato.projects.base.config.interceptor.RequestLogInterceptor;


@Configuration
@ComponentScan({"com.gtomato.projects.*"})
@EnableWebMvc
@EnableScheduling
public class WebMvcConfig extends BaseWebMvcConfig {

	@Autowired
	private Environment env;

//	@Autowired
//	private SpringLiquibase liquibase;

//	@Autowired
//	protected DatabaseMessageResourceService databaseMessageResourceService;

//	@Autowired
//	private SystemService systemService;

	@Override
	public void addInterceptors(InterceptorRegistry interceptorRegistry) {
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		//localeChangeInterceptor.setParamName("lang");
		interceptorRegistry.addInterceptor(localeChangeInterceptor);
		interceptorRegistry.addInterceptor(new RequestLogInterceptor());
		interceptorRegistry.addInterceptor(new PresetAttributeInterceptor());
		interceptorRegistry.addInterceptor(new PjaxResponseInterceptor()).addPathPatterns("/admin/**");
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/api*//**//**").allowedMethods("GET","POST","PUT","DELETE","PATCH","OPTIONS");
	}

//
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry resourceHandlerRegistry) {
//		// super.addResourceHandlers(resourceHandlerRegistry);
//		resourceHandlerRegistry.addResourceHandler(Url.STATIC_CSS_PREFIX + "/**").addResourceLocations("/resources/static/css/");
//		resourceHandlerRegistry.addResourceHandler(Url.STATIC_IMAGE_PREFIX + "/**").addResourceLocations("/resources/static/images/");
//		resourceHandlerRegistry.addResourceHandler(Url.STATIC_JS_PREFIX + "/**").addResourceLocations("/resources/static/js/");
//		resourceHandlerRegistry.addResourceHandler(Url.STATIC_PLUGINS_PREFIX + "/**").addResourceLocations("/resources/plugins/");
//		resourceHandlerRegistry.addResourceHandler(Url.STATIC_PREFIX + "/**").addResourceLocations("/resources/static/");
//	}
//
//	@Override
//	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer defaultServletHandlerConfigurer) {
//		// super.configureDefaultServletHandling(defaultServletHandlerConfigurer);
//		defaultServletHandlerConfigurer.enable();
//	}
//
//	@Bean(name = "messageSource")
//	protected MessageSource getMessageSource() {
//
//		// ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
//		// reloadableResourceBundleMessageSource.setBasename("classpath:i18n/messages");
//		// reloadableResourceBundleMessageSource.setCacheSeconds(1);
//		// reloadableResourceBundleMessageSource.setFallbackToSystemLocale(false);
//		// return reloadableResourceBundleMessageSource;
//
//		// Get System Setting - default language value
//		String defaultLanguage = this.systemService.findAllSystemSettingMap().get("default.language");
//
//		if( StringUtils.isNotBlank(defaultLanguage)){
//			Locale defaultLocale = new Locale( defaultLanguage.split("_")[0], defaultLanguage.split("_")[1] );
//			this.databaseMessageResourceService.setDefaultLocale( defaultLocale );
//		}
//		else{
//			this.databaseMessageResourceService.setDefaultLocale(Locale.ENGLISH.US);
//		}
//
//		// Print all message resource content
//		this.databaseMessageResourceService.printAll();
//
//		return (MessageSource) this.databaseMessageResourceService;
//
//	}
	
	@Bean(name = "passwordEncoder")
	protected Md5PasswordEncoder getPasswordEncoder(){
		return new Md5PasswordEncoder();
	}


	@Bean(name = "baseFileStorageAdaptor")
	protected BaseFileStorageAdaptor getBaseFileStorageAdaptor(){
		return new BaseFileStorageAdaptor(env.getProperty("cityplaza.fileroot"));
	}

	@Bean(name = "fileStorage")
	protected FileStorage getFileStorageAdaptor() {
		return new FileStorage(getBaseFileStorageAdaptor());
	}
//
//	@Bean(name = "localeResolver")
//	protected CookieLocaleResolver getLocaleResolver() {
//		CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
//		cookieLocaleResolver.setCookieName("locale");
//		return cookieLocaleResolver;
//	}
//
////	@Bean(name = "tilesConfigurer")
////	public TilesConfigurer getTilesConfigurer() {
////		TilesConfigurer tilesConfigurer = new TilesConfigurer();
////		tilesConfigurer.setDefinitions("classpath:tiles/tiles.xml");
////		return tilesConfigurer;
////	}
//
//	@Bean(name = "viewResolver")
//	protected ViewResolver getViewResolver() {
//		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
//		viewResolver.setViewClass(JstlView.class);
//		viewResolver.setPrefix("/WEB-INF/views");
//		viewResolver.setSuffix(".jsp");
//		viewResolver.setOrder(2);
//		return viewResolver;
//	}
//
//	@Bean(name = "multipartResolver")
//	protected MultipartResolver getMultipartResolver() {
//		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
//		multipartResolver.setMaxUploadSize(1024 * 1024 * 1024);
//		return multipartResolver;
//	}
//
//	@Override
//	public void configureMessageConverters(List<HttpMessageConverter<?>> httpMessageConverters) {
//		// super.configureMessageConverters(httpMessageConverters);
//		httpMessageConverters.add(this.getMappingJackson2HttpMessageConverter());
//	}
//
//	@Bean(name = "mappingJackson2HttpMessageConverter")
//	protected MappingJackson2HttpMessageConverter getMappingJackson2HttpMessageConverter() {
//		MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
//		mappingJackson2HttpMessageConverter.setObjectMapper(this.getObjectMapper());
//		return mappingJackson2HttpMessageConverter;
//	}
//
//	@Bean(name = "objectMapper")
//	protected ObjectMapper getObjectMapper() {
//		ObjectMapper objectMapper = new ObjectMapper();
//		// objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
//		objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
//		return objectMapper;
//	}

}
