package com.gtomato.projects.base.config.dialect;

import org.hibernate.dialect.MySQL5InnoDBDialect;

public class UTF8MySQL5InnoDBDialect extends MySQL5InnoDBDialect {

	public String getTableTypeString() {
		return " ENGINE=InnoDB DEFAULT CHARSET=utf8";
	}

}
