package com.gtomato.projects.base.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;


@Configuration
@PropertySource("classpath:config/environment.properties")
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	protected Environment environment;


	@Override
	public void configure(WebSecurity web) throws Exception {
		// super.configure(web);
		//web.ignoring().antMatchers(Url.STATIC_CSS_PREFIX + "/**", Url.STATIC_IMAGE_PREFIX + "/**", Url.STATIC_JS_PREFIX + "/**" , Url.STATIC_PLUGINS_PREFIX +"/**");
		web.ignoring().antMatchers(Url.STATIC_PREFIX + "/**", Url.STATIC_PLUGINS_PREFIX +"/**");
		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// super.configure(http);

		
		http.csrf().disable() //

				.headers() //
				.contentTypeOptions().and() //
				.xssProtection().and() //
				.cacheControl().and() //
				.httpStrictTransportSecurity().and() //
//				.frameOptions() //
				
				.and() //
				.authorizeRequests() //
				.antMatchers(Url.API_PREFIX + "/**").permitAll();//

				http.authorizeRequests().anyRequest()
				//.denyAll() //
				.authenticated(); //


//
	}

}
