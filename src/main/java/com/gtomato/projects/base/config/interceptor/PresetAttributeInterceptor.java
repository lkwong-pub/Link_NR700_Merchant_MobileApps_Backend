package com.gtomato.projects.base.config.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

//import com.gtomato.projects.base.config.Constant;
//import com.gtomato.projects.base.config.Url;

public class PresetAttributeInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//		request.setAttribute("appPath", Constant.getInstance().getProperty("app.path"));
//		request.setAttribute("resPath", Constant.getInstance().getProperty("res.path"));
//		request.setAttribute("projectName", Constant.getInstance().getProperty("project.name"));
//		request.setAttribute("indexPath", Constant.getInstance().getProperty("app.path")  + Url.ADMIN_PREFIX + Url.ADMIN_INDEX_PAGE);
//		request.setAttribute("url", Constant.getInstance().getProperty("project.url"));
//		request.setAttribute("global", Constant.getInstance().getSystemSettingMap());
		return true;
	}
}
