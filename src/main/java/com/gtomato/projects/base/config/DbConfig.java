//package com.gtomato.projects.base.config;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.persistence.EntityManagerFactory;
//
//import com.gtomato.projects.base.service.DatabaseMessageResourceService;
//import com.gtomato.projects.base.service.SystemService;
//import liquibase.integration.spring.SpringLiquibase;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.context.annotation.PropertySources;
//import org.springframework.core.env.Environment;
//import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
//import org.springframework.data.auditing.DateTimeProvider;
//import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.JpaVendorAdapter;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//@Configuration
//@EnableJpaAuditing(dateTimeProviderRef = "dateTimeProvider")
//@PropertySources(value = { @PropertySource({ "classpath:config/datasource.properties" }) })
//@EnableTransactionManagement
//@EnableJpaRepositories("com.gtomato.projects.*.repository")
//public class DbConfig extends BaseDbConfig implements InitializingBean {
//
//    private Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    @Autowired
//    protected DataSourceConfig dataSourceConfig;
//
//    @Autowired
//    protected Environment environment;
//
//    @Bean(name="liquibase")
//    public SpringLiquibase liquibase() {
//        SpringLiquibase liquibase = new SpringLiquibase();
//        liquibase.setChangeLog("classpath:config/changelog.xml");
//        liquibase.setDataSource(dataSourceConfig.getDataSource());
//        return liquibase;
//    }
//
//    @Override
//    public void afterPropertiesSet() throws Exception {
////        Boolean isMigration = Boolean.valueOf(this.environment.getProperty("process.migration"));
//////        logger.info("Process Migration: " + processMigration);
////        if(Boolean.valueOf(isMigration)) {
////            this.liquibase(); //Trigger Migration
////        }
//        logger.info("DBConfig Ready");
//    }
//
//	/*@Autowired
//	protected Environment environment;
//
//	@Autowired
//	protected BaseDataSourceConfig dataSourceConfig;
//
//	@Bean(name = "jpaVendorAdapter")
//	protected JpaVendorAdapter getJpaVendorAdapter() {
//		AbstractJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
//		jpaVendorAdapter.setDatabasePlatform(this.environment.getProperty("hibernate.dialect"));
//		jpaVendorAdapter.setShowSql(this.environment.getProperty("hibernate.show.sql", Boolean.class));
//		return jpaVendorAdapter;
//	}
//
//	@Bean(name = "entityManagerFactory")
//	protected LocalContainerEntityManagerFactoryBean getEntityManagerFactory() {
//		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
//		entityManagerFactory.setPackagesToScan("com.gtomato.projects.*.entity");
//		entityManagerFactory.setJpaVendorAdapter(getJpaVendorAdapter());
//		entityManagerFactory.setDataSource(this.dataSourceConfig.getDataSource());
//		entityManagerFactory.setJpaPropertyMap(this.getJpaProperties());
//		return entityManagerFactory;
//	}
//
//	@Bean(name = "transactionManager")
//	protected PlatformTransactionManager getTransactionManager(EntityManagerFactory entityManagerFactory) {
//		JpaTransactionManager transactionManager = new JpaTransactionManager();
//		transactionManager.setEntityManagerFactory(entityManagerFactory);
//		return transactionManager;
//	}
//
//	@Bean(name = "exceptionTranslation")
//	protected PersistenceExceptionTranslationPostProcessor getExceptionTranslation() {
//		return new PersistenceExceptionTranslationPostProcessor();
//	}
//
//	protected Map<String, String> getJpaProperties() {
//		Map<String, String> jpaProperties = new HashMap<String, String>();
//		// jpaProperties.put("hibernate.connection.charSet", "UTF-8");
//		// jpaProperties.put("hibernate.connection.characterEncoding", "UTF-8");
//		// jpaProperties.put("hibernate.connection.useUnicode", "true");
//		jpaProperties.put(org.hibernate.cfg.Environment.DIALECT, this.environment.getRequiredProperty("hibernate.dialect"));
//		jpaProperties.put(org.hibernate.cfg.Environment.FORMAT_SQL, this.environment.getRequiredProperty("hibernate.format.sql"));
//		jpaProperties.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, this.environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
//		jpaProperties.put(org.hibernate.cfg.Environment.SHOW_SQL, this.environment.getRequiredProperty("hibernate.show.sql"));
//		return jpaProperties;
//	}
//
//	@Bean
//	protected DateTimeProvider dateTimeProvider() {
//        return new AuditingDateTimeProvider();
//    }*/
//}
