package com.gtomato.projects.base.config;

import com.gtomato.projects.base.config.interceptor.aop.AopApiRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class AopConfig {

	@Bean
	public AopApiRequestInterceptor aopApiRequestInterceptor(){
		return new AopApiRequestInterceptor();
	}

}
