package com.gtomato.projects.base.config;

public final class Path {

	
	/******************** Project ********************/
	public static final String VERSION_PATH_V1 = "/v1";

	public static final String GLOBAL_ACCESS_DENIED = "/403";
	
	public static final String MODULE_ADMIN_PATH = "/admin";

	public static final String MODULE_ROLE_PATH = "/role";
	
	public static final String MODULE_PERMISSION_PATH = "/permission";

	public static final String MODULE_AUDIT_LOG_PATH = "/auditlog";

	public static final String MODULE_GIFT_PATH = "/gift";

	public static final String MODULE_MEMBER_PATH = "/member";

	/******************** API ********************/

	public static final String MODULE_CHECK_REFERRAL_NO_PATH = "/checkReferralno";


}