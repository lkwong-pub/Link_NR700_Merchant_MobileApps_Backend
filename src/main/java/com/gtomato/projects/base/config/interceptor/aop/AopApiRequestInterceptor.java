package com.gtomato.projects.base.config.interceptor.aop;



import com.gtomato.projects.linkreit.service.MemberService;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by doncheung on 31/10/2017.
 */
@Aspect
public class AopApiRequestInterceptor {

    Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private MemberService memberService;


    @Around("within(com.gtomato.projects.linkreit.controller.api..*)")
    public Object aroundActionHandle(ProceedingJoinPoint point) throws Throwable{

        Object proceedRes = null;
        try {
            proceedRes = point.proceed();
            return proceedRes;
        } catch (Throwable e) {
            throw e;
        }
    }


}
