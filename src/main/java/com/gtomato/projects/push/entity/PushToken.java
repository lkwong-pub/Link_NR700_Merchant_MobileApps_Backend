package com.gtomato.projects.push.entity;


import com.gtomato.projects.push.responseObject.api.BasePushToken;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "push_token")
@Data
public class PushToken implements BasePushToken {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "user_id")
	private Integer userId;
	
	@Column
	private String locale;
	
	@Column(unique = true)
	private String token;
	
	@Column(name = "device_id", unique = true)
	private String deviceId;
	
	@Column(name = "device_type")
	private Integer deviceType;
	
	@CreatedDate
	@Column(name = "create_time")
	private Date createTime;

	@LastModifiedDate
	@Column(name = "last_update_time")
	private Date lastUpdateTime;
	
	@Column(name = "country_id")
	private Integer countryId;
	
	@Column
	private Integer status;
	
//	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "pushTokens")
//	public List<PushMessage> pushMessages;

	@Override
	public String toString() {
		return "PushToken [id=" + id + ", userId=" + userId + ", locale="
				+ locale + ", token=" + token + ", deviceId=" + deviceId
				+ ", deviceType=" + deviceType + ", createTime=" + createTime
				+ ", lastUpdateTime=" + lastUpdateTime + ", countryId="
				+ countryId + ", status=" + status +  "]";
	}

	@PrePersist
	void prePersist(){
		this.setCreateTime( new Date() );
	}
	
	@PreUpdate
	void preUpdate(){
		this.setLastUpdateTime( new Date() );
	}


	public Integer getDeviceTypeInt() {
		return this.deviceType;
	}


	public void setDeviceTypeInt(Integer deviceTypeInt) {
		this.deviceType = deviceTypeInt;
	}

	public Long getMemberId(){
		return this.userId == null ? 0L : Long.parseLong(this.userId.toString());
	};

}
