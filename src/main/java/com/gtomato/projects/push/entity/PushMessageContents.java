package com.gtomato.projects.push.entity;


import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "push_message_contents")
@Data
public class PushMessageContents{

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column
	private String locale;

	@Column
	private String title;
	
	@Column
	private String content;
	

	@ManyToOne(fetch=FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="master_id")
	private PushMessage pushMessage;


	@Override
	public String toString() {
		return "PushMessageContents [id=" + id + ", locale=" + locale
				+ ", title=" + title + ", content=" + content
				+ ", masterId=" + (pushMessage == null ? 0 : pushMessage.getId()) + "]";
	}

	
	
}
