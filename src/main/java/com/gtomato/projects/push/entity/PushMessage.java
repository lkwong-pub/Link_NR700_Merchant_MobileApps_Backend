package com.gtomato.projects.push.entity;


import com.gtomato.projects.base.entity.BaseEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "push_message")
@Data
@NamedQuery(name="PushMessage.findByKeyword", query="SELECT c.pushMessage.id FROM PushMessageContents c where content like :keyword") 
public class PushMessage extends BaseEntity implements Serializable{

	@Column
	private Integer priority;

	@Column(name = "region_id", nullable = true)
	private Integer regionId ;

	@Column(name = "target_type")
	private Integer targetType;
	
	@Column(name = "schedule_time")
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date scheduleTime;
	
	@Column(name = "push_status")
	private Integer pushStatus;
	
	@Column(name = "last_offset")
	private Integer lastOffset;
	
	@Column(name = "redirect_details")
	private String redirectDetails;

	@Column(name="push_config_id")
	private Integer pushConfigId;

	@OneToMany(mappedBy="pushMessage", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<PushMessageContents> contents;

//	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.REFRESH })
//	@JoinTable(name = "push_message_token_mapping", joinColumns = { 
//			@JoinColumn(name = "push_message_id", nullable = false, updatable = false) }, 
//			inverseJoinColumns = { @JoinColumn(name = "push_token_id", 
//					nullable = false, updatable = false) })
//	@BatchSize(size=1)
//	private List<PushToken> pushTokens;

	@Override
	public String toString() {
		return "PushMessage [priority=" + priority + ", targetType="
				+ targetType + ", scheduleTime=" + scheduleTime
				+ ", pushStatus=" + pushStatus + ", lastOffset=" + lastOffset
				+ ", redirectDetails=" + redirectDetails  +", contents=" + contents
				+ "]";
	} 
	

	
}
