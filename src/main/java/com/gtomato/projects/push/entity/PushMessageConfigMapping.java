package com.gtomato.projects.push.entity;


import com.gtomato.projects.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "push_message_config_mapping")
@Data
//@NamedQuery(name="PushMessage.findByKeyword", query="SELECT c.pushMessage.id FROM PushMessageContents c where content like :keyword")
@SequenceGenerator(name="SEQ", sequenceName="PUSH_CONFIG_SEQ", allocationSize=1)
public class PushMessageConfigMapping extends BaseEntity implements Serializable{


    @Column(name="app_name")
    private String appName;

    @Column(name = "region_id")
    private Integer regionId ;

    @Column(name="ios_app_id")
    private String iosAppId;

    @Column(name="android_app_id")
    private String androidAppId;

    @Column(name="ios_push_cert")
    private String iosPushCert;

    @Column(name="ios_push_password")
    private String iosPushPassword;

    @Column(name="ios_push_topic")
    private String iosPushTopic;

    @Column(name="ios_is_production")
    private String iosIsProduction;

    @Column(name="android_push_key")
    private String androidPushKey;

    @Column(name="is_default")
    private Boolean isDefault;

    @Override
    public String toString() {
        return "PushMessageConfigMapping{" +
                "appName='" + appName + '\'' +
                ", regionId=" + regionId +
                ", iosAppId='" + iosAppId + '\'' +
                ", androidAppId='" + androidAppId + '\'' +
                ", iosPushCert='" + iosPushCert + '\'' +
                ", iosPushPassword='" + iosPushPassword + '\'' +
                ", iosPushTopic='" + iosPushTopic + '\'' +
                ", iosIsProduction='" + iosIsProduction + '\'' +
                ", androidPushKey='" + androidPushKey + '\'' +
                ", isDefault=" + isDefault +
                '}';
    }
}
