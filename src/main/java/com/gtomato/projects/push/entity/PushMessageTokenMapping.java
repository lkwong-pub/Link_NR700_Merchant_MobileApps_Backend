package com.gtomato.projects.push.entity;


import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "push_message_token_mapping")
@Data
public class PushMessageTokenMapping{

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "push_message_id")
	private Integer pushMessageId;
	
	@Column(name = "push_token_id")
	private Integer pushTokenId;
	
}
