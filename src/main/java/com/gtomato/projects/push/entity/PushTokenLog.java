package com.gtomato.projects.push.entity;


import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "push_token_log")
@Data
public class PushTokenLog{

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name="token")
	private String token;

	@Column(name = "device_type")
	private Integer deviceType;

	@Column(name = "push_message_id")
	private Integer pushMessageId;

	@Column(name = "push_message_type")
	private Integer pushMessageType;

	@Column(name ="is_sent")
	private Boolean isSent;

	@Column(name ="is_success")
	private Boolean isSuccess;

	@Column(name = "send_datetime")
	private Date sendDatetime;

	@Column
	private String remarks;

}
