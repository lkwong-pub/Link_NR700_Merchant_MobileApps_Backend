package com.gtomato.projects.push.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "push_message_log")
@Data
public class PushMessageLog{

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "start_time")
	private Date startTime;

	@Column(name = "end_time")
	private Date endTime;
	
	@Column(name = "ios_success_count")
	private Integer iosSuccessCount;

	@Column(name = "android_success_count")
	private Integer androidSuccessCount;
	
	@Column(name = "ios_fail_count")
	private Integer iosFailCount;
	
	@Column(name = "android_fail_count")
	private Integer androidFailCount;
	
	@Column(name = "ios_expect_count")
	private Integer iosExpectCount;
	
	@Column(name = "android_expect_count")
	private Integer androidExpectCount;
	
}
