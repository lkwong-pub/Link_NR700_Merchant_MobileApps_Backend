package com.gtomato.projects.push.service.impl;

import com.gtomato.projects.base.config.Constant;
import com.gtomato.projects.base.service.impl.BaseServiceImpl;
import com.gtomato.projects.push.entity.*;
import com.gtomato.projects.push.repository.*;
import com.gtomato.projects.push.responseObject.api.BaseTokenReqObject;
import com.gtomato.projects.push.responseObject.search.PushSearchCriteria;
import com.gtomato.projects.push.responseObject.service.TokenReqObject;
import com.gtomato.projects.push.service.PushMessageService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service("PushMessageService")
@Transactional
public class PushMessageServiceImpl extends BaseServiceImpl<PushMessage, PushSearchCriteria> implements PushMessageService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PushMessageRepository pushMessageRepository;
	
	@Autowired
	private PushTokenRepository pushTokenRepository;

	@Autowired
	private PushTokenLogRepository pushTokenLogRepository;
	
	@Autowired
	private PushMessageTokenMappingRepository pushMessageTokenMappingRepository;

	@Autowired
	private PushMessageConfigMappingRepository pushMessageConfigMappingRepository;

	private List<PushMessageConfigMapping> pushMessageConfigMappingList;

	@Autowired
	public PushMessageServiceImpl(PushMessageRepository pushMessageRepository, PushMessageConfigMappingRepository pushMessageConfigMappingRepository){
		super(pushMessageRepository);
		this.pushMessageRepository = pushMessageRepository;
		this.pushMessageConfigMappingRepository = pushMessageConfigMappingRepository;
		this.updatePushMessageConfigMapping();
	}
	
	public void updatePendingPushStatus(){
		this.pushMessageRepository.updatePendPushStatus(new Date());
	};
	
	public List<PushMessage> getListByStatusOrderByPriority(Collection<Integer> status, Integer size){
		Pageable pageable = new PageRequest( 0, size, new Sort(new Order(Direction.fromString("asc"), "priority")));
		return this.pushMessageRepository.getByPushStatusIn(status, pageable);
	};
	
	public Page<Integer> findPushTokenIdByPushMessageId(Integer pushMessageId, Integer page, Integer size){
		Pageable pageable = new PageRequest( page, size);
		return this.pushMessageTokenMappingRepository.findPushTokenIdByPushMessageId(pushMessageId, pageable);
	};
	
	public List<PushToken> findPushTokenByIdIn(Collection<Integer> ids){
		return this.pushTokenRepository.findByIdIn(ids);
	};
	
	public Page<PushToken> findPushTokenByTokenIsNotNull(Integer page, Integer size) {
		Pageable pageable = new PageRequest(page, size);
		return this.pushTokenRepository.findByTokenIsNotNull(pageable);
	}
	
	public List<PushToken> findPushTokenByUserIdIn(Collection<Integer> userIds){
		return this.pushTokenRepository.findByUserIdIn(userIds);
	};

	public void saveOrUpdatePushMessageTokenMapping(List<PushMessageTokenMapping> pushMessageTokenMappings){
		this.pushMessageTokenMappingRepository.save(pushMessageTokenMappings);
	};
	
	
	public void addNUpdatePushToken( BaseTokenReqObject tokenReqObject ){
		//PushToken Checking
		
		if(StringUtils.isNotBlank(tokenReqObject.getPushToken())){
			PushToken pushToken = this.pushTokenRepository.findBydeviceId(tokenReqObject.getDeviceId());
			if( pushToken == null ){
				pushToken = new PushToken();

			}
			pushToken.setStatus(Constant.getInstance().getIntegerProperty("status.active"));

			//sign up / sign in
			if( pushToken.getUserId() != null && pushToken.getDeviceId() != null ){
				//Same Device, Different User
				if(pushToken.getUserId().intValue() != tokenReqObject.getUserId().intValue() &&
						pushToken.getDeviceId().equals( tokenReqObject.getDeviceId() )	){
					pushToken.setUserId(tokenReqObject.getUserId());
				}
				//Different Device, Same User
				else if ( pushToken.getUserId().intValue() == tokenReqObject.getUserId().intValue() &&
						!pushToken.getDeviceId().equals( tokenReqObject.getDeviceId() )	){
					if(tokenReqObject.isSingleLogin()){
						pushToken.setUserId(null);
						this.pushTokenRepository.save(pushToken);
						pushToken = new PushToken();
						pushToken.setUserId(tokenReqObject.getUserId());
					}
				}
			}
			
			if(tokenReqObject.getCountryId() != null)
				pushToken.setCountryId( tokenReqObject.getCountryId() );
			
			pushToken.setToken(tokenReqObject.getPushToken());
			pushToken.setLocale( tokenReqObject.getLocale() );
			pushToken.setDeviceType( tokenReqObject.getDeviceType() );
			pushToken.setDeviceId( tokenReqObject.getDeviceId() );

			this.pushTokenRepository.save(pushToken);
		}
		
		
	};
	
	public void removePushToken( BaseTokenReqObject tokenReqObject ){
			if(TokenReqObject.REQUEST_TYPE_LOGOUT.equals(tokenReqObject.getRequestType())){
				this.pushTokenRepository.removeUserIdByDeviceId(tokenReqObject.getDeviceId());
			}
			else if(TokenReqObject.REQUEST_TYPE_NOTIFICATION_CONTROL.equals(tokenReqObject.getRequestType())){
				this.pushTokenRepository.updateStatus(tokenReqObject.getDeviceId(), tokenReqObject.getNotificationStatus());
			}	
	};

	public void updatePushMessageConfigMapping(){
		List<PushMessageConfigMapping> pushMessageConfigMappingList = this.pushMessageConfigMappingRepository.findAll();
		this.pushMessageConfigMappingList = pushMessageConfigMappingList;
	}

	public List<PushMessageConfigMapping> getPushMessageConfigMappingList(){
		return pushMessageConfigMappingList;
	}

	public PushMessageConfigMapping getPushMessageConfigMappingById( Integer id ) {
		for(PushMessageConfigMapping pushMessageConfigMapping : pushMessageConfigMappingList){
			if(pushMessageConfigMapping.getId() == id ){
				return pushMessageConfigMapping;
			}
		}
		return null;
	}

	public PushMessageConfigMapping getPushMessageConfigMappingByAppId( String appId ) {
		for(PushMessageConfigMapping pushMessageConfigMapping : pushMessageConfigMappingList){
			if(pushMessageConfigMapping.getAndroidAppId().equals( appId ) || pushMessageConfigMapping.getIosAppId().equals( appId )){
				return pushMessageConfigMapping;
			}
		}
		return null;
	};

	public PushMessageConfigMapping getPushMessageConfigMappingByRegionId( Integer regionId ) {
		for(PushMessageConfigMapping pushMessageConfigMapping : pushMessageConfigMappingList){
			if(pushMessageConfigMapping.getRegionId() == regionId){
				return pushMessageConfigMapping;
			}
		}
		return null;
	};

	public PushMessageConfigMapping getDefaultPushMessageConfigMapping() {
		for(PushMessageConfigMapping pushMessageConfigMapping : pushMessageConfigMappingList){
			if(pushMessageConfigMapping.getIsDefault()){
				return pushMessageConfigMapping;
			}
		}
		return null;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public int updatePushMessageOffset(PushMessage pushMessage, int page){
		return pushMessageRepository.updateLastOffset(pushMessage.getId(), page);
	}

	public List<PushToken> findActivePushTokenByIdIn(Collection<Integer> ids){
		return this.pushTokenRepository.findByIdInAndStatus(ids, 0);
	};

	public Page<PushToken> findActivePushTokenByTokenIsNotNull(Integer page, Integer size) {
		Pageable pageable = new PageRequest(page, size);
		return this.pushTokenRepository.findByTokenIsNotNullAndStatus(pageable, 0);
	}

	public PushToken findPushTokenByToken( String token ){
		return this.pushTokenRepository.findByToken(token);
	};

	public void savePushTokenList(List<PushToken> pushTokenList){
		this.pushTokenRepository.save(pushTokenList);
	};

	public void savePushTokenLogList(List<PushTokenLog> pushTokenLogList){
		this.pushTokenLogRepository.save(pushTokenLogList);
	};
}
