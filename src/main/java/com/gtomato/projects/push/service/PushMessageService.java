package com.gtomato.projects.push.service;

import com.gtomato.projects.base.service.BaseService;
import com.gtomato.projects.push.entity.*;
import com.gtomato.projects.push.responseObject.api.BaseTokenReqObject;
import com.gtomato.projects.push.responseObject.search.PushSearchCriteria;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

public interface PushMessageService extends BaseService<PushMessage, PushSearchCriteria> {
	
	void updatePendingPushStatus();
	
	List<PushMessage> getListByStatusOrderByPriority(Collection<Integer> status, Integer size);
	
	Page<Integer> findPushTokenIdByPushMessageId(Integer pushMessageId, Integer page, Integer size);
	
	List<PushToken> findPushTokenByIdIn(Collection<Integer> ids);
	
	Page<PushToken> findPushTokenByTokenIsNotNull(Integer page, Integer size);
	
	List<PushToken> findPushTokenByUserIdIn(Collection<Integer> userId);
	
	void saveOrUpdatePushMessageTokenMapping(List<PushMessageTokenMapping> pushMessageTokenMappings);
	
	/**
	 * Add / Update Push Token by following case
	 * Query PushToken By 'deviceId' 
	 * 		if PushToken not exist
	 * 				Create new PushToken Object with create time
	 * 		if PushToken exist
	 * 				Check request type
	 * 
	 * 					if type = LAUNCH
	 * 						set parameter to PushToken Object 
	 * 							-locale
	 * 							-deviceId
	 * 							-token
	 * 							-deviceType
	 * 					
	 * 						type = REGION
	 * 						set parameter to PushToken Object 
	 * 							-locale
	 * 							-deviceId
	 * 							-token
	 * 							-deviceType
	 * 							-countryId
	 * 						
	 * 						type = SIGNIN
	 * 						set parameter to PushToken Object 
	 * 							-locale
	 * 							-deviceId
	 * 							-token
	 * 							-deviceType
	 * 							-countryId [ if parameter: countryId value != null ]
	 * 							-userId[ if parameter: userId value != null ]
	 * 				
	 * 						type = LOGIN
	 * 							if parameter - isSingleLogin is true
	 * 								Check parameter and data 
	 * 								1. UserId & deviceId is equals 
	 *								set parameter to PushToken Object 
	 * 									-locale
	 * 									-deviceId
	 * 									-token
	 * 									-deviceType
	 * 									-countryId 
	 * 									-userId
	 * 							**	2. UserId is equals but deviceId is not equals
	 * 								set existing data UserId to null
	 * 								Create new full set
	 * 								3. UserId is not equals but deviceId is equals
	 * 								set parameter to PushToken Object 
	 * 									-locale
	 * 									-deviceId
	 * 									-token
	 * 									-deviceType
	 * 									-countryId 
	 * 									-userId 
	 * 								 
	 * 
	 * @param tokenReqObject
	 */
	void addNUpdatePushToken(BaseTokenReqObject tokenReqObject);
	
	/**
	 * 2 case
	 * 1. type = LOGOUT
	 * 	Query PushToken By 'deviceId' 
	 *  Set userId = null
	 * 2. type = NOTIFICATION_CONTROL
	 * 	set status = 0(ON) or 1(OFF)
	 * 
	 * @param tokenReqObject
	 */
	void removePushToken(BaseTokenReqObject tokenReqObject);

	public List<PushMessageConfigMapping> getPushMessageConfigMappingList();

	public PushMessageConfigMapping getPushMessageConfigMappingById(Integer id);

	public PushMessageConfigMapping getPushMessageConfigMappingByAppId(String appId);

	public PushMessageConfigMapping getPushMessageConfigMappingByRegionId(Integer regionId);

	public PushMessageConfigMapping getDefaultPushMessageConfigMapping();

	int updatePushMessageOffset(PushMessage pushMessage, int page);

	List<PushToken> findActivePushTokenByIdIn(Collection<Integer> ids);

	Page<PushToken> findActivePushTokenByTokenIsNotNull(Integer page, Integer size);

	PushToken findPushTokenByToken(String token);

	void savePushTokenList(List<PushToken> pushTokenList);

	void savePushTokenLogList(List<PushTokenLog> pushTokenLogList);
}
