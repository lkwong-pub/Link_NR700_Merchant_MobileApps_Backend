package com.gtomato.projects.push.properties;

import lombok.Getter;

import java.util.HashMap;

@Getter
public enum PushMessageTargetType {

	
	SEND_TO_ALL(1, "All"),
	SEND_TO_SPECIFIC(2,"Specific");
	
	private int value;
	private String name;
	
	
	PushMessageTargetType( int value, String name ){
		this.value = value;
		this.name = name;
	}
	
	public static String getName( int value ){
		for(PushMessageTargetType pushMessageValue : PushMessageTargetType.values()){
			if(pushMessageValue.value == value )
				return pushMessageValue.name;
		}
		return null;
	};
	
	public static HashMap getMap(){
		HashMap map = new HashMap();
		for(PushMessageTargetType pushMessageValue : PushMessageTargetType.values())
			map.put(pushMessageValue.getValue(), pushMessageValue.getName());
		return map;
	}
}
