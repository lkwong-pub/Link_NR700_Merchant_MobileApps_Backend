package com.gtomato.projects.push.properties;

import lombok.Getter;

import java.util.HashMap;

@Getter
public enum PushMessagePriority {

	HIGH(1, "High"),
	MIDDLE(2,"Middle"),
	LOW(3,"Low");
	
	private int value;
	private String name;
	
	
	PushMessagePriority( int value, String name ){
		this.value = value;
		this.name = name;
	}
	
	public static String getName( int value ){
		for(PushMessagePriority pushMessageValue : PushMessagePriority.values()){
			if(pushMessageValue.value == value )
				return pushMessageValue.name;
		}
		return null;
	};
	
	public static HashMap getMap(){
		HashMap map = new HashMap();
		for(PushMessagePriority pushMessageValue : PushMessagePriority.values())
			map.put(pushMessageValue.getValue(), pushMessageValue.getName());
		return map;
	}
}
