package com.gtomato.projects.push.properties;

import lombok.Getter;

@Getter
public enum PushMessageStatus {

	PENDING(1, "Pending"),
	READY_TO_PUSH(2,"Ready To Push"),
	PROCESSING(3,"Processing"),
	FINISHED(4,"Finished"),
	ERROR(5,"Error"),
	CANCELLED(6, "Cancelled");
	
	private int value;
	private String name;
	
	
	PushMessageStatus( int value, String name ){
		this.value = value;
		this.name = name;
	}
	
	public static String getName( int value ){
		for(PushMessageStatus pushMessageValue : PushMessageStatus.values()){
			if(pushMessageValue.value == value )
				return pushMessageValue.name;
		}
		return null;
	};
}
