package com.gtomato.projects.push.properties;

import lombok.Getter;

import java.util.HashMap;

@Getter
public enum PushMessageDeviceType {

	IOS(1, "IOS"),
	ANDROID(2,"Android");

	private int value;
	private String name;


	PushMessageDeviceType(int value, String name ){
		this.value = value;
		this.name = name;
	}
	
	public static String getName( int value ){
		for(PushMessageDeviceType pushMessageDeviceType : PushMessageDeviceType.values()){
			if(pushMessageDeviceType.value == value )
				return pushMessageDeviceType.name;
		}
		return null;
	};
	
	public static HashMap getMap(){
		HashMap map = new HashMap();
		for(PushMessageDeviceType pushMessageDeviceType : PushMessageDeviceType.values())
			map.put(pushMessageDeviceType.getValue(), pushMessageDeviceType.getName());
		return map;
	}
}
