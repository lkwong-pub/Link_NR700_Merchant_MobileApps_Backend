package com.gtomato.projects.push.scheduler;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import com.gtomato.projects.push.properties.PushMessageStatus;
import com.gtomato.projects.push.properties.PushMessageTargetType;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gtomato.projects.push.config.notification.PushSender;
import com.gtomato.projects.push.entity.PushMessage;
import com.gtomato.projects.push.entity.PushToken;
import com.gtomato.projects.push.service.PushMessageService;

//@PropertySources(value = { @PropertySource({ "classpath:config/scheduler.properties" }) })
@Component("pushNotificationScheduler")
//@Service("pushNotificationScheduler")
public class PushNotificationScheduler implements InitializingBean{

	private static final Integer pushMessageSize = 100;

	private static final Integer pushTokenSize = 500;
	
	private static Logger LOG = Logger.getLogger(PushNotificationScheduler.class);
	
	@Autowired
	private PushSender<PushToken> pushSender;

	@Autowired
	private PushMessageService pushMessageService;
	
	@Autowired
	private Environment environment;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.pushSender.updatePushNotificationMapping(this.pushMessageService.getPushMessageConfigMappingList());
		this.pushSender.connectPushNotificationProvider();
		this.sendPushNotification();

	}

	@Scheduled(cron = "0,10,20,30,40,50 * * * * *")
	public void execute() {
		//if (this.environment.getProperty("pushNotificationScheduler.enabled", "n").equals("y"))
			this.sendPushNotification();
	}

	/**
	 * Reconnect APNS Service per 30 min (Goals: < 60 mins since throwing exception after 60 mins)
	 *
	 */
	@Scheduled(fixedDelay = 30 * 60 * 1000)
	public void execute2() {
		this.rebuildPushNotifictionSender();
	}

	private void rebuildPushNotifictionSender(){
		this.pushSender.reconnectPushNotificationProvider();
	}

	private void sendPushNotification() {

		//TODO criteria?
		/*
		 * Update Push Message Status from "Pending" to "Ready To Push" if
		 * 		1. Push Time is not null
		 * 		2. Publish Time <= Now 
		 */
		this.pushMessageService.updatePendingPushStatus();

		/*
		 * Query "Ready To Send" and "Processing" Push Message 
		 * 
		 */
		List<PushMessage> pushMessages = this.pushMessageService.getListByStatusOrderByPriority(Arrays.asList(PushMessageStatus.READY_TO_PUSH.getValue(),PushMessageStatus.PROCESSING.getValue()), pushMessageSize);
		LOG.debug("No of push message: "+ ( pushMessages == null ? 0 : pushMessages.size() ) );


		/*
		 * Push Message List Empty Checking
		 */
		if(pushMessages == null || pushMessages.isEmpty())
			return;

		/*
		 * Send Push Message Procedure
		 * 1. Update push message status to "Processing"
		 * 2. Query Push Token By Push Type
		 * 		a) To all -> Query all push token by batch
		 * 		b) to specific user -> Query push token user mapping -> query token by token id
		 * 3. Call Send Push Method -> push thread to thread pool
		 */
		for (PushMessage pushMessage : pushMessages) {
			try{
				LOG.debug("Push Message (ID:"+pushMessage.getId()+") Start." );
				pushMessage.setPushStatus(PushMessageStatus.PROCESSING.getValue());
				this.pushMessageService.saveOrUpdate(pushMessage);

				Integer page = pushMessage.getLastOffset() == null ? 0 : pushMessage.getLastOffset();

				Page<PushToken> pushTokenPage = null;
				Page<Integer> pushTokenIdList = null;
				do {
					try{
						if( pushMessage.getTargetType() == PushMessageTargetType.SEND_TO_ALL.getValue() ){
							pushTokenPage = this.pushMessageService.findActivePushTokenByTokenIsNotNull(page, pushTokenSize);
							this.pushSender.sendPushNotification(pushTokenPage.getContent(), pushMessage);
						}

						else if ( pushMessage.getTargetType() == PushMessageTargetType.SEND_TO_SPECIFIC.getValue() ){
							pushTokenIdList = this.pushMessageService.findPushTokenIdByPushMessageId(pushMessage.getId(), page, pushTokenSize);
							if(pushTokenIdList.getContent() != null && pushTokenIdList.getContent().size()>0){
								List<PushToken> tokenList = this.pushMessageService.findActivePushTokenByIdIn(pushTokenIdList.getContent());
								this.pushSender.sendPushNotification(tokenList, pushMessage);
							}
						}
						LOG.debug("Push Message - Token " + (page*pushTokenSize) +"-" +((page+1)*pushTokenSize) +" sent.");
					}catch(Exception e){
						e.printStackTrace();
						LOG.debug("Push Message - Token " + (page*pushTokenSize) +"-" +((page+1)*pushTokenSize) +" sent fail.");
					}
					page++;
					pushMessage.setLastOffset(page);
					this.pushMessageService.saveOrUpdate(pushMessage);
				} while ( (pushTokenPage != null && pushTokenPage.hasNext() ) ||
						(pushTokenIdList != null && pushTokenIdList.hasNext() ));
				LOG.debug("Push Message (ID:"+pushMessage.getId()+") End." );
				pushMessage.setPushStatus(PushMessageStatus.FINISHED.getValue());
				this.pushMessageService.saveOrUpdate(pushMessage);
			}catch(Exception e){
				LOG.debug("Push Message (ID:"+pushMessage.getId()+") Error Occurred." );
				pushMessage.setPushStatus(PushMessageStatus.ERROR.getValue());
				this.pushMessageService.saveOrUpdate(pushMessage);
				e.printStackTrace();
			}
		}

		LOG.debug("Push Message Scheduler End.");

	}



}
