package com.gtomato.projects.push.repository;

import com.gtomato.projects.base.repository.BaseRepository;
import com.gtomato.projects.push.entity.PushToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface PushTokenRepository extends BaseRepository<PushToken> {
	
	public PushToken findBydeviceId(String deviceId);

	public PushToken findByToken(String token);

	public List<PushToken> findByUserId(Integer userId);

	public Page<PushToken> findByTokenIsNotNull(Pageable pageable);
	
	public List<PushToken> findByIdIn(@Param("id") Collection<Integer> id);
	
	public List<PushToken> findByUserIdIn(Collection<Integer> userIds);
	
	@Modifying
    @Query("UPDATE PushToken t SET t.userId = null, t.status = 0 WHERE t.deviceId = :deviceId")
    public int removeUserIdByDeviceId(@Param("deviceId") String deviceId);
	
	@Modifying
    @Query("UPDATE PushToken t SET t.status = :status WHERE t.deviceId = :deviceId")
    public int updateStatus(@Param("deviceId") String deviceId, @Param("status") Integer status);

	public List<PushToken> findByIdInAndStatus(@Param("id") Collection<Integer> id, @Param("status") Integer status);

	public Page<PushToken> findByTokenIsNotNullAndStatus(Pageable pageable, @Param("status") Integer status);
	
}
