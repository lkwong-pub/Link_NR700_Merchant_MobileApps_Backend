package com.gtomato.projects.push.repository;

import com.gtomato.projects.push.entity.PushMessage;
import com.gtomato.projects.push.entity.PushMessageContents;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PushMessageContentsRepository extends JpaRepository<PushMessageContents, Integer>, JpaSpecificationExecutor {

    @Query("SELECT t FROM PushMessageContents t WHERE t.pushMessage = :pushMessage and t.locale = :locale and rowNum <= 1")
    public PushMessageContents findOneByPushMessageAndLocale(@Param("pushMessage") PushMessage pushMessage, @Param("locale") String locale);

}
