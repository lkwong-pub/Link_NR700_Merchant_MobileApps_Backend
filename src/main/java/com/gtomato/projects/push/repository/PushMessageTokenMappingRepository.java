package com.gtomato.projects.push.repository;

import com.gtomato.projects.push.entity.PushMessageTokenMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PushMessageTokenMappingRepository extends JpaRepository<PushMessageTokenMapping, Integer> {
	
	@Query("select pushTokenId from PushMessageTokenMapping where pushMessageId in (:pushMessageId)")
	public Page<Integer> findPushTokenIdByPushMessageId(@Param("pushMessageId") Integer pushMessageId, Pageable pageable);
}
