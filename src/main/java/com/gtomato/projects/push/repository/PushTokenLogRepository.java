package com.gtomato.projects.push.repository;

import com.gtomato.projects.push.entity.PushTokenLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PushTokenLogRepository extends JpaRepository<PushTokenLog, Integer>, JpaSpecificationExecutor {
	
}
