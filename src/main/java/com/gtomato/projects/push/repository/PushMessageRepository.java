package com.gtomato.projects.push.repository;

import com.gtomato.projects.base.repository.BaseRepository;
import com.gtomato.projects.push.entity.PushMessage;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface PushMessageRepository extends BaseRepository<PushMessage> {

    @Modifying
    @Query("UPDATE PushMessage t SET t.pushStatus = 2 WHERE t.pushStatus = 1 and t.scheduleTime <= :now")
    public int updatePendPushStatus(@Param("now") Date now);

    @Modifying
    @Query(value = "UPDATE push_message t set t.last_offset = :page + 1 where t.id = :id and t.last_offset = :page ", nativeQuery = true)
    public int updateLastOffset(@Param("id") Integer id, @Param("page") Integer page);
	
	public List<PushMessage> getByPushStatusIn(Collection<Integer> pushStatus, Pageable pageable);
}
