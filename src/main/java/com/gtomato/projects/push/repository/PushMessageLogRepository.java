package com.gtomato.projects.push.repository;

import com.gtomato.projects.base.repository.BaseRepository;
import com.gtomato.projects.push.entity.PushMessage;

public interface PushMessageLogRepository extends BaseRepository<PushMessage> {
	
}
