package com.gtomato.projects.push.repository;

import com.gtomato.projects.base.repository.BaseRepository;
import com.gtomato.projects.push.entity.PushMessageConfigMapping;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PushMessageConfigMappingRepository extends BaseRepository<PushMessageConfigMapping> {

    @Query("SELECT m FROM PushMessageConfigMapping m WHERE m.isDefault = 1 and rowNum <= 1")
    public PushMessageConfigMapping findByIsDefaultTrue();

    @Query("SELECT m FROM PushMessageConfigMapping m WHERE m.iosAppId = :iosAppId")
    public PushMessageConfigMapping findByIosAppId(@Param("iosAppId") String iosAppId);

    @Query("SELECT m FROM PushMessageConfigMapping m WHERE m.androidAppId = :androidAppId")
    public PushMessageConfigMapping findByAndroidAppId(@Param("androidAppId") String androidAppId);
}
