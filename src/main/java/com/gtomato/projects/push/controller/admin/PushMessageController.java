package com.gtomato.projects.push.controller.admin;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

import com.gtomato.projects.push.properties.PushMessagePriority;
import com.gtomato.projects.push.properties.PushMessageStatus;
import com.gtomato.projects.push.properties.PushMessageTargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gtomato.projects.base.config.Constant;
import com.gtomato.projects.base.config.Url;
import com.gtomato.projects.base.controller.admin.BaseBackendController;
import com.gtomato.projects.base.responseObject.admin.AdminControllerResponse;
import com.gtomato.projects.base.responseObject.form.BaseFileUploadForm;
import com.gtomato.projects.base.responseObject.search.FieldDetailsResponse;
import com.gtomato.projects.base.util.BeanUtil;
import com.gtomato.projects.base.util.CSVUtil;
import com.gtomato.projects.push.config.PushMessageConfig;
import com.gtomato.projects.push.config.notification.AndroidPushNotification;
import com.gtomato.projects.push.config.notification.IOSPushNotificationV2;
import com.gtomato.projects.push.entity.PushMessage;
import com.gtomato.projects.push.entity.PushMessageContents;
import com.gtomato.projects.push.entity.PushMessageTokenMapping;
import com.gtomato.projects.push.entity.PushToken;
import com.gtomato.projects.push.responseObject.admin.PushMessageRedirectObject;
import com.gtomato.projects.push.responseObject.admin.PushMessageRequestObject;
import com.gtomato.projects.push.responseObject.search.PushSearchCriteria;
import com.gtomato.projects.push.service.PushMessageService;

@Controller
@RequestMapping(Url.ADMIN_PREFIX + "/push")
public class PushMessageController extends BaseBackendController<PushMessage, PushSearchCriteria> { 
		
	private static final String SPECIFIC_PATH ="/specific";
	
	public PushMessageService pushMessageService;
	
	@Autowired
	private IOSPushNotificationV2<PushToken> iosPushNotification;

	@Autowired
	private AndroidPushNotification<PushToken> androidPushNotification;
	
	@Autowired
	public PushMessageController (PushMessageService pushMessageService){
		//super(demoService, Path.MODULE_DEMO_PATH, SEARCH_DETAILS, LIST_DETAILS, UPDATE_MASTER_DETAILS, "/images");
		super(pushMessageService, PushMessageConfig.getInstance());
		this.pushMessageService = pushMessageService;
	}
	
	@RequestMapping(value= SPECIFIC_PATH, method = RequestMethod.GET)
	public String details(Model model, @ModelAttribute PushMessage pushMessage) throws InstantiationException, IllegalAccessException{
		return this.get(null, pushMessage, model);
	}
	
	/* Base Update View */
	@RequestMapping(value=SPECIFIC_PATH+"/{id}", method = RequestMethod.GET)
	public String get(@PathVariable("id") Integer id, @ModelAttribute PushMessage pushMessage,//
			Model model) throws InstantiationException, IllegalAccessException {
		if (id == null || id == 0 ) {
			model.addAttribute("item", pushMessage);
			model.addAttribute("method", "POST");
		}
		else{
			PushMessage object = this.baseService.findByPK( id );
			model.addAttribute("item", object);
			//model.addAttribute("method", "PUT");
			model.addAttribute("method", "POST");
		}
		log.debug("Get Item - ID:" + id);
		model.addAttribute("id", id);
		model.addAttribute("updateDetails", PushMessageConfig.getInstance().getUpdateMasterDetails());
		model.addAttribute("updateContentDetails", PushMessageConfig.getInstance().getUpdateContentDetails());
		model.addAttribute("listPath", ADMIN_PREFIX + CTRL_PREFIX + LIST_PATH );
		model.addAttribute("updatePath", ADMIN_PREFIX + CTRL_PREFIX + ROOT_PAGE +"specific/"); 
		model.addAttribute("uploadSubDirectory", UPLOAD_SUB_DIRECTORY);
		model.addAttribute("isMultipleTable", PushMessageConfig.getInstance().getIsMultipleTable() );
		
		return ADMIN_PREFIX + CTRL_PREFIX + DETAILS_PATH;
	}

	/* Base Add / Update Handling */
	@RequestMapping(value = SPECIFIC_PATH, method = {RequestMethod.POST, RequestMethod.PUT} )
	@ResponseBody
	public AdminControllerResponse add( 
			@ModelAttribute PushMessage pushMessage, @ModelAttribute BaseFileUploadForm baseFileUploadForm,
			@ModelAttribute PushMessageRequestObject pushMessageRequestObject, 
			@ModelAttribute PushMessageRedirectObject pushMessageRedirectObject,
			Model model) throws IllegalStateException, IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		return this.update(null, pushMessage, baseFileUploadForm, pushMessageRequestObject, pushMessageRedirectObject, model);
	}
			
		
	@RequestMapping(value = SPECIFIC_PATH+"/{id}", method = {RequestMethod.POST, RequestMethod.PUT} )
	@ResponseBody
	public AdminControllerResponse update( @PathVariable("id") Integer id,
			@ModelAttribute PushMessage pushMessage, @ModelAttribute BaseFileUploadForm baseFileUploadForm,
			@ModelAttribute PushMessageRequestObject pushMessageRequestObject, 
			@ModelAttribute PushMessageRedirectObject pushMessageRedirectObject,
			Model model) throws IllegalStateException, IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
			
		List<String> memberIdList = new ArrayList<String>();
		if(pushMessageRequestObject.getInputType() == 0){
			memberIdList.add( pushMessageRequestObject.getMemberId() );
			pushMessage.setTargetType(PushMessageTargetType.SEND_TO_ALL.getValue());
		}
		else if(pushMessageRequestObject.getInputType() == 1){
			memberIdList.add( pushMessageRequestObject.getMemberId() );
			pushMessage.setTargetType(PushMessageTargetType.SEND_TO_SPECIFIC.getValue());
		}
		else{
			//TODO Update member list (CSV)
			List<List<String>> uploadedMemberIdList = new CSVUtil().retrieveDataByMultipartFile(pushMessageRequestObject.getMemberFile());
			for(List<String> uploadedMemberId : uploadedMemberIdList){
				memberIdList.add(uploadedMemberId.get(0));
			}
			pushMessage.setTargetType(PushMessageTargetType.SEND_TO_SPECIFIC.getValue());
		}
		pushMessage.setPushStatus(PushMessageStatus.READY_TO_PUSH.getValue()); //for scheduler query
		pushMessage.setScheduleTime( new Date() ); //Send immediately
		pushMessage.setLastOffset(0);
		 //Send to Specific
		pushMessage.setPriority(PushMessagePriority.HIGH.getValue()); //High Priority
		
		HashMap<String,PushMessageRedirectObject> redirectDetails = new HashMap<String,PushMessageRedirectObject>();
		redirectDetails.put("data", pushMessageRedirectObject);
		String redirectDetailsJson = new ObjectMapper().writeValueAsString(redirectDetails);
		pushMessage.setRedirectDetails(redirectDetailsJson);
		
		for(PushMessageContents content : pushMessage.getContents()){
			content.setPushMessage( pushMessage );
		}
		
		//log.info("Model Attribute : " + pushMessage);
		if( id == null || id == 0 ){
			this.baseService.saveOrUpdate( pushMessage );
			log.debug("Create Item");
		}
		else{
			PushMessage existT = this.baseService.findByPK( id );
			
			BeanUtil.myCopyProperties( pushMessage, existT );
			this.baseService.saveOrUpdate( existT );
			log.debug("Update Item - ID:" + id);
		}
		
		if(pushMessageRequestObject.getInputType() != 0){
			//TODO Query Token By userID
			//memberIdList -> your project user ID
			List<Integer> userIds = Arrays.asList(1);
			List<PushToken> tokenList = this.pushMessageService.findPushTokenByUserIdIn(userIds);
			List<PushMessageTokenMapping> pushMessageTokenMappingList = new ArrayList<PushMessageTokenMapping>();
			
			for(PushToken token : tokenList){
				PushMessageTokenMapping pushMessageTokenMapping = new PushMessageTokenMapping();
				pushMessageTokenMapping.setPushTokenId(token.getId().intValue());
				pushMessageTokenMapping.setPushMessageId(pushMessage.getId());
				pushMessageTokenMappingList.add(pushMessageTokenMapping);
			}
			
			this.pushMessageService.saveOrUpdatePushMessageTokenMapping(pushMessageTokenMappingList);
		
		}
		//log.info("Model Attribute : " + pushMessage);
		AdminControllerResponse adminControllerResponse = new AdminControllerResponse();
		adminControllerResponse.setForwardUrl(Constant.getInstance().getProperty("app.path") + ADMIN_PREFIX + CTRL_PREFIX + LIST_PATH);
		
		return adminControllerResponse;
	}
	
	@RequestMapping(value="/thread", method = RequestMethod.GET)
	public String threadList(Model model) throws InstantiationException, IllegalAccessException{
		
		HashMap<String,ThreadPoolExecutor> threadPools = new HashMap<String, ThreadPoolExecutor>();
		threadPools.put( "iOS API Thread Pool", (ThreadPoolExecutor) iosPushNotification.getApiThreadPool() );
		threadPools.put( "iOS CMS Thread Pool", (ThreadPoolExecutor) iosPushNotification.getCmsThreadPool() );
		threadPools.put( "aOS API Thread Pool", (ThreadPoolExecutor) androidPushNotification.getApiThreadPool() );
		threadPools.put( "aOS CMS Thread Pool", (ThreadPoolExecutor) androidPushNotification.getCmsThreadPool() );
		for( String key : threadPools.keySet() ){
			StringBuffer threadStr = new StringBuffer();
			ThreadPoolExecutor threadPool = threadPools.get( key );
			for(Method method : threadPool.getClass().getMethods()){
				try{
					if( method.getName().contains("get") && !method.getName().contains("Keep")){
						Object value = threadPool.getClass().getMethod(method.getName()).invoke(threadPool);
						threadStr.append(method.getName() + ": "+value.toString() +"</br>");
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			model.addAttribute(key,threadStr);
		}
		model.addAttribute("listPath", ADMIN_PREFIX + CTRL_PREFIX + LIST_PATH );
		model.addAttribute("updatePath", ADMIN_PREFIX + CTRL_PREFIX + "/thread" ); 
		return ADMIN_PREFIX + CTRL_PREFIX + "/thread";
	}

}