package com.gtomato.projects.push.controller.api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.gtomato.projects.push.config.notification.IOSPushNotificationV2;
import com.gtomato.projects.push.config.notification.PushSender;
import com.gtomato.projects.push.responseObject.service.SendPushNotificationReq;
import com.gtomato.projects.push.responseObject.service.SendPushNotificationRes;
import lombok.AllArgsConstructor;

import org.jboss.logging.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gtomato.projects.base.config.Url;
import com.gtomato.projects.base.controller.api.BaseAPIController;
import com.gtomato.projects.base.responseObject.api.BaseAPIResponse;
import com.gtomato.projects.base.responseObject.search.FieldDetailsResponse;
import com.gtomato.projects.base.util.BeanUtil;
import com.gtomato.projects.push.entity.PushMessage;
import com.gtomato.projects.push.entity.PushToken;
import com.gtomato.projects.push.responseObject.search.PushSearchCriteria;
import com.gtomato.projects.push.responseObject.service.TokenReqObject;
import com.gtomato.projects.push.service.PushMessageService;

@Controller
@RequestMapping(Url.API_PREFIX + "/push")
public class PushMessageAPIController{

	public static final FieldDetailsResponse[] SEARCH_DETAILS = new FieldDetailsResponse[0];
	
	@Autowired
	PushMessageService pushMessageService;

	@Autowired
	PushSender<PushToken> pushSender;
	
	@RequestMapping(value = "/sendPush", method = RequestMethod.POST)
	public ResponseEntity<BaseAPIResponse> create(@ModelAttribute PushToken pushToken, @Param String message) {
		
		/* Initial Response Object */
		BaseAPIResponse response = new BaseAPIResponse();
		HttpStatus statusCode = HttpStatus.CREATED;
		
		/* Base Criteria Checking 
		 * 1. Return HttpStatus.BAD_REQUEST (400) if not enough data is provided
		 * 2. Return HttpStatus.CONFLICT (409) if on the server side is determined a record is exists  
		 *  */

		PushToken existPushToken = this.pushMessageService.findPushTokenByToken(pushToken.getToken());

		SendPushNotificationReq res = new SendPushNotificationReq(
				null,
				SendPushNotificationReq.PUSH_MESSAGE_TYPE_API,
				message,
				null,
				new HashMap<Long, Integer>(){{put(1L, 1);}},
				Arrays.asList(existPushToken),
				existPushToken.getDeviceType(),
				null,
				null,
				null
		);
		pushSender.sendPushNotification(res);
		/* Main Logic of API */
		//new IOSPushNotificationV2<PushToken>().sendPushNotification(pushToken, "test", null, null);
	    /* Return Response Object with correct Http Status Code */
	    return new ResponseEntity<BaseAPIResponse>(response, statusCode);
	}
	
	/*Create Record API Sample */
	@RequestMapping(value = "/addToken", method = RequestMethod.POST)
	public ResponseEntity<BaseAPIResponse> create(@ModelAttribute TokenReqObject tokenReqObject) {

		/* Initial Response Object */
		BaseAPIResponse response = new BaseAPIResponse();
		HttpStatus statusCode = HttpStatus.CREATED;
		
		/* Base Criteria Checking 
		 * 1. Return HttpStatus.BAD_REQUEST (400) if not enough data is provided
		 * 2. Return HttpStatus.CONFLICT (409) if on the server side is determined a record is exists  
		 *  */
				
		/* Main Logic of API */
		tokenReqObject.setRequestType(TokenReqObject.REQUEST_TYPE_LAUNCH);
		this.pushMessageService.addNUpdatePushToken(tokenReqObject);

	    /* Return Response Object with correct Http Status Code */
	    return new ResponseEntity<BaseAPIResponse>(response, statusCode);
	}

}
