package com.gtomato.projects.push.responseObject.api;

public interface BasePushToken {

	public Long getId();
	
	public void setId(Long id);
	
	public Integer getDeviceTypeInt();
	
	public void setDeviceTypeInt(Integer deviceTypeInt);
	
	public String getToken();
	
	public void setToken(String token);
	
	public String getLocale();
	
	public void setLocale(String locale);

	public Long getMemberId();

	public void setStatus(Integer status);

	public Integer getStatus();

}
