package com.gtomato.projects.push.responseObject.service;

import com.gtomato.projects.push.responseObject.api.BaseTokenReqObject;
import lombok.Data;

@Data
public class TokenReqObject implements BaseTokenReqObject{
	
	public static final String REQUEST_TYPE_LAUNCH = "LAUNCH";
	public static final String REQUEST_TYPE_REGION = "REGION";
	public static final String REQUEST_TYPE_SIGNIN = "SIGNIN";
	public static final String REQUEST_TYPE_LOGIN = "LOGIN";
	public static final String REQUEST_TYPE_LOGOUT = "LOGOUT";
	public static final String REQUEST_TYPE_NOTIFICATION_CONTROL = "NOTIFICATION_CONTROL";
	
	
	private boolean isSingleLogin = true;
	private String deviceId;
	private Integer deviceType;
	private String token;
	private String locale;
	private Integer userId;
	private Integer countryId;
	private Integer notificationStatus;
	private String requestType;

	public String getPushToken() {
		return token;
	}

	public void setPushToken(String pushToken) {
		this.token = pushToken;
	}
	
}
