package com.gtomato.projects.push.responseObject.api;

public interface BaseTokenReqObject {
	
	public boolean isSingleLogin();
	
	public void setSingleLogin(boolean isSingleLogin);
	
	public String getDeviceId();
	
	public void setDeviceId(String deviceId);
	
	public Integer getDeviceType();
	
	public void setDeviceType(Integer deviceType);
	
	public String getPushToken();
	
	public void setPushToken(String pushToken);
	
	public String getLocale();
	
	public void setLocale(String locale);
	
	public Integer getUserId();
	
	public void setUserId(Integer userId);
	
	public Integer getCountryId();
	
	public void setCountryId(Integer countryId);
	
	public Integer getNotificationStatus();
	
	public void setNotificationStatus(Integer notificationStatus);
	
	public String getRequestType();
	
	public void setRequestType(String type);
	
}
