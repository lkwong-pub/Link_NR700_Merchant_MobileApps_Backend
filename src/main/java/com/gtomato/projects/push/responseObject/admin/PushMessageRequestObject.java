package com.gtomato.projects.push.responseObject.admin;

import lombok.Data;

import org.springframework.web.multipart.MultipartFile;

@Data
public class PushMessageRequestObject {

	private Integer inputType;
	private String memberId;
	private MultipartFile memberFile;
}
