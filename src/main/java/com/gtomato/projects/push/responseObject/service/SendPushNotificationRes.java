package com.gtomato.projects.push.responseObject.service;

import lombok.Data;

import java.util.Date;

/**
 * Created by doncheung on 14/7/2017.
 */
@Data
public class SendPushNotificationRes {

    //Token Details
    String originalToken;
    String updateToken;
    Boolean isUpdatedToken = false;
    Boolean isInvalidToken = false;
    String remarks;
    Integer deviceType;

    //Push Details
    Integer pushMessageId;
    Integer pushMessageType;
    Long userId;
    Boolean isSent;
    Boolean isSuccess;
    Date sendDatetime;


}
