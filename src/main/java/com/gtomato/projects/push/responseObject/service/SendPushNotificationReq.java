package com.gtomato.projects.push.responseObject.service;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by doncheung on 25/1/2017.
 */
@Data
@AllArgsConstructor
public class SendPushNotificationReq<T> {

    public static final Integer PUSH_MESSAGE_TYPE_CMS = 1;
    public static final Integer PUSH_MESSAGE_TYPE_API = 2;

    //Push Message
    private Integer pushMessageId; // null if pushMessageType = API
    private Integer pushMessageType; //1 = CMS, 2 = API
    private String messageContent; // Corresponding locale message content
    private Map<String, Object> customDictionary; //Customized dictionary

    //Token
    private HashMap<Long, Integer> badge; //Alert Badge no group by member ID
    private List<T> pushTokens; //Push Token List
    private Integer deviceType; //Device Type

    // Config
    private Integer regionId; //Region ID
    private String appId; //Apps ID
    private Integer pushConfigId; // Push Notification Config ID


}
