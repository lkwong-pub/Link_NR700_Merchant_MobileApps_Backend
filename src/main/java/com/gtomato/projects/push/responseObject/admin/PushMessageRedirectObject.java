package com.gtomato.projects.push.responseObject.admin;

import lombok.Data;

import org.springframework.web.multipart.MultipartFile;

@Data
public class PushMessageRedirectObject {

	private Integer redirectType;
}
