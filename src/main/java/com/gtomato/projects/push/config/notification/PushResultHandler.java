package com.gtomato.projects.push.config.notification;

import com.gtomato.projects.push.entity.PushToken;
import com.gtomato.projects.push.entity.PushTokenLog;
import com.gtomato.projects.push.responseObject.service.SendPushNotificationRes;
import com.gtomato.projects.push.service.PushMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by doncheung on 13/7/2017.
 */

@Configuration
public class PushResultHandler implements InitializingBean {

    @Autowired
    private PushMessageService pushMessageService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private Integer THREAD_POOL_DEFAULT_SIZE = 128;

    private Integer DEFAULT_PUSH_NOTIFICATION_RESPONSE_TIMEOUT = 15 * 60; //Timeout for push notification response

    public void afterPropertiesSet() throws Exception {
        //threadPool = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(THREAD_POOL_DEFAULT_SIZE, new CustomizableThreadFactory("PushResultHandler-")) );

    }

    public void saveTokenResult(Future<List<SendPushNotificationRes>> response){
        try {


            List<SendPushNotificationRes> pushNotificationResList = response.get(DEFAULT_PUSH_NOTIFICATION_RESPONSE_TIMEOUT, TimeUnit.SECONDS); //TODO Add timeout

            logger.info("Save Token Result Start");

            logger.info("No Of Push Notification Response :"+ pushNotificationResList.size());

            //Empty Response checking
            if( pushNotificationResList.isEmpty() )
                return;

            List<PushToken> savePushTokenList = new ArrayList<PushToken>();
            List<PushTokenLog> savePushTokenLogList = new ArrayList<PushTokenLog>();
            for(SendPushNotificationRes res : pushNotificationResList){
                if(res == null)
                    continue;

                PushToken pushToken = this.pushMessageService.findPushTokenByToken(res.getOriginalToken());
                if(pushToken == null)
                    continue;

                //Invalid Token Checking
                if(res.getIsInvalidToken()) {
                    pushToken.setStatus(99); //Delete
                    savePushTokenList.add(pushToken);
                }

                //Updated Token Checking
                if(res.getIsUpdatedToken()) {
                    pushToken.setToken(res.getUpdateToken());
                    savePushTokenList.add(pushToken);
                }

                //Create Push Token Log
                PushTokenLog pushTokenLog = new PushTokenLog();
                pushTokenLog.setPushMessageType(res.getPushMessageType());
                pushTokenLog.setToken(res.getOriginalToken());
                pushTokenLog.setDeviceType(res.getDeviceType());
                pushTokenLog.setPushMessageId(res.getPushMessageId());
                pushTokenLog.setIsSent(res.getIsSent());
                pushTokenLog.setIsSuccess(res.getIsSuccess());
                pushTokenLog.setSendDatetime(res.getSendDatetime());
                pushTokenLog.setRemarks(res.getRemarks());
                savePushTokenLogList.add(pushTokenLog);

            }

            if(!savePushTokenList.isEmpty())
                this.pushMessageService.savePushTokenList(savePushTokenList);

            if(!savePushTokenLogList.isEmpty())
                this.pushMessageService.savePushTokenLogList(savePushTokenLogList);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        logger.info("Save Token Result End");
    }

//    public void onFailure(Throwable arg0)
//    {
//        System.out.println("onFailure:"+arg0);
//    }
//
//    public void onSuccess(List<SendPushNotificationRes> pushNotificationResList)
//    {
////        Predicate<SendPushNotificationRes> updateTokenPredicate = new Predicate<SendPushNotificationRes>() {
////            @Override
////            public boolean apply(SendPushNotificationRes res) {
////                return res.getIsUpdatedToken();
////            }
////        };
////
////        Predicate<SendPushNotificationRes> invalidTokenPredicate = new Predicate<SendPushNotificationRes>() {
////            @Override
////            public boolean apply(SendPushNotificationRes res) {
////                return res.getIsInvalidToken();
////            }
////        };
////
////        Function<SendPushNotificationRes, String> func = new Function<SendPushNotificationRes,String>(){
////            @Override
////            public String apply(SendPushNotificationRes res) {
////                return res.getOriginalToken();
////            }
////        };
////
////        Collection<SendPushNotificationRes> updateTokenList = Collections2.filter(pushNotificationResList, updateTokenPredicate);
////        Collection<SendPushNotificationRes> invalidTokenList = Collections2.filter(pushNotificationResList, updateTokenPredicate);
//
//        logger.info("Push Result Handler Start");
//        logger.info("No of Result: "+ (pushNotificationResList == null ? 0 : pushNotificationResList.size()));
////        logger.info("No of token need to change "+ (updateTokenList == null ? 0 : updateTokenList.size()));
////        logger.info("No of token is invalid "+ (invalidTokenList == null ? 0 : invalidTokenList.size()));
//
//
//        for(SendPushNotificationRes res : pushNotificationResList ){
//            //Save token result to push message log
//        }
//    }


}
