package com.gtomato.projects.push.config.notification;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gcm.server.*;
import com.google.android.gcm.server.Message.Builder;
import com.gtomato.projects.base.config.Constant;
import com.gtomato.projects.push.entity.PushMessageConfigMapping;
import com.gtomato.projects.push.properties.PushMessageDeviceType;
import com.gtomato.projects.push.responseObject.api.BasePushToken;
import com.gtomato.projects.push.responseObject.service.SendPushNotificationReq;
import com.gtomato.projects.push.responseObject.service.SendPushNotificationRes;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class AndroidPushNotification<T extends BasePushToken> implements InitializingBean {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final Integer THREAD_POOL_DEFAULT_SIZE = 128;

	@Getter
	private ExecutorService apiThreadPool = null;

	@Getter
	private ExecutorService cmsThreadPool = null;

	@Autowired
	private PushResultHandler pushResultHandler;

	@Setter
	private List<PushMessageConfigMapping> pushMessageConfigMappingList;


	HashMap<Integer, Sender> configIdSenderMap = new HashMap<Integer, Sender>();
	HashMap<String, Sender> appIdSenderMap = new HashMap<String, Sender>();
	HashMap<Integer, Sender> regionIdSenderMap = new HashMap<Integer, Sender>();

	Sender defaultSender = null;

	public void afterPropertiesSet() throws Exception {

		logger.info("Initial AOS Push Notification");
		Constant constant = Constant.getInstance();
		// Thread Pool
		logger.info("Initial AOS Push Notification - Thread Pool [ New Fixed Thread Pool size: " +constant.getIntegerProperty("push.notification.pool.size", THREAD_POOL_DEFAULT_SIZE)+ "]");
		apiThreadPool = Executors.newFixedThreadPool(constant.getIntegerProperty("push.notification.pool.size", THREAD_POOL_DEFAULT_SIZE), new CustomizableThreadFactory("AOS-Push-Notification-"));
		cmsThreadPool = Executors.newFixedThreadPool(constant.getIntegerProperty("push.notification.pool.size", THREAD_POOL_DEFAULT_SIZE), new CustomizableThreadFactory("AOS-Push-Notification-"));

	}

	public void updateConfig(List<PushMessageConfigMapping> pushMessageConfigMappingList){
		this.pushMessageConfigMappingList = pushMessageConfigMappingList;

	}

	public void initialConnection(){
		logger.info( "AOS Push Notification Initial Connection Start" );
		for (PushMessageConfigMapping pushMessageConfigMapping : pushMessageConfigMappingList) {
			logger.info("ConfigID = " + pushMessageConfigMapping.getId());
			Sender sender = new GCMProxySender(pushMessageConfigMapping.getAndroidPushKey());
			this.configIdSenderMap.put(pushMessageConfigMapping.getId(), sender);
			this.appIdSenderMap.put(pushMessageConfigMapping.getAndroidAppId(), sender);
			this.regionIdSenderMap.put(pushMessageConfigMapping.getRegionId(), sender);
			if(pushMessageConfigMapping.getIsDefault())
				this.defaultSender = sender;
		}
		logger.info( "AOS Push Notification Initial Connection End" );

	}

	public void sendPushNotification(T pushToken, String messageContent, Integer badge, Map<String, Object> customDictionary) {
		HashMap<Long, Integer> badgeMap = new HashMap<Long, Integer>();
		if(pushToken.getMemberId() != null) {
			badgeMap.put(pushToken.getMemberId(), badge);
		}
		this.sendPushNotification(Arrays.asList(pushToken), messageContent, badgeMap, customDictionary);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary) {
		this.sendPushNotification(pushTokens, messageContent, badge, customDictionary, null, null, null);
		//threadPool.submit(new PushNotificationRunnable(pushTokens, messageContent, badge, customDictionary));
	}

	public void sendPushNotification(T pushToken, String messageContent, Integer badge, Map<String, Object> customDictionary, Integer regionId) {
		HashMap<Long, Integer> badgeMap = new HashMap<Long, Integer>();
		if(pushToken.getMemberId() != null) {
			badgeMap.put(pushToken.getMemberId(), badge);
		}
		this.sendPushNotification(Arrays.asList(pushToken), messageContent, badgeMap, customDictionary, regionId);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId) {
		this.sendPushNotification(pushTokens, messageContent, badge, customDictionary, regionId, null, null);
		//threadPool.submit(new PushNotificationRunnable(pushTokens, messageContent, badge, customDictionary, regionId));
	}

	public void sendPushNotification(T pushToken, String messageContent, Integer badge, Map<String, Object> customDictionary, Integer regionId, String appId) {
		HashMap<Long, Integer> badgeMap = new HashMap<Long, Integer>();
		if(pushToken.getMemberId() != null) {
			badgeMap.put(pushToken.getMemberId(), badge);
		}
		this.sendPushNotification(Arrays.asList(pushToken), messageContent, badgeMap, customDictionary, regionId, appId, null);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId) {
		this.sendPushNotification(pushTokens, messageContent, badge, customDictionary, regionId, appId, null);
		//threadPool.submit(new PushNotificationRunnable(pushTokens, messageContent, badge, customDictionary, regionId, appId, null));
	}

	public void sendPushNotification(T pushToken, String messageContent, Integer badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId) {
		HashMap<Long, Integer> badgeMap = new HashMap<Long, Integer>();
		if(pushToken.getMemberId() != null) {
			badgeMap.put(pushToken.getMemberId(), badge);
		}
		this.sendPushNotification(Arrays.asList(pushToken), messageContent, badgeMap, customDictionary, regionId, appId, pushConfigId);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId) {
		this.sendPushNotification(null, pushTokens, messageContent, badge, customDictionary, regionId, appId, pushConfigId);
	}

	public void sendPushNotification(Integer pushMessageId, List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId) {
		this.sendPushNotification(null, pushTokens, messageContent, badge, customDictionary, regionId, appId, pushConfigId, null);
	}

	public void sendPushNotification(Integer pushMessageId, List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId, Integer pushMessageType) {
		//Thread thread = new Thread(new PushNotificationRunnable(pushTokens, messageContent, badge, customDictionary, regionId, appId, pushConfigId));
		Callable task = new PushNotificationCallable(pushMessageId, pushTokens, messageContent, badge, customDictionary, regionId, appId, pushConfigId, pushMessageType);
		//Future<List<SendPushNotificationRes>> future = threadPool.submit(task);
		this.saveTokenResult(task, pushMessageType);
	}

	public void saveTokenResult(Callable task, Integer pushMessageType){
		if( SendPushNotificationReq.PUSH_MESSAGE_TYPE_API.equals(pushMessageType ))
			this.pushResultHandler.saveTokenResult(apiThreadPool.submit(task));
		else if( SendPushNotificationReq.PUSH_MESSAGE_TYPE_CMS.equals(pushMessageType ))
			this.pushResultHandler.saveTokenResult(cmsThreadPool.submit(task));
	}

	private class PushNotificationCallable implements Callable {

		//private Integer maxPushPerReq = 500;

		private Integer pushMessageId;

		private List<T> pushTokens;

		private String messageContent;

		private Map<String, Object> customDictionary;

		private HashMap<Long, Integer> badge;

		private Integer regionId;

		private String appId;

		private Integer pushConfigId;

		private Integer pushMessageType;

		long threadId = Thread.currentThread().getId();

		List<SendPushNotificationRes> response = new ArrayList<SendPushNotificationRes>();

		public PushNotificationCallable(Integer pushMessageId, List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId, Integer pushMessageType) {
			this.pushMessageId = pushMessageId;
			this.pushTokens = pushTokens;
			this.messageContent = messageContent;
			this.badge = badge;
			this.customDictionary = customDictionary;
			this.regionId = regionId;
			this.appId = appId;
			this.pushConfigId = pushConfigId;
			this.pushMessageType = pushMessageType;
		}

		public List<SendPushNotificationRes> call() {

			try{
				//Empty Push Token List Checking
				logger.info("[threadID:"+threadId+"] Total size of tokens to push = " + pushTokens.size());
				if (this.pushTokens.size() == 0)
					return null;

				//Get aOS Push Notification Sender
				Sender sender = getPushSender();

				//Re-group push token by badge no
				HashMap<Integer, List<T>> badgeTokenMap = getBadgeGroup();


				//Send Push Notification
				for (Integer badgeSet : badgeTokenMap.keySet()) {
					List<T> registrationIds = badgeTokenMap.get(badgeSet);
					logger.info("[threadID:"+threadId+"] Badge no: " + badgeSet + ". Android Push Size : " + registrationIds.size());

					try {
						Builder builder = new Builder() //
								.collapseKey("1") //
								.timeToLive(600) //
								.delayWhileIdle(true)//
								.addData("message", this.messageContent); //

						if(badgeSet != null)
							builder.addData("badge_no", String.valueOf(badgeSet));

						if (this.customDictionary != null)
							for (String key : this.customDictionary.keySet())
								builder.addData(key, new ObjectMapper().writeValueAsString(customDictionary.get(key)));

						Message message = builder.build();

						if (registrationIds.size() == 1) {
							Result result = sender.send(message, registrationIds.get(0).getToken(), 3);
							SendPushNotificationRes res = getSendPushNotificationRes(result, registrationIds.get(0));
							response.add(res);
						} else {
							List<String> tokenList = new ArrayList<String>();
							for(T token : registrationIds)
								tokenList.add(token.getToken());

							MulticastResult multicastResult = sender.send(message, tokenList, 3);
							for (int i = 0; i < multicastResult.getResults().size(); i++) {
								Result result = multicastResult.getResults().get(i);
								SendPushNotificationRes res = getSendPushNotificationRes(result, registrationIds.get(i));
								response.add(res);
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}catch(Exception e){
				logger.error(e.getMessage());
				e.printStackTrace();
			}

			return response;
		}

		private HashMap<Integer, List<T>> getBadgeGroup(){
			logger.info("[threadID:"+threadId+"] Re-group push token by badge no");

			HashMap<Integer, List<T>> badgeTokenMap = new HashMap<Integer, List<T>>();


			//List<List<T>> subPushTokenList = new ListUtils<T>().split(pushTokens, this.maxPushPerReq);
			//for(List<T> subList : subPushTokenList){
				for(T pushToken : pushTokens){
					if (pushToken.getMemberId() != null && badge != null) {
						Integer badgeNo = badge.get(pushToken.getMemberId());
						List<T> registrationIds = badgeTokenMap.get(
								badgeNo) == null ? new ArrayList<T>() : badgeTokenMap.get(
								badgeNo);
						registrationIds.add(pushToken);
						badgeTokenMap.put(badgeNo, registrationIds);
					}
				}
			//}
			logger.info("[threadID:"+threadId+"] No of Badge No Group : " + badgeTokenMap.size());
				return badgeTokenMap;
		}

		private Sender getPushSender(){

			if (pushConfigId == null && StringUtils.isBlank(appId) && regionId == null){
				logger.info("[threadID:"+threadId+"] Get Default AOS Sender" );
				return defaultSender;
			}
			else {
				if (pushConfigId != null) {
					logger.info( "[threadID:"+threadId+"] Get AOS Sender By Push Config ID : " + pushConfigId );
					return configIdSenderMap.get(pushConfigId);
				} else if (StringUtils.isNotBlank(appId)) {
					logger.info( "[threadID:"+threadId+"] Get AOS Sender By App ID : " + appId );
					return appIdSenderMap.get(appId);
				} else if (pushConfigId == null && StringUtils.isBlank(appId) && regionId != null) {
					logger.info( "[threadID:"+threadId+"] Get AOS Sender By Region ID : " + regionId );
					return regionIdSenderMap.get(regionId);
				}
			}
			return null;
		}

		private SendPushNotificationRes getSendPushNotificationRes(Result result, T pushToken){

			//New SendPushNotificationRes Object
			SendPushNotificationRes res = new SendPushNotificationRes();
			res.setOriginalToken(pushToken.getToken());
			res.setPushMessageId(pushMessageId);
			res.setPushMessageType(pushMessageType);
			res.setDeviceType(PushMessageDeviceType.ANDROID.getValue());
			res.setSendDatetime(new Date());
			res.setIsSent(true);
			res.setUserId(pushToken.getMemberId());


			//Success Handling
			if(StringUtils.isBlank(result.getErrorCodeName())) {
				logger.info("[threadID:" + threadId + "] AOS Push notification sent successfully to: " + pushToken.getToken());
				if (result.getMessageId() != null) {
					String canonicalRegistrationId = result.getCanonicalRegistrationId();
					if (canonicalRegistrationId != null) {
						logger.info("[threadID:" + threadId + "] " + pushToken.getToken() + " Changed to " + canonicalRegistrationId);
						res.setIsUpdatedToken(true);
						res.setUpdateToken(result.getCanonicalRegistrationId());
						res.setRemarks("Token Changed");
					}
				}
				res.setIsInvalidToken(false);
				res.setIsSuccess(true);
			}
			//Fail Handling
			else {
				logger.info("[threadID:" + threadId + "] AOS Push notification sent fail to: " + pushToken.getToken() + " [Error Code: "+ result.getErrorCodeName() +"]");
				String remarks;
				if (result.getErrorCodeName().equals(Constants.ERROR_NOT_REGISTERED)){
					remarks = "Unregistered";
					res.setIsInvalidToken(true);
				}
				else if (result.getErrorCodeName().equals(Constants.ERROR_INVALID_REGISTRATION)) {
					remarks = "Invalid Registration";
					res.setIsInvalidToken(true);
				}
				else
					remarks = "Other Error: [Code:" + result.getErrorCodeName() + "]";
				res.setRemarks(remarks);
				res.setIsSuccess(false);

			}

			return res;
		}
	}
}
