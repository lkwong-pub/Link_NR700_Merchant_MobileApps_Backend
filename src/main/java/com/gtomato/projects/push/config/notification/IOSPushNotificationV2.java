package com.gtomato.projects.push.config.notification;


import com.gtomato.projects.base.config.Constant;
import com.gtomato.projects.push.entity.PushMessageConfigMapping;
import com.gtomato.projects.push.properties.PushMessageDeviceType;
import com.gtomato.projects.push.responseObject.api.BasePushToken;
import com.gtomato.projects.push.responseObject.service.SendPushNotificationReq;
import com.gtomato.projects.push.responseObject.service.SendPushNotificationRes;
import com.relayrides.pushy.apns.ApnsClient;
import com.relayrides.pushy.apns.ApnsClientBuilder;
import com.relayrides.pushy.apns.ClientNotConnectedException;
import com.relayrides.pushy.apns.PushNotificationResponse;
import com.relayrides.pushy.apns.proxy.HttpProxyHandlerFactory;
import com.relayrides.pushy.apns.util.ApnsPayloadBuilder;
import com.relayrides.pushy.apns.util.SimpleApnsPushNotification;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.*;


@Configuration
public class IOSPushNotificationV2<T extends BasePushToken> implements InitializingBean, DisposableBean {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final Integer THREAD_POOL_DEFAULT_SIZE = 128;

	@Getter
	private ExecutorService apiThreadPool = null;

	@Getter
	private ExecutorService cmsThreadPool = null;

	@Autowired
	private PushResultHandler pushResultHandler;

	@Setter
	private List<PushMessageConfigMapping> pushMessageConfigMappingList;


	HashMap<Integer, ApnsClient> configIdApnsClientMap = new HashMap<Integer, ApnsClient>();
	HashMap<String, ApnsClient> appIdApnsClientMap = new HashMap<String, ApnsClient>();
	HashMap<Integer, ApnsClient> regionIdApnsClientMap = new HashMap<Integer, ApnsClient>();

	HashMap<Integer, String> configIdTopicMap = new HashMap<Integer, String>();
	HashMap<String, String> appIdTopicMap = new HashMap<String, String>();
	HashMap<Integer, String> regionIdTopicMap = new HashMap<Integer, String>();

	ApnsClient defaultApnsClient = null;
	String defaultTopic = null;

	// Shared factory
	private HttpProxyHandlerFactory httpProxyHandlerFactory;

	public void afterPropertiesSet() throws Exception {

        logger.info("Initial IOS Push Notification");
		Constant constant = Constant.getInstance();
		// Thread Pool Config
        logger.info("Initial IOS Push Notification - Thread Pool [ New Fixed Thread Pool size: " +constant.getIntegerProperty("push.notification.pool.size", THREAD_POOL_DEFAULT_SIZE)+ "]");
		apiThreadPool = Executors.newFixedThreadPool(constant.getIntegerProperty("push.notification.pool.size", THREAD_POOL_DEFAULT_SIZE), new CustomizableThreadFactory("IOS-Push-Notification-"));
		cmsThreadPool = Executors.newFixedThreadPool(constant.getIntegerProperty("push.notification.pool.size", THREAD_POOL_DEFAULT_SIZE), new CustomizableThreadFactory("IOS-Push-Notification-"));

		logger.info("Initial IOS Push Notification - Use proxy flag = " + Constant.getInstance().getBooleanProperty("push.notification.use.proxy", false));

		// Proxy Factory Config
		if(Constant.getInstance().getBooleanProperty("push.notification.use.proxy", false)) {
			logger.info("Initial IOS Push Notification - proxy URI & Port = " + constant.getProperty("push.notification.proxy.uri") +":"+constant.getIntegerProperty("push.notification.proxy.port"));
			if(StringUtils.isNotBlank(constant.getProperty("push.notification.username"))) {
				//With username & password
				httpProxyHandlerFactory = new HttpProxyHandlerFactory(
						new InetSocketAddress(constant.getProperty("push.notification.proxy.uri"),
								constant.getIntegerProperty("push.notification.proxy.port")),
						constant.getProperty("push.notification.username"),
						constant.getProperty("push.notification.password"));
			} else {
				//Without username & password
				httpProxyHandlerFactory = new HttpProxyHandlerFactory(
						new InetSocketAddress(constant.getProperty("push.notification.proxy.uri"),
								constant.getIntegerProperty("push.notification.proxy.port")));
			}
		}

	}

	public void updateConfig(List<PushMessageConfigMapping> pushMessageConfigMappingList){
		this.pushMessageConfigMappingList = pushMessageConfigMappingList;
	}

	public void initialConnection(){
		try {
			logger.info( "IOS Push Notification Initial Connection Start" );
			for (PushMessageConfigMapping pushMessageConfigMapping : pushMessageConfigMappingList) {
				logger.info("ConfigID = " + pushMessageConfigMapping.getId());
				ApnsClient apnsClient = null;
				ApnsClientBuilder apnsClientBuilder = new ApnsClientBuilder();
				apnsClientBuilder.setClientCredentials(new File(pushMessageConfigMapping.getIosPushCert()), pushMessageConfigMapping.getIosPushPassword());

				logger.info("httpProxyHandlerFactory != null? " + (httpProxyHandlerFactory != null));
				if (httpProxyHandlerFactory != null) {
					apnsClientBuilder.setProxyHandlerFactory(httpProxyHandlerFactory);
				}
				apnsClient = apnsClientBuilder.build();
				logger.info("Connection to be opened. ConfigID = " + pushMessageConfigMapping.getId());
				final Future<Void> connectFuture = apnsClient.connect(ApnsClient.PRODUCTION_APNS_HOST);
				connectFuture.await();
				logger.info("Connection opened. ConfigID = " + pushMessageConfigMapping.getId());

				this.configIdApnsClientMap.put(pushMessageConfigMapping.getId(), apnsClient);
				this.appIdApnsClientMap.put(pushMessageConfigMapping.getIosAppId(), apnsClient);
				this.regionIdApnsClientMap.put(pushMessageConfigMapping.getRegionId(), apnsClient);

				this.configIdTopicMap.put(pushMessageConfigMapping.getId(), pushMessageConfigMapping.getIosPushTopic());
				this.appIdTopicMap.put(pushMessageConfigMapping.getIosAppId(), pushMessageConfigMapping.getIosPushTopic());
				this.regionIdTopicMap.put(pushMessageConfigMapping.getRegionId(), pushMessageConfigMapping.getIosPushTopic());

				if(pushMessageConfigMapping.getIsDefault()){
					this.defaultApnsClient = apnsClient;
					this.defaultTopic = pushMessageConfigMapping.getIosPushTopic();
				}

			}
			logger.info( "IOS Push Notification Initial Connection End" );

		}catch (Exception e){
			e.printStackTrace();
		}

	}

	public void reconnection() {

		logger.info( "IOS Push Notification Reconnection Start" );
		for (PushMessageConfigMapping pushMessageConfigMapping : pushMessageConfigMappingList) {

			logger.info("ConfigID = " + pushMessageConfigMapping.getId());
			ApnsClient apnsClient = this.configIdApnsClientMap.get(pushMessageConfigMapping.getId());

			long timestampForDisconnect = System.currentTimeMillis();
			try {
				logger.info("apnsClient : " + apnsClient + ". Is Connected = " + (apnsClient == null ? "False" : apnsClient.isConnected()));
				if(apnsClient != null) {
					logger.info("Now waiting reconnectFuture.await() : " + timestampForDisconnect + ". ConfigID = " + pushMessageConfigMapping.getId());
					apnsClient.getReconnectionFuture().await();
					logger.info("Reconnected. apnsClient: "+apnsClient+". Is Connected = " + (apnsClient == null ? "False" : apnsClient.isConnected()));
					logger.info("Completed waiting reconnectFuture.await() : " + timestampForDisconnect + ", difference: " + (System.currentTimeMillis() - timestampForDisconnect) + ". ConfigID = " + pushMessageConfigMapping.getId() );
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		logger.info( "IOS Push Notification Reconnection End" );
	}

	public void disconnection(){

		logger.info( "IOS Push Notification Disconnection Start" );
		for (PushMessageConfigMapping pushMessageConfigMapping : pushMessageConfigMappingList) {

			logger.info("ConfigID = " + pushMessageConfigMapping.getId());
			ApnsClient apnsClient = this.configIdApnsClientMap.get(pushMessageConfigMapping.getId());


			// Finally, when we're done sending notifications (i.e. when our
			// application is shutting down), we should disconnect all APNs clients
			// that may be in play..
			long timestampForDisconnect = System.currentTimeMillis();
			Future<Void> disconnectFuture = null;
			logger.info("apnsClient : " + apnsClient + ". Is Connected = " + (apnsClient == null ? "False" : apnsClient.isConnected()));
			if(apnsClient != null && apnsClient.isConnected()) {
				disconnectFuture = apnsClient.disconnect();
				logger.info("Now waiting disconnectFuture.await() : " + timestampForDisconnect + ". ConfigID = " + pushMessageConfigMapping.getId());
			}
			try {
				if(disconnectFuture != null)
					disconnectFuture.await();
				logger.info("Disconnected. apnsClient : " + apnsClient + ". Is Connected = " + (apnsClient == null ? "False" : apnsClient.isConnected()));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			logger.info("Completed waiting disconnectFuture.await() : " + timestampForDisconnect + ", difference: " + (System.currentTimeMillis() - timestampForDisconnect) + ". ConfigID = " + pushMessageConfigMapping.getId() );
		}

		logger.info( "IOS Push Notification Disconnection End" );
	}


	public void sendPushNotification(T pushToken, String messageContent, Integer badge, Map<String, Object> customDictionary) {
		this.sendPushNotification(pushToken, messageContent, badge, customDictionary, null, null, null);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary) {
		this.sendPushNotification(pushTokens, messageContent, badge, customDictionary, null, null, null);
	}

	public void sendPushNotification(T pushToken, String messageContent, Integer badge, Map<String, Object> customDictionary, Integer regionId) {
		this.sendPushNotification(pushToken, messageContent, badge, customDictionary, regionId, null, null);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId) {
		this.sendPushNotification(pushTokens, messageContent, badge, customDictionary, regionId, null, null);
	}

	public void sendPushNotification(T pushToken, String messageContent, Integer badge, Map<String, Object> customDictionary, Integer regionId, String appId) {
		this.sendPushNotification(pushToken, messageContent, badge, customDictionary, regionId, appId, null);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId) {
		this.sendPushNotification(pushTokens, messageContent, badge, customDictionary, regionId, appId, null);
	}

	public void sendPushNotification(T pushToken, String messageContent, Integer badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId) {
		HashMap<Long, Integer> badgeMap = new HashMap<Long, Integer>();
		if(pushToken.getMemberId() != null) {
			badgeMap.put(pushToken.getMemberId(), badge);
		}
		this.sendPushNotification(Arrays.asList(pushToken), messageContent, badgeMap, customDictionary, regionId, appId, pushConfigId);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId) {
		this.sendPushNotification(null, pushTokens, messageContent, badge, customDictionary, regionId, appId, pushConfigId);
	}

	public void sendPushNotification(Integer pushMessageId, List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId) {
		this.sendPushNotification(null, pushTokens, messageContent, badge, customDictionary, regionId, appId, pushConfigId, null);
	}

	public void sendPushNotification(Integer pushMessageId, List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId, Integer pushMessageType) {
        Callable task = new PushNotificationCallable(pushMessageId, pushTokens, messageContent, badge, customDictionary, regionId, appId, pushConfigId, pushMessageType);
		this.saveTokenResult(task, pushMessageType);
	}

	public void saveTokenResult(Callable task, Integer pushMessageType){
		if( SendPushNotificationReq.PUSH_MESSAGE_TYPE_API.equals(pushMessageType ))
			this.pushResultHandler.saveTokenResult(apiThreadPool.submit(task));
		else if( SendPushNotificationReq.PUSH_MESSAGE_TYPE_CMS.equals(pushMessageType ))
			this.pushResultHandler.saveTokenResult(cmsThreadPool.submit(task));
	}

	public void destroy() throws Exception {
		logger.info( "IOS Push Notification Destroy Start" );
		disconnection();
		logger.info( "IOS Push Notification Destroy End" );
	}

	private class PushNotificationCallable implements Callable {

		private Integer pushMessageId;

		private List<T> pushTokens;

		private String messageContent;

		private Map<String, Object> customDictionary;

		private HashMap<Long, Integer> badge;

		private Integer regionId;

		private String appId;

		private Integer pushConfigId;

		private Integer pushMessageType;

		long threadId = Thread.currentThread().getId();

		private Map<String, T> tokenMap;

		List<SendPushNotificationRes> response = new ArrayList<SendPushNotificationRes>();

        public PushNotificationCallable(Integer pushMessageId, List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer regionId, String appId, Integer pushConfigId, Integer pushMessageType) {
			this.pushMessageId = pushMessageId;
        	this.pushTokens = pushTokens;
			this.messageContent = messageContent;
			this.customDictionary = customDictionary;
			this.badge = badge;
			this.regionId = regionId;
			this.appId = appId;
			this.pushConfigId = pushConfigId;
			this.pushMessageType = pushMessageType;
            tokenMap = getTokenMap();
		}

		public List<SendPushNotificationRes> call() {

			try {

				//Empty Push Token List Checking
				logger.info("[threadID:"+threadId+"] Total size of tokens to push = " + pushTokens.size());
				if (this.pushTokens.size() == 0)
					return null;

                //Get APNS Client
                ApnsClient apnsClient = getApnsClient();

                //Get Apns Push Notification
				final List<SimpleApnsPushNotification> pushNotifications = getApnsPushNotificationList();

                //Send Push Notification
                final CountDownLatch countDownLatch = new CountDownLatch(pushNotifications.size());

                for (final SimpleApnsPushNotification pushNotification : pushNotifications) {
                    final Future<PushNotificationResponse<SimpleApnsPushNotification>> future =
                            apnsClient.sendNotification(pushNotification);

                    future.addListener(new GenericFutureListener<Future<PushNotificationResponse<SimpleApnsPushNotification>>>() {

                        public void operationComplete(final Future<PushNotificationResponse<SimpleApnsPushNotification>> future) throws Exception {
                            try {
                                response.add(getSendPushNotificationRes(future));
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                                if (e.getCause() instanceof ClientNotConnectedException) {
                                    reconnection();
                                }
                            } finally {
                                countDownLatch.countDown();
                            }
                        }
                    });
                }
                long timestamp = System.currentTimeMillis();
                logger.info("Now waiting countDownLatch.await() : " + timestamp + ", thread id = " + threadId);
                countDownLatch.await();
                logger.info("Completed waiting countDownLatch.await() : " + timestamp + ", difference: " + (System.currentTimeMillis() - timestamp)+ ", thread id = " + threadId);


			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
            return response;
		}




        private String getTopic(){
            if (pushConfigId == null && StringUtils.isBlank(appId) && regionId == null) {
                logger.info( "[threadID:"+threadId+"] Get Default Topic" );
                return defaultTopic;
            } else {
                if (pushConfigId != null ) {
                    logger.info( "[threadID:"+threadId+"] Get Topic By Push Config ID : " + pushConfigId );
                    return configIdTopicMap.get(pushConfigId);
                } else if (StringUtils.isNotBlank(appId)) {
                    logger.info( "[threadID:"+threadId+"] Get Topic By App ID : " + appId );
                    return appIdTopicMap.get(appId);
                } else if (pushConfigId == null && StringUtils.isBlank(appId) && regionId != null) {
                    logger.info( "[threadID:"+threadId+"] Get Topic By Region ID : " + regionId );
                    return regionIdTopicMap.get(appId);
                }
            }

			return null;
		}

        private ApnsClient getApnsClient(){

			if (pushConfigId == null && StringUtils.isBlank(appId) && regionId == null) {
				logger.info( "[threadID:"+threadId+"] Get Default Apns Client" );
				return defaultApnsClient;
			} else {
				if (pushConfigId != null ) {
					logger.info( "[threadID:"+threadId+"] Get Apns Client By Push Config ID : " + pushConfigId );
					return configIdApnsClientMap.get(pushConfigId);
				} else if (StringUtils.isNotBlank(appId)) {
					logger.info( "[threadID:"+threadId+"] Get Apns Client By App ID : " + appId );
					return appIdApnsClientMap.get(appId);
				} else if (pushConfigId == null && StringUtils.isBlank(appId) && regionId != null) {
					logger.info( "[threadID:"+threadId+"] Get Apns Client By Region ID : " + regionId );
					return regionIdApnsClientMap.get(appId);
				}
			}
			return null;
		}

        private final List<SimpleApnsPushNotification> getApnsPushNotificationList(){
            final List<SimpleApnsPushNotification> pushNotifications = new ArrayList<SimpleApnsPushNotification>();
            //Get APNS Client Push Topic
            String topic = getTopic();

            for (T pushToken : this.pushTokens) {
                final SimpleApnsPushNotification pushNotification;

                {
                    final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
                    payloadBuilder.setAlertBody(messageContent);

                    if (customDictionary != null)
                        for (String key : customDictionary.keySet())
                            payloadBuilder.addCustomProperty(key, customDictionary.get(key));
                    if (badge != null && pushToken.getMemberId() !=null) {
                        Integer badgeNumber = badge.get(pushToken.getMemberId());
                        payloadBuilder.setBadgeNumber(badgeNumber);
                    }
                    final String payload = payloadBuilder.buildWithDefaultMaximumLength();
                    final String token = pushToken.getToken();

                    pushNotification = new SimpleApnsPushNotification(token, topic, payload);
                }
                pushNotifications.add(pushNotification);

            }
            return pushNotifications;
        }

        private Map<String, T> getTokenMap(){
		    Map<String, T> tokenMap = new HashMap<String, T>();
		    if(pushTokens == null || pushTokens.isEmpty())
		        return null;

		    for(T token : pushTokens){
                tokenMap.put(token.getToken(), token);
            }
            return tokenMap;
        }

        private SendPushNotificationRes getSendPushNotificationRes(final Future<PushNotificationResponse<SimpleApnsPushNotification>> future) throws ExecutionException, InterruptedException {

            if (future.isSuccess()) {
                final PushNotificationResponse<SimpleApnsPushNotification> pushNotificationResponse = future.get();
                T pushToken = tokenMap.get(pushNotificationResponse.getPushNotification().getToken());
                SendPushNotificationRes res = new SendPushNotificationRes();
                res.setOriginalToken(pushToken.getToken());
                res.setPushMessageId(pushMessageId);
                res.setPushMessageType(pushMessageType);
                res.setDeviceType(PushMessageDeviceType.IOS.getValue());
                res.setSendDatetime(new Date());
                res.setIsSent(true);
				res.setUserId(pushToken.getMemberId());
				//Success Handling
				if (pushNotificationResponse.isAccepted()) {
                    logger.info("[threadID:" + threadId + "] IOS Push notification sent successfully to: " + pushNotificationResponse.getPushNotification().getToken());
                    res.setIsInvalidToken(false);
                    res.setIsUpdatedToken(false);
                    res.setIsSuccess(true);
                }
                //Fail Handling
                else {

                    logger.info("[threadID:" + threadId + "] IOS Push notification sent failed to: " + pushNotificationResponse.getPushNotification().getToken()+ " Error MSG: ["+pushNotificationResponse.getRejectionReason()+"]");
                    if("BadDeviceToken".equals(pushNotificationResponse.getRejectionReason()) || // Null push token or incorrect push token
                            "MissingDeviceToken".equals(pushNotificationResponse.getRejectionReason()) || //Empty push token
                            "Unregistered".equals(pushNotificationResponse.getRejectionReason()) //Deleted apps device
                            ){
                        res.setIsInvalidToken(true);
                    }
                    res.setRemarks(pushNotificationResponse.getRejectionReason());
                    res.setIsSuccess(false);
                }
                return res;
            }
            return null;
        }
	}

}