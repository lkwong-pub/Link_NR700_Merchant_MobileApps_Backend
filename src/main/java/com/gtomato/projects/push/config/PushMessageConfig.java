package com.gtomato.projects.push.config;

import java.util.HashMap;

import lombok.Data;

import com.gtomato.projects.base.config.BaseModuleConfig;
import com.gtomato.projects.base.responseObject.search.FieldDetailsResponse;

@Data
public class PushMessageConfig implements BaseModuleConfig{

	private static PushMessageConfig pushMessageConfig;
	public static PushMessageConfig getInstance(){
		if( pushMessageConfig == null ) pushMessageConfig = new PushMessageConfig();
		return pushMessageConfig;

	}
	private String modulePrefix = "/push";
	
	private Boolean isSearchable = false;
	
	private String multiplePath = "/"; 
	
	private Boolean isMultipleTable = true;

	private String moduleCode = "PUSH_MESSAGE";
	
	/**
	 * 1. Search field title, 
	 * 2. Search entity field name / "contents"(Keyword) + entity named query method name
	 * 3. Search field display type, 
	 * 4. Predefine data ( * Use only for Fields type = SEARCH_TYPE_REFERENCE ) 
	 */
	
	private FieldDetailsResponse[] searchDetails = new FieldDetailsResponse[]{
//		new FieldDetailsResponse("Demo Content","contents.Demo.findByKeyword", FieldDetailsResponse.SEARCH_TYPE_TEXT, null),
//		new FieldDetailsResponse("ID","id", FieldDetailsResponse.SEARCH_TYPE_INT, null),
//		new FieldDetailsResponse("ID Range","id", FieldDetailsResponse.SEARCH_TYPE_INTRANGE, null),
//		new FieldDetailsResponse("Date","date", FieldDetailsResponse.SEARCH_TYPE_DATE, null),
//		new FieldDetailsResponse("Date Range","date", FieldDetailsResponse.SEARCH_TYPE_DATERANGE, null),
//		new FieldDetailsResponse("Type","type", FieldDetailsResponse.SEARCH_TYPE_REFERENCE, new HashMap(){{put("1", "test1");put("2", "test2");}}),
//		new FieldDetailsResponse("Status","status", FieldDetailsResponse.SEARCH_TYPE_STATUS, null)
	};
	
	/**
	 * 1. List field title, 
	 * 2. List entity field name / sub entity field name
	 * 3. List field display type, 
	 * 4. Predefine data ( * Use only for Fields type = SEARCH_TYPE_REFERENCE ) 
	 */
	private FieldDetailsResponse[] listDetails = new FieldDetailsResponse[]{
		new FieldDetailsResponse("Status","status", FieldDetailsResponse.LIST_TYPE_STATUS, null),
		new FieldDetailsResponse("Title","contents.title", FieldDetailsResponse.LIST_TYPE_MULTIPLE_TEXT, null), 
		new FieldDetailsResponse("Publish Time","scheduleTime", FieldDetailsResponse.LIST_TYPE_DATETIME, null)
		//new FieldDetailsResponse("Target Type","targetType", FieldDetailsResponse.LIST_TYPE_REFERENCE, new HashMap(){{put(1, "ALL");put(2, "SPECIFIC");}}),
	};
	
	/**
	 * 1. Edit field title, 
	 * 2. Edit entity field name
	 * 3. Edit display type, 
	 * 4. Predefine data ( * Use only for Fields type = SEARCH_TYPE_REFERENCE ) 
	 */
	private FieldDetailsResponse[] updateMasterDetails = new FieldDetailsResponse[]{
//		new FieldDetailsResponse("Name","name", FieldDetailsResponse.DETAIL_TYPE_TEXT, null),
//		new FieldDetailsResponse("Publish Time","scheduleTime", FieldDetailsResponse.DETAIL_TYPE_DATETIME, null),
//		new FieldDetailsResponse("Priority","priority", FieldDetailsResponse.DETAIL_TYPE_REFERENCE, PushMessagePriority.getMap()),
//		new FieldDetailsResponse("Target Type","targetType", FieldDetailsResponse.DETAIL_TYPE_REFERENCE, new HashMap(){{put(1, "ALL");put(2, "SPECIFIC");}}),
		new FieldDetailsResponse("Status","status", FieldDetailsResponse.DETAIL_TYPE_STATUS, null)
	};
	
	/**
	 * Used for Mutliple Language
	 * 1. Edit sub entity field title, 
	 * 2. Edit sub entity field name
	 * 3. Edit display type, 
	 * 4. Predefine data ( * Use only for Fields type = SEARCH_TYPE_REFERENCE ) 
	 */
	private FieldDetailsResponse[] updateContentDetails = new FieldDetailsResponse[]{
		new FieldDetailsResponse("Title","title", FieldDetailsResponse.DETAIL_TYPE_TEXT, null),
		new FieldDetailsResponse("Content","content", FieldDetailsResponse.DETAIL_TYPE_TEXTAREA, null),
	};
	
}
