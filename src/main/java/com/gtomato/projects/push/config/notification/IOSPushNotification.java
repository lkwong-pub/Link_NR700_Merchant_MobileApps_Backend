package com.gtomato.projects.push.config.notification;//package com.gtomato.projects.push.config.notification;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ThreadPoolExecutor;
//
//import javax.servlet.ServletContextListener;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//
//import com.google.android.gcm.server.Constants;
//import com.google.android.gcm.server.Message;
//import com.google.android.gcm.server.MulticastResult;
//import com.google.android.gcm.server.Result;
//import com.google.android.gcm.server.Sender;
//import com.google.android.gcm.server.Message.Builder;
//import com.gtomato.projects.base.config.Constant;
//import com.gtomato.projects.base.service.SystemService;
//import com.gtomato.projects.push.entity.PushToken;
//import com.gtomato.projects.push.responseObject.api.BasePushToken;
//
//
//@Configuration
//public class IOSPushNotification<T extends BasePushToken> implements InitializingBean {
//
//	@Autowired
//	private SystemService systemService;
//
//	private ExecutorService threadPool = Executors.newCachedThreadPool();
//
//	public ExecutorService getThreadPool(){
//		return this.threadPool;
//	}
//
//	private String certPath;
//
//	private String password;
//
//	private Boolean isProduction;
//
//	public void setConfig(String certPath, String password, boolean isProduction){
//		this.certPath = certPath;
//		this.password = password;
//		this.isProduction = isProduction;
//	}
//
//	@Override
//	public void afterPropertiesSet() throws Exception {
//		((ThreadPoolExecutor) threadPool).setMaximumPoolSize(1000);
//		//this.threadPool = Executors.newCachedThreadPool();
//		Map<String, String> settingMap = this.systemService.findAllSystemSettingMap();
//		certPath = settingMap.get("iOSPushNotificationServiceCertificate");//this.getClass().getResource(settingMap.get("iOSPushNotificationServiceCertificate")).getPath();
//		password = settingMap.get("iOSPushNotificationServicePassword");
//		isProduction = Boolean.parseBoolean(settingMap.get("iOSPushNotificationServiceIsProduction"));
//	}
//
//	private final Logger logger = LoggerFactory.getLogger(this.getClass());
//
//	public void sendPushNotification(T pushToken, String messageContent, Integer badge, Map<String, Object> customDictionary) {
//		this.sendPushNotification(Arrays.asList(pushToken), messageContent, badge, customDictionary);
//	}
//
//	public void sendPushNotification(List<T> pushTokens, String messageContent, Integer badge, Map<String, Object> customDictionary) {
//		this.threadPool.submit(new PushNotificationRunnable(pushTokens, messageContent, badge, customDictionary));
//	}
//
//	public void sendPushNotification(String pushToken, String deviceId, String messageContent, Integer badge, Map<String, Object> customDictionary) {
//		this.threadPool.submit(new SinglePushNotificationRunnable(pushToken, deviceId, messageContent, badge, customDictionary));
//	}
//
//	private class PushNotificationRunnable implements Runnable {
//
//		private Integer maxPushPerReq = 5000;
//
//		private List<T> pushTokens;
//
//		private String messageContent;
//
//		private Map<String, Object> customDictionary;
//
//		private Integer badge;
//
//		private final Integer threadNo = 0;
//
//
//		public PushNotificationRunnable(List<T> pushTokens, String messageContent, Integer badge, Map<String, Object> customDictionary) {
//			this.pushTokens = pushTokens;
//			this.messageContent = messageContent;
//			this.customDictionary = customDictionary;
//			this.badge = badge;
//		}
//
//		public void run() {
//			final List<T> iPushTokens = pushTokens;
//			PushNotificationPayload pushNotificationPayload = null;
//			try {
//				pushNotificationPayload = PushNotificationPayload.complex();
//				pushNotificationPayload.addAlert(messageContent);
//				if (customDictionary != null)
//					for (String key : customDictionary.keySet())
//						pushNotificationPayload.addCustomDictionary(key, customDictionary.get(key));
//				if (badge != null)
//					pushNotificationPayload.addBadge(badge);
//
//				logger.debug(pushNotificationPayload.getPayload().toString());
//
//			} catch (Exception e) {
//				e.printStackTrace();
//				pushNotificationPayload = null;
//
//			}
//
//			final PushNotificationPayload iPushNotificationPayload = pushNotificationPayload;
//
//			if (iPushNotificationPayload != null) {
//				try {
//
//					int totalPage = iPushTokens.size() / this.maxPushPerReq;
//					if (iPushTokens.size() % this.maxPushPerReq != 0)
//						totalPage++;
//
//					for (int x = 0; x < totalPage; x++) {
//						List<Device> devices = new ArrayList<Device>();
//
//						for (int i = 0; i < this.maxPushPerReq; i++)
//							if (x * this.maxPushPerReq + i < iPushTokens.size()) {
//								T pushToken = iPushTokens.get(x * this.maxPushPerReq + i);
//								if (pushToken.getToken().length() == 64) {
//									Device device = new BasicDevice();
//									device.setDeviceId(pushToken.getDeviceId());
//									device.setToken(pushToken.getToken());
//									devices.add(device);
//								}
//							}
//
//						logger.info((x + 1) + ". iOS Push Size : " + devices.size());
//						List<PushedNotification> pushedNotifications = Push.payload(iPushNotificationPayload, //
//								certPath, //
//								password, //
//								isProduction, //
//								this.threadNo, //
//								devices);
//
//						for (int i = 0; i < pushedNotifications.size(); i++) {
//							try {
//								if (pushedNotifications.get(i).isSuccessful()) {
//									logger.info("Push notification sent successfully to: " + pushedNotifications.get(i).getDevice().getToken() + " & " + pushedNotifications.get(i).getDevice().getDeviceId());
//								} else {
//									logger.info("Push notification sent fail to: " + pushedNotifications.get(i).getDevice().getToken() + " & " + pushedNotifications.get(i).getDevice().getDeviceId());
//
//									Exception problem = pushedNotifications.get(i).getException();
//									problem.printStackTrace();
//
//									ResponsePacket errorResponse = pushedNotifications.get(i).getResponse();
//									if (errorResponse != null)
//										logger.info(errorResponse.getMessage());
//								}
//							} catch (Exception ee) {
//								ee.printStackTrace();
//							}
//
//						}
//					}
//
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//
//		}
//	}
//
//	private class SinglePushNotificationRunnable implements Runnable {
//
//		private Integer maxPushPerReq = 5000;
//
//		private String pushToken;
//
//		private String deviceId;
//
//		private String messageContent;
//
//		private Map<String, Object> customDictionary;
//
//		private Integer badge;
//
//		private final Integer threadNo = 0;
//
//
//		public SinglePushNotificationRunnable(String pushToken, String deviceId, String messageContent, Integer badge, Map<String, Object> customDictionary) {
//			this.pushToken = pushToken;
//			this.deviceId = deviceId;
//			this.messageContent = messageContent;
//			this.customDictionary = customDictionary;
//			this.badge = badge;
//		}
//
//		public void run() {
//			PushNotificationPayload pushNotificationPayload = null;
//			try {
//				pushNotificationPayload = PushNotificationPayload.complex();
//				pushNotificationPayload.addAlert(messageContent);
//				if (customDictionary != null)
//					for (String key : customDictionary.keySet())
//						pushNotificationPayload.addCustomDictionary(key, customDictionary.get(key));
//				if (badge != null)
//					pushNotificationPayload.addBadge(badge);
//
//				logger.debug(pushNotificationPayload.getPayload().toString());
//
//			} catch (Exception e) {
//				e.printStackTrace();
//				pushNotificationPayload = null;
//
//			}
//
//			final PushNotificationPayload iPushNotificationPayload = pushNotificationPayload;
//
//			if (iPushNotificationPayload != null) {
//				try {
//
//					if (pushToken.length() == 64) {
//						Device device = new BasicDevice();
//						device.setDeviceId(deviceId);
//						device.setToken(pushToken);
//
//						List<PushedNotification> pushedNotifications = Push.payload(iPushNotificationPayload, //
//						certPath, //
//						password, //
//						isProduction, //
//						this.threadNo, //
//						device);
//						for (int i = 0; i < pushedNotifications.size(); i++) {
//							try {
//								if (pushedNotifications.get(i).isSuccessful()) {
//									logger.info("Push notification sent successfully to: " + pushedNotifications.get(i).getDevice().getToken() + " & " + pushedNotifications.get(i).getDevice().getDeviceId());
//								} else {
//									logger.info("Push notification sent fail to: " + pushedNotifications.get(i).getDevice().getToken() + " & " + pushedNotifications.get(i).getDevice().getDeviceId());
//
//									Exception problem = pushedNotifications.get(i).getException();
//									problem.printStackTrace();
//
//									ResponsePacket errorResponse = pushedNotifications.get(i).getResponse();
//									if (errorResponse != null)
//										logger.info(errorResponse.getMessage());
//								}
//							} catch (Exception ee) {
//								ee.printStackTrace();
//							}
//
//						}
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//
//		}
//	}
//
//}