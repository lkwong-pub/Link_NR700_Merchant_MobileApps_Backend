package com.gtomato.projects.push.config.notification;

import com.google.android.gcm.server.Sender;
import com.gtomato.projects.base.config.Constant;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.*;

public class GCMProxySender extends Sender {


    Logger logger = Logger.getLogger(GCMProxySender.class);

    public GCMProxySender(String key) {
        super(key);
    }

    @Override
    protected HttpURLConnection getConnection(String url) throws IOException {

//        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("202.67.220.145", 8000));
//        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.49.108.65", 8080));  // production
        HttpURLConnection conn = null;

        logger.info("AOS Push Notification - Use proxy flag = " + Constant.getInstance().getBooleanProperty("push.notification.use.proxy", false));
        if(Constant.getInstance().getBooleanProperty("push.notification.use.proxy")){
            logger.info("AOS Push Notification - proxy URI & Port = " + Constant.getInstance().getProperty("push.notification.proxy.uri") +":"+ Constant.getInstance().getIntegerProperty("push.notification.proxy.port"));
            Proxy proxy = new Proxy(Proxy.Type.HTTP,
                    new InetSocketAddress(Constant.getInstance().getProperty("push.notification.proxy.uri"), Constant.getInstance().getIntegerProperty("push.notification.proxy.port")));
            conn = (HttpURLConnection) new URL(url).openConnection(proxy);

            if(StringUtils.isNotBlank(Constant.getInstance().getProperty("push.notification.username"))) {
                // this part is not necessary if authenitcation is not needed
                Authenticator authenticator = new Authenticator() {

                    public PasswordAuthentication getPasswordAuthentication() {
                        return (new PasswordAuthentication(Constant.getInstance().getProperty("push.notification.username"),
                                Constant.getInstance().getProperty("push.notification.password").toCharArray()));
                    }
                };
                Authenticator.setDefault(authenticator);
            }
        }else
            conn = (HttpURLConnection) new URL(url).openConnection();

        return conn;

    }
}
