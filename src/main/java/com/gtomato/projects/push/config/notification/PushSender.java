package com.gtomato.projects.push.config.notification;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gtomato.projects.push.entity.PushMessage;
import com.gtomato.projects.push.entity.PushMessageConfigMapping;
import com.gtomato.projects.push.entity.PushMessageContents;
import com.gtomato.projects.push.properties.PushMessageDeviceType;
import com.gtomato.projects.push.responseObject.api.BasePushToken;
import com.gtomato.projects.push.responseObject.service.SendPushNotificationReq;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.*;


@Configuration
public class PushSender<T extends BasePushToken> {
	@Autowired
	private IOSPushNotificationV2 iosPushNotification;
	
//	@Autowired
//	private IOSPushNotificationV2 iosPushNotificationV2;

	@Autowired
	private AndroidPushNotification<T> androidPushNotification;

	public void updatePushNotificationMapping(List<PushMessageConfigMapping> pushMessageConfigMappingList){
		this.iosPushNotification.updateConfig(pushMessageConfigMappingList);
		this.androidPushNotification.updateConfig(pushMessageConfigMappingList);
	};

	public void connectPushNotificationProvider(){
		this.iosPushNotification.initialConnection();
		this.androidPushNotification.initialConnection();
	}


	public void reconnectPushNotificationProvider(){
		this.iosPushNotification.reconnection();
	}

	public void disconnectPushNotificationProvider(){
		this.iosPushNotification.disconnection();
	}


	public Map<String, Object> getCustomDictionary(PushMessage pushMessage){
		Map<String, Object> customDictionary = null;
		try{
			if( StringUtils.isNotBlank( pushMessage.getRedirectDetails() ) )
				customDictionary = new ObjectMapper().readValue(pushMessage.getRedirectDetails(),new TypeReference<HashMap<String, Object>>(){});
		}catch(Exception e){
			customDictionary = null;
		}
		return customDictionary;
	}
	
	public void sendPushNotification(List<T> pushTokens, PushMessage pushMessage) {
		this.sendPushNotification(pushTokens, pushMessage, getCustomDictionary(pushMessage));
	}
	
	public void sendPushNotification(T pushToken, PushMessage pushMessage) {
		this.sendPushNotification(Arrays.asList(pushToken), pushMessage, getCustomDictionary(pushMessage));
	}
	
	public void sendPushNotification(T pushToken, PushMessage pushMessage, Map<String, Object> customDictionary) {
		this.sendPushNotification(Arrays.asList(pushToken), pushMessage, customDictionary);
	}

	public void sendPushNotification(List<T> pushTokens, PushMessage pushMessage, Map<String, Object> customDictionary) {
		this.sendPushNotification(pushTokens, pushMessage, null, customDictionary);
	}
	public void sendPushNotification(List<T> pushTokens, PushMessage pushMessage, HashMap<Long, Integer> badge, Map<String, Object> customDictionary) {

		Map<String, Map<Integer, List<T>>> pushTokenMap = new HashMap<String, Map<Integer, List<T>>>();

		for (T pushToken : pushTokens){
			Map<Integer, List<T>> pushTokenDeviceMap = pushTokenMap.get(pushToken.getLocale()) == null ? new HashMap<Integer, List<T>>() : pushTokenMap.get(pushToken.getLocale());
			List<T> tokenList = pushTokenDeviceMap.get(pushToken.getDeviceTypeInt()) == null ? new ArrayList<T>() : pushTokenDeviceMap.get(pushToken.getDeviceTypeInt());
			tokenList.add(pushToken);
			pushTokenDeviceMap.put(pushToken.getDeviceTypeInt(), tokenList);
			pushTokenMap.put(pushToken.getLocale(), pushTokenDeviceMap);
		}

		for(PushMessageContents pushMessageContent : pushMessage.getContents()){
			if(pushTokenMap.get(pushMessageContent.getLocale()) != null ){
				Map<Integer, List<T>> pushTokenDeviceMap = pushTokenMap.get(pushMessageContent.getLocale());
				for(Integer deviceType : pushTokenDeviceMap.keySet()){
					//TODO Custom handling for badge / sound etc
					HashMap<Long, Integer> tempBadge = new HashMap<Long, Integer>();
					for(T pushToken : pushTokenDeviceMap.get(deviceType)){
						if(pushToken.getMemberId() != null && badge != null) {
							tempBadge.put(pushToken.getMemberId(), badge.get(pushToken.getMemberId()));
						}
					}

					SendPushNotificationReq<T> req = new SendPushNotificationReq<T>(
							pushMessage.getId(),
							(pushMessage.getId() == null ? SendPushNotificationReq.PUSH_MESSAGE_TYPE_API : SendPushNotificationReq.PUSH_MESSAGE_TYPE_CMS),
							pushMessageContent.getContent(),
							customDictionary,
							tempBadge,
							pushTokenDeviceMap.get(deviceType),
							deviceType,
							pushMessage.getRegionId(),
							null,
							pushMessage.getPushConfigId()
					);
					this.sendPushNotification(req);
					//this.sendPushNotification(pushTokenDeviceMap.get(deviceType), pushMessageContent.getContent(), tempBadge, customDictionary, deviceType, pushMessage.getRegionId(),null,pushMessage.getPushConfigId());
				}
			}
		};
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer deviceType, Integer regionId) {
		this.sendPushNotification(pushTokens, messageContent, badge, customDictionary, deviceType, regionId, null, null);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer deviceType, Integer regionId, String appId){
		this.sendPushNotification(pushTokens, messageContent, badge, customDictionary, deviceType, regionId, appId, null);
	}

	public void sendPushNotification(List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer deviceType, Integer regionId, String appId, Integer pushConfigId) {
		this.sendPushNotification(null, pushTokens, messageContent, badge, customDictionary, deviceType, regionId, appId, pushConfigId, null);
	}

	public void sendPushNotification(SendPushNotificationReq req){
		this.sendPushNotification(req.getPushMessageId(), req.getPushTokens(), req.getMessageContent(), req.getBadge(), req.getCustomDictionary(), req.getDeviceType(), req.getRegionId(), req.getAppId(), req.getPushConfigId(), req.getPushMessageType() );
	}

	public void sendPushNotification(Integer pushMessageId, List<T> pushTokens, String messageContent, HashMap<Long, Integer> badge, Map<String, Object> customDictionary, Integer deviceType, Integer regionId, String appId, Integer pushConfigId, Integer pushMessageType) {
		if(PushMessageDeviceType.ANDROID.getValue() == deviceType)
			this.androidPushNotification.sendPushNotification(pushMessageId, pushTokens, messageContent, badge, customDictionary, regionId, appId, pushConfigId, pushMessageType);
		else if(PushMessageDeviceType.IOS.getValue() == deviceType)
			this.iosPushNotification.sendPushNotification(pushMessageId, pushTokens, messageContent, badge, customDictionary, regionId, appId, pushConfigId, pushMessageType);
		
	}
}
