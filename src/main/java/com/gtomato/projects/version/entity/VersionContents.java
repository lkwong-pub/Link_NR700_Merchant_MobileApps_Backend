package com.gtomato.projects.version.entity;


import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "version_contents")
@Data
public class VersionContents{

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column
	private String title;
	
	@Column
	private String content;
	
	@Column
	private String locale;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="master_id")
	private Version version;

	@Override
	public String toString() {
		return "VersionContents [id=" + id + ", title=" + title + ", content="
				+ content + ", locale=" + locale + ", versionId=" + ( version == null || version.getId() == null ? -1 : version.getId()) + "]";
	}

	
}
