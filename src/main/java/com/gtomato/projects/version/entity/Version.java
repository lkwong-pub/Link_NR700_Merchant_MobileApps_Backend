package com.gtomato.projects.version.entity;


import com.gtomato.projects.base.entity.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "version")
@Data 
public class Version extends BaseEntity implements Serializable{

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 2611889734771486474L;

	@Column(name="device_type")
	private Integer deviceType;
	
	@Column(name="is_force_update")
	private Integer isForceUpdate;
	
	@Column
	private String version;

	@Column
	private String url;
	
	@Column
	private String description;
	
	@Column(name="ls_latest_version")
	private Integer isLatestVersion;

	@Column(name="app_config_id")
	private Integer appConfigId;

	@OneToMany(mappedBy="version", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<VersionContents> contents;

	@Override
	public String toString() {
		return "Version [deviceType=" + deviceType + ", isForceUpdate="
				+ isForceUpdate + ", version=" + version + ", url=" + url
				+ ", description=" + description + ", isLatestVersion="
				+ isLatestVersion + ", appConfigId=" + appConfigId + ", contents=" + contents + "]";
	}
	
}
