package com.gtomato.projects.version.service;

import com.gtomato.projects.base.exception.ApiBaseException;
import com.gtomato.projects.base.service.BaseService;
import com.gtomato.projects.version.entity.Version;
import com.gtomato.projects.version.responseObject.api.VersionCheckingRequestObject;
import com.gtomato.projects.version.responseObject.api.VersionCheckingResponseObject;
import com.gtomato.projects.version.responseObject.search.VersionSearchCriteria;

public interface VersionService extends BaseService<Version, VersionSearchCriteria> {

	public VersionCheckingResponseObject checkVersion(VersionCheckingRequestObject versionCheckingRequestObject) throws ApiBaseException;

	public void clearAllLatestVersion(Integer deviceType, Integer appConfigId);
}
