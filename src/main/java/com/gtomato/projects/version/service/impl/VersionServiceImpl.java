package com.gtomato.projects.version.service.impl;

import com.gtomato.projects.base.exception.ApiBaseException;
import com.gtomato.projects.base.responseObject.properties.BaseBoolean;
import com.gtomato.projects.base.service.impl.BaseServiceImpl;
import com.gtomato.projects.push.entity.PushMessageConfigMapping;
import com.gtomato.projects.push.repository.PushMessageConfigMappingRepository;
import com.gtomato.projects.version.entity.Version;
import com.gtomato.projects.version.entity.VersionContents;
import com.gtomato.projects.version.repository.VersionRepository;
import com.gtomato.projects.version.responseObject.api.VersionCheckingRequestObject;
import com.gtomato.projects.version.responseObject.api.VersionCheckingResponseObject;
import com.gtomato.projects.version.responseObject.search.VersionDeviceType;
import com.gtomato.projects.version.responseObject.search.VersionSearchCriteria;
import com.gtomato.projects.version.service.VersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.regex.Pattern;

@Component("VersionService")
public class VersionServiceImpl extends BaseServiceImpl<Version, VersionSearchCriteria> implements VersionService {

	@PersistenceContext
	private EntityManager em;
	
	private VersionRepository versionRepository;

	@Autowired
	private PushMessageConfigMappingRepository pushMessageConfigMappingRepository;

	@Autowired
	public VersionServiceImpl(VersionRepository versionRepository){
		super(versionRepository);
		this.versionRepository = versionRepository;
	}
	
	final static int LESSER = -1; // versionA is lesser than versionB
	final static int EQUALS = 0; // versionA equal to versionB
	final static int GREATER = 1; // versionA is greater then versionB
	
	public VersionCheckingResponseObject checkVersion(VersionCheckingRequestObject versionCheckingRequestObject) throws ApiBaseException {

		
		VersionCheckingResponseObject versionResponseObject = new VersionCheckingResponseObject();
		
		
		String version = versionCheckingRequestObject.getVersion();
		Integer deviceType = versionCheckingRequestObject.getDeviceType();
		String lang = versionCheckingRequestObject.getLang();
		String appId = versionCheckingRequestObject.getAppId();
		//Version String checking
		Boolean isMatchVersion = Pattern.matches("\\d+.\\d+.\\d+",version);
		if( !isMatchVersion ){
			isMatchVersion = Pattern.matches("\\d+.\\d+",version);
			if( isMatchVersion ){
				version += ".0";
			}
			else{
				throw new ApiBaseException(HttpStatus.SERVICE_UNAVAILABLE, "9999", "System Error", "Incorrect version format" );
			}
		}
		PushMessageConfigMapping pushMessageConfig = null;
		if(VersionDeviceType.IOS.getValue() == deviceType && appId != null)
			pushMessageConfig = this.pushMessageConfigMappingRepository.findByIosAppId(appId);
		else if (VersionDeviceType.ANDROID.getValue() == deviceType && appId != null)
			pushMessageConfig = this.pushMessageConfigMappingRepository.findByAndroidAppId(appId);
		else
			pushMessageConfig = this.pushMessageConfigMappingRepository.findByIsDefaultTrue();

		List<Version> latestVersion = this.versionRepository.findByDeviceTypeAndAppConfigIdAndIsLatestVersion( deviceType, pushMessageConfig.getId() );
		
		//No version stored on server 
		if(latestVersion == null || latestVersion.size() == 0 ){
			versionResponseObject.setAction("none");
			versionResponseObject.setRemark("Case1");
		}
		else{
			Version releaseVersion = latestVersion.get(0);
			versionResponseObject.setVersion(latestVersion.get(0).getVersion());
			
			// Older version handling
			if(compareVersions(version, releaseVersion.getVersion() )  == VersionServiceImpl.LESSER ){
				List<Version> currentVersion = this.versionRepository.findByDeviceTypeAndAppConfigIdAndVersionAndAppId( deviceType, version, pushMessageConfig.getId() );
				//Version not exist
				if( currentVersion == null || currentVersion.size() == 0 ){
					versionResponseObject.setAction("force");
					versionResponseObject.setRemark("Case2A");
					versionResponseObject.setMessage(getContent(releaseVersion.getContents(), lang));
					versionResponseObject.setUrl(releaseVersion.getUrl());
				}
				//Version exist - action depends of current version 
				else{
					if( currentVersion.get(0).getIsForceUpdate() == BaseBoolean.YES.getValue() ){
						versionResponseObject.setAction("force");
						versionResponseObject.setRemark("Case2B");
					}
					else{
						versionResponseObject.setAction("optional");
						versionResponseObject.setRemark("Case2C");
					}
					versionResponseObject.setMessage(getContent(releaseVersion.getContents(), lang));
					versionResponseObject.setUrl(releaseVersion.getUrl());
				}
			}
			//Version Equal latest version
			else if (compareVersions(version, releaseVersion.getVersion() )  == VersionServiceImpl.EQUALS){
				versionResponseObject.setAction("none"); 
				versionResponseObject.setRemark("Case3");
			}
			//Submit version greater than or equal latest version(Apple submission support)
			else{
				versionResponseObject.setAction("none");
				versionResponseObject.setRemark("Case4");
			}
		}
		return versionResponseObject;
	}
	
	public void clearAllLatestVersion(Integer deviceType, Integer appConfigId){
		this.versionRepository.clearAllLatestVersion(deviceType, appConfigId);
	};

	public static int compareVersions(String versionA, String versionB) {
		String[] a = versionA.split("\\.");
		String[] b = versionB.split("\\.");
		int i, j;
		int index = 0;
		while ((index < a.length) && (index < b.length)) { 
			i = Integer.parseInt(a[index]); 
			j = Integer.parseInt(b[index]); 
			if (i > j) {
				return VersionServiceImpl.GREATER;
			} 
			else if (i < j) { 
				return VersionServiceImpl.LESSER;
				} 
			index++; 
		} 
		if ((index < a.length) && (index == b.length)) { 
			return VersionServiceImpl.GREATER;
		} 
		else if ((index == a.length) && (index < b.length)) { 
			return VersionServiceImpl.LESSER;
		} 
		else { 
			return VersionServiceImpl.EQUALS;
		}
	}

	public String getContent(List<VersionContents> contents, String lang){
		String str = "";
		boolean isMatch = false;
		for(VersionContents content : contents){
			if( content.getLocale().equals(lang) ){
				str = content.getContent();
				isMatch = true;
				break;
			}
		}
		if(!isMatch){
			System.out.println("Locale Not Match!!");
		}
		return str;
	}
//	/**
//	 *  Customized Get List Sample
//	 *  2 Param receive only (contents.name, type)
//	 */
//	@Override
//	public Page<Demo> getList( DemoSearchCriteria searchRequest ){
//		Pageable pageable = new PageRequest( ( searchRequest.getPage() - 1 ), searchRequest.getSize(), searchRequest.getSort());
//		
//		BaseSpecificationBuilder<Demo> baseSpecificationBuilder = new BaseSpecificationBuilder<Demo>();
//		Map<String,Object> paramMap = searchRequest.getParams();
//		Object name = paramMap.get("contents.findByKeyword");
//		if( name != null && StringUtils.isNotBlank( name.toString() ) ){
//			List idList = this.demoRepository.findByKeyword( "%" + name + "%");
//			if(idList != null && idList.size() > 0) 
//				baseSpecificationBuilder.with("id", "in", idList);
//			
//		} 
//		if( paramMap.get("type") != null && StringUtils.isNotBlank( paramMap.get("type").toString() ) ){
//			baseSpecificationBuilder.with("type", "=", Integer.parseInt( paramMap.get("type").toString() ) );
//		}
//		return baseRepository.findAll(baseSpecificationBuilder.build(), pageable);
//		
//	};
	
//	@Override
//	public HashMap<String, String> uploadFile(BaseFileUploadForm baseFileUploadForm, String uploadPath ) {
//		return new BaseFileUploadUtil( uploadPath ).uploadOriginalFile( baseFileUploadForm );
//	}
	
}
