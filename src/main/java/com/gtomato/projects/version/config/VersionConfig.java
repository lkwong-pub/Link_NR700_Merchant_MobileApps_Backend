package com.gtomato.projects.version.config;

import java.util.HashMap;

import lombok.Data;

import com.gtomato.projects.base.config.BaseModuleConfig;
import com.gtomato.projects.base.responseObject.properties.BaseBoolean;
import com.gtomato.projects.base.responseObject.search.FieldDetailsResponse;
import com.gtomato.projects.version.responseObject.search.VersionDeviceType;

@Data
public class VersionConfig implements BaseModuleConfig{

	private static VersionConfig demoConfig;
	public static VersionConfig getInstance(){
		if( demoConfig == null ) demoConfig = new VersionConfig();
		return demoConfig;

	}
	private String modulePrefix = "/version";
	
	private Boolean isSearchable = true;
	
	private String multiplePath = "/images"; 
	
	private Boolean isMultipleTable = true;

	private String moduleCode = "VERSION";
	
	/**
	 * 1. Search field title, 
	 * 2. Search entity field name / "contents"(Keyword) + entity named query method name
	 * 3. Search field display type, 
	 * 4. Predefine data ( * Use only for Fields type = SEARCH_TYPE_REFERENCE ) 
	 */
	
	private FieldDetailsResponse[] searchDetails = new FieldDetailsResponse[]{
		new FieldDetailsResponse("Device Type","deviceType", FieldDetailsResponse.SEARCH_TYPE_REFERENCE, VersionDeviceType.getValueSilmMap()),
		new FieldDetailsResponse("Status","status", FieldDetailsResponse.SEARCH_TYPE_STATUS, null)
	};
	
	/**
	 * 1. List field title, 
	 * 2. List entity field name / sub entity field name
	 * 3. List field display type, 
	 * 4. Predefine data ( * Use only for Fields type = SEARCH_TYPE_REFERENCE ) 
	 */
	private FieldDetailsResponse[] listDetails = new FieldDetailsResponse[]{
		new FieldDetailsResponse("Status","status", FieldDetailsResponse.LIST_TYPE_STATUS, null),
		new FieldDetailsResponse("Device Type","deviceType", FieldDetailsResponse.LIST_TYPE_REFERENCE, VersionDeviceType.getValueSilmMap()),
		new FieldDetailsResponse("Version","version", FieldDetailsResponse.LIST_TYPE_TEXT, null),
		new FieldDetailsResponse("Alert Message","contents.content", FieldDetailsResponse.LIST_TYPE_MULTIPLE_TEXT, null), 
		new FieldDetailsResponse("Is Force Update","isForceUpdate", FieldDetailsResponse.LIST_TYPE_REFERENCE, BaseBoolean.getCodeMap(Integer.class, 1)),
		new FieldDetailsResponse("Is Latest Version","isLatestVersion", FieldDetailsResponse.LIST_TYPE_REFERENCE, BaseBoolean.getCodeMap(Integer.class, 1)),
	};
	
	/**
	 * 1. Edit field title, 
	 * 2. Edit entity field name
	 * 3. Edit display type, 
	 * 4. Predefine data ( * Use only for Fields type = SEARCH_TYPE_REFERENCE ) 
	 */
	private FieldDetailsResponse[] updateMasterDetails = new FieldDetailsResponse[]{
		new FieldDetailsResponse("Device Type","deviceType", FieldDetailsResponse.DETAIL_TYPE_REFERENCE, VersionDeviceType.getValueSilmMap()),
		new FieldDetailsResponse("Version","version", FieldDetailsResponse.DETAIL_TYPE_TEXT, null), 
		new FieldDetailsResponse("Is Force Update","isForceUpdate", FieldDetailsResponse.DETAIL_TYPE_REFERENCE, BaseBoolean.getCodeMap(Integer.class, 1)),
		new FieldDetailsResponse("Is Latest Version","isLatestVersion", FieldDetailsResponse.DETAIL_TYPE_REFERENCE, BaseBoolean.getCodeMap(Integer.class, 1)),
		new FieldDetailsResponse("URL","url", FieldDetailsResponse.DETAIL_TYPE_TEXT, null),
		new FieldDetailsResponse("Status","status", FieldDetailsResponse.DETAIL_TYPE_STATUS, null)
	};
	
	/**
	 * Used for Mutliple Language
	 * 1. Edit sub entity field title, 
	 * 2. Edit sub entity field name
	 * 3. Edit display type, 
	 * 4. Predefine data ( * Use only for Fields type = SEARCH_TYPE_REFERENCE ) 
	 */
	private FieldDetailsResponse[] updateContentDetails = new FieldDetailsResponse[]{
		new FieldDetailsResponse("Alert","content", FieldDetailsResponse.DETAIL_TYPE_TEXTAREA, null),
	};
	
}
