package com.gtomato.projects.version.controller.admin;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gtomato.projects.base.config.Constant;
import com.gtomato.projects.base.config.Url;
import com.gtomato.projects.base.controller.admin.BaseBackendController;
import com.gtomato.projects.base.responseObject.admin.AdminControllerResponse;
import com.gtomato.projects.base.responseObject.form.BaseFileUploadForm;
import com.gtomato.projects.base.responseObject.properties.BaseBoolean;
import com.gtomato.projects.base.responseObject.search.FieldDetailsResponse;
import com.gtomato.projects.base.util.BeanUtil;
import com.gtomato.projects.version.config.VersionConfig;
import com.gtomato.projects.version.entity.Version;
import com.gtomato.projects.version.responseObject.search.VersionSearchCriteria;
import com.gtomato.projects.version.service.VersionService;

@Controller
@RequestMapping(Url.ADMIN_PREFIX + "/version")
public class VersionController extends BaseBackendController<Version, VersionSearchCriteria> { 
	public VersionService versionService;
	
	@Autowired
	public VersionController (VersionService versionService){
		super(versionService, VersionConfig.getInstance());
		this.versionService = versionService;
	}
	
	@Override
	@RequestMapping(value = "/{id}", method = {RequestMethod.POST, RequestMethod.PUT} )
	@ResponseBody
	public AdminControllerResponse update( @PathVariable("id") Integer id,
			@ModelAttribute Version t, @ModelAttribute BaseFileUploadForm baseFileUploadForm,
			Model model) throws IllegalStateException, IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
			
		//File Upload Handling
		HashMap<String, String> fieldMap = this.baseService.uploadFile(baseFileUploadForm, Constant.getInstance().getProperty("upload.directory") + UPLOAD_SUB_DIRECTORY);
		this.baseService.updateUploadField(t, fieldMap);
		
		if( VersionConfig.getInstance().getIsMultipleTable() ){
			List contents = (List)t.getClass().getMethod( "getContents" ).invoke(t);
			for( Object content : contents ){
				content.getClass().getMethod("set" + t.getClass().getSimpleName(), t.getClass()).invoke(content, t);
			}
		}
		
		if(t.getIsLatestVersion() == BaseBoolean.YES.getValue()){
			this.versionService.clearAllLatestVersion(t.getDeviceType(), 1);
		}
		
		log.info("Model Attribute : " + t);
		if( id == null || id == 0 ){
			this.baseService.saveOrUpdate( t );
			log.debug("Create Item");
		}
		else{
			Version existT = this.baseService.findByPK( id );
			BeanUtil.myCopyProperties( t, existT );
			this.baseService.saveOrUpdate( existT );
			log.debug("Update Item - ID:" + id);
		}
		
		AdminControllerResponse adminControllerResponse = new AdminControllerResponse();
		adminControllerResponse.setForwardUrl(Constant.getInstance().getProperty("app.path") + ADMIN_PREFIX + CTRL_PREFIX + LIST_PATH);
		
		return adminControllerResponse;
	}
	
//	//Customized List / Add / Update View ( Uncomment if need )
//	//@Override
//	@RequestMapping(value="/list2", method = RequestMethod.GET)
//	public String list2(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page, //
//			@RequestParam(value = "size", required = false, defaultValue = "20") Integer size, //
//			@RequestParam(value = "orderingField", required = false, defaultValue = "id") String orderingField,//
//			@RequestParam(value = "ordering", required = false, defaultValue = "desc") String ordering,//
//			Model model) {
//		Page<Demo> pages = this.demoService.getList(page, size, orderingField, ordering);
//		System.out.println("pages:"+ pages.getContent());
//		return ADMIN_PREFIX + CTRL_PREFIX + LIST_PATH;
//	}
//	
//	@Override
//	@RequestMapping(value="/", method = RequestMethod.GET)
//	public String details(Model model) throws InstantiationException, IllegalAccessException{
//		return this.get(0, model);
//	}
//	
//	@Override
//	@RequestMapping(value="/{id}", method = RequestMethod.GET)
//	public String get(@PathVariable("id") Integer id, //
//			Model model) throws InstantiationException, IllegalAccessException {
//		
//		return ADMIN_PREFIX + CTRL_PREFIX + DETAILS_PATH;
//	}
}