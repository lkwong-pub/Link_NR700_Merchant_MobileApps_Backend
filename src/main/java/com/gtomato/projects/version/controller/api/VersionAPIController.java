package com.gtomato.projects.version.controller.api;

import java.util.Map;

import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gtomato.projects.base.config.Url;
import com.gtomato.projects.base.controller.api.BaseAPIController;
import com.gtomato.projects.base.exception.ApiBaseException;
import com.gtomato.projects.base.responseObject.api.BaseAPIDetailResponse;
import com.gtomato.projects.base.responseObject.api.BaseAPIResponse;
import com.gtomato.projects.base.responseObject.search.FieldDetailsResponse;
import com.gtomato.projects.base.util.BeanUtil;
import com.gtomato.projects.version.entity.Version;
import com.gtomato.projects.version.responseObject.api.VersionCheckingRequestObject;
import com.gtomato.projects.version.responseObject.api.VersionCheckingResponseObject;
import com.gtomato.projects.version.responseObject.api.VersionDetailResponseObject;
import com.gtomato.projects.version.responseObject.api.VersionListResponseObject;
import com.gtomato.projects.version.responseObject.search.VersionSearchCriteria;
import com.gtomato.projects.version.service.VersionService;

@Controller
@RequestMapping(Url.API_PREFIX + "/version")
public class VersionAPIController extends BaseAPIController<Version, VersionSearchCriteria, VersionListResponseObject, VersionDetailResponseObject>{

	public static final FieldDetailsResponse[] SEARCH_DETAILS = new FieldDetailsResponse[0];
	
	private VersionService versionService; 
	
	@Autowired
	public VersionAPIController (VersionService versionService){
		super(versionService, SEARCH_DETAILS, new VersionListResponseObject(), new VersionDetailResponseObject() );
		this.versionService = versionService;
	}
	@RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<BaseAPIDetailResponse<VersionCheckingResponseObject>> checkVersion(@ModelAttribute VersionCheckingRequestObject versionCheckingRequestObject) {
		
		/* Initial Response Object */
		BaseAPIDetailResponse<VersionCheckingResponseObject> response = new BaseAPIDetailResponse<VersionCheckingResponseObject>();
		HttpStatus statusCode = HttpStatus.OK;
		
		/* Base Criteria Checking 
		 * 1. Return HttpStatus.BAD_REQUEST (400) if not enough data is provided
		 * 2. Return HttpStatus.CONFLICT (409) if on the server side is determined a record is exists  
		 *  */
				
		/* Main Logic of API */
		try{
			VersionCheckingResponseObject responseObject = this.versionService.checkVersion(versionCheckingRequestObject);
			response.setDetail( responseObject );
		}catch(ApiBaseException e){
			e.printStackTrace();
			statusCode = HttpStatus.SERVICE_UNAVAILABLE;
			response.setMessage(e.getMessage());
			response.setRemark(e.getDeveloperMessage());
		}
		catch(Exception e){
			e.printStackTrace();
			statusCode = HttpStatus.SERVICE_UNAVAILABLE;
			response.setRemark(e.getMessage());
		}
		
	    /* Return Response Object with correct Http Status Code */
	    return new ResponseEntity<BaseAPIDetailResponse<VersionCheckingResponseObject>>(response, statusCode);
    }
}
