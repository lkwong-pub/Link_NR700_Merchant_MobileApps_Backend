package com.gtomato.projects.version.repository;

import com.gtomato.projects.base.repository.BaseRepository;
import com.gtomato.projects.version.entity.Version;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VersionRepository extends BaseRepository<Version> {
	
	//Find Last version record 
	@Query("SELECT t FROM Version t WHERE t.deviceType = :deviceType AND status = 0 ORDER BY t.version desc")
	public List<Version> findByDeviceTypeOrderByVersionDesc(@Param("deviceType") Integer deviceType);
	
	//Find current version record
	@Query("SELECT t FROM Version t WHERE t.deviceType = :deviceType AND t.version = :version AND status = 0 ")
	public List<Version> findByDeviceTypeAndVersion(@Param("deviceType") Integer deviceType, @Param("version") String version);
	
	//Find latest version record
	@Query("SELECT t FROM Version t WHERE t.deviceType = :deviceType AND t.isLatestVersion = 1  AND status = 0 ORDER BY t.version desc")
	public List<Version> findByDeviceTypeAndIsLatestVersion(@Param("deviceType") Integer deviceType);
	
	@Modifying
	@Query("update Version t set t.isLatestVersion = 0 where t.isLatestVersion = 1 and t.deviceType = :deviceType and t.appConfigId = :appConfigId")
	public int clearAllLatestVersion(@Param("deviceType") Integer deviceType, @Param("appConfigId") Integer appConfigId);

	//Find Last version record with appId
	@Query("SELECT t FROM Version t WHERE t.deviceType = :deviceType and t.appConfigId = :appConfigId AND t.isLatestVersion = 1  AND status = 0 ORDER BY t.version desc")
	public List<Version> findByDeviceTypeAndAppConfigIdAndIsLatestVersion(@Param("deviceType") Integer deviceType, @Param("appConfigId") Integer appConfigId);

	//Find current version record with AppId
	@Query("SELECT t FROM Version t WHERE t.deviceType = :deviceType AND t.version = :version and t.appConfigId = :appConfigId AND status = 0 ")
	public List<Version> findByDeviceTypeAndAppConfigIdAndVersionAndAppId(@Param("deviceType") Integer deviceType, @Param("version") String version, @Param("appConfigId") Integer appConfigId);
	
	
}
