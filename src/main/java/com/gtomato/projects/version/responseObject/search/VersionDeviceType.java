package com.gtomato.projects.version.responseObject.search;

import lombok.Getter;

import java.util.HashMap;

@Getter
public enum VersionDeviceType {
	
	IOS(1, "i", "iOS"),
	ANDROID(2, "a", "Android"),
	IPAD(3, "ipad", "IPad"),
	DB(4, "db", "DB");
	
	private int value;
	private String code;
	private String name;
	
	
	VersionDeviceType( int value, String code, String name ){
		this.value = value;
		this.code = code;
		this.name = name;
	}
	
	public static VersionDeviceType getObject(int value ){
		for(VersionDeviceType versionDeviceType : VersionDeviceType.values()){
			if(versionDeviceType.value == value )
				return versionDeviceType;
		}
		return null;
	};
	
	public static VersionDeviceType getObject(String code ){
		for(VersionDeviceType versionDeviceType : VersionDeviceType.values()){
			if(versionDeviceType.code == code )
				return versionDeviceType;
		}
		return null;
	};
	
	
	public static HashMap getCodeMap(){
		HashMap map = new HashMap();
		for(VersionDeviceType pushMessageValue : VersionDeviceType.values())
			map.put(pushMessageValue.getCode(), pushMessageValue.getName());
		return map;
	}
	
	public static HashMap getValueMap(){
		HashMap map = new HashMap();
		for(VersionDeviceType pushMessageValue : VersionDeviceType.values())
			map.put(pushMessageValue.getValue(), pushMessageValue.getName());
		return map;
	}
	
	public static HashMap getCodeSilmMap(){
		HashMap map = new HashMap();
		map.put(IOS.getCode(), IOS.getName());
		map.put(ANDROID.getCode(), ANDROID.getName());
		return map;
	}
	
	public static HashMap getValueSilmMap(){
		HashMap map = new HashMap();
		map.put(IOS.getValue(), IOS.getName());
		map.put(ANDROID.getValue(), ANDROID.getName());
		return map;
	}

}
