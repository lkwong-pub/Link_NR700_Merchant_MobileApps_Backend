package com.gtomato.projects.version.responseObject.api;

import com.gtomato.projects.base.responseObject.api.BaseResponseObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VersionCheckingResponseObject extends BaseResponseObject {

	public String version;
	public String action;
	public String url;
	public String message;
	public String remark;

}
