package com.gtomato.projects.version.responseObject.api;

import com.gtomato.projects.base.responseObject.api.BaseResponseObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VersionCheckingRequestObject extends BaseResponseObject {

	public String version;
	public Integer deviceType;
	public String lang;
	public String appId;

}
