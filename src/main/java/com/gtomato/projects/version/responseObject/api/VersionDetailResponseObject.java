package com.gtomato.projects.version.responseObject.api;

import com.gtomato.projects.base.responseObject.api.BaseResponseObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VersionDetailResponseObject extends BaseResponseObject {

	public Integer id;
	public String name;
	public String content;
	public String image;
	public String date;

}
