<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>
<_AJAX_>


<_PAGE_ id="dialogFrag"><![CDATA[
<div id="dialog" class="modal fade"></div>
]]></_PAGE_>

<_PAGE_ id="confirmFrag"><![CDATA[
<div id="alertBackground" class="modal-backdrop"></div>
]]></_PAGE_>

<_PAGE_ id="progressBarFrag"><![CDATA[
<div id="progressBar" class="progressBar"><img src="${pageContext.request.contextPath}/static/images/progressBar_m.gif" alt=""/><span>Loading ...</span></div>
]]></_PAGE_>

<!-- 当有modal fade 这两个CSS时会被bootstrap框架检查到而使用CSS动画效果 in为目标元素渐显 -->
<_PAGE_ id="alertBoxFrag"><![CDATA[
<div id="alertMsgBox" class="modal fade in alert alert-danger">
	<a class="close" data-dismiss="alert" onclick="javascript:alertMsg.close()">
		<i class="icon-large icon-remove-circle"></i>
	</a>
	<h4 class="alert-heading">#title#</h4>
	<p>#message#</p><p>#butFragment#</p></div>
]]></_PAGE_>


<_PAGE_ id="alertButFrag"><![CDATA[
<a class="btn" rel="#callback#" onclick="alertMsg.close()" href="javascript:"><span>#butMsg#</span></a>
]]></_PAGE_>

<_MSG_ id="statusCode_503"><![CDATA[Server is down!]]></_MSG_>
<_MSG_ id="sessionTimout"><![CDATA[Session Timeout, please login again.]]></_MSG_>
<_MSG_ id="alertSelectMsg"><![CDATA[Please Select one record!]]></_MSG_>


</_AJAX_>