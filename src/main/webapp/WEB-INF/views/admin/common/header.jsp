<%@ include file="/include.inc.jsp"%>

<div id="header">
	<h1>
		<a href="${indexPath}">${ projectName }</a>
	</h1>
	<a id="menu-trigger" href="#"><i class="fa fa-bars"></i></a>
</div>
<div id="user-nav">
	<ul class="btn-group">
		<li class="btn">
			<a title="<spring:message code="GLOBAL.CHANGEPASSWORD"/>" data-pjax href="${appPath}/admin/password/${ administrator.id }">
				<i class="fa fa-key"></i><span class="text"><spring:message code="GLOBAL.CHANGEPASSWORD"/></span>
			</a>
		</li>
		<li class="btn">
			<a title="" href="${appPath}/admin/logout">
			<i class="fa fa-share"></i><span class="text">Logout</span>
			</a>
		</li>
	</ul>
</div>
