<%@ include file="/include.inc.jsp"%>
<div id="sidebar">
	<ul>
		
        <%-- <li class="submenu"><a href="#"><i class="fa fa-user"></i><span>Admin</span><i class="arrow fa fa-chevron-right"></i></a>
            <ul>

				<li><a href="#User" hrefx="${pageContext.request.contextPath}/admin/user/list.do"><span>User</span></a></li>
				<li><a href="#Role" hrefx="${pageContext.request.contextPath}/admin/role/list.do"><span>Role</span></a></li>
				<li><a href="#Permission" hrefx="${pageContext.request.contextPath}/admin/permission/list.do"><i class="fa fa-tasks"></i> <span>Permission</span></a></li>

            </ul>
        </li>
        

        <li><a href="#Version" hrefx="${pageContext.request.contextPath}/admin/version/list.do"><i class="fa fa-puzzle-piece"></i> <span>Version Control</span></a></li>
        
        <li><a href="#User" hrefx="${pageContext.request.contextPath}/umt/admin/user/list.do"><i class="fa fa-puzzle-piece"></i> <span>User</span></a></li>
 --%>
       <%--  <li><a href="#Demo" hrefx="${pageContext.request.contextPath}/admin/demo/"><i class="fa fa-puzzle-piece"></i> <span>Demo</span></a></li> --%>
        <sec:authorize access="hasAnyRole('ROLE_SUPERADMIN','PERMISSION_ROLE_VIEW','PERMISSION_ADMIN_VIEW')">
        <li class="submenu"><a href="#"><i class="fa fa-user"></i><span>Admin</span><i class="arrow fa fa-chevron-right"></i></a>
            <ul>
				<%--<sec:authorize access="hasRole('ROLE_SUPERADMIN')">--%>
					<%--<li><a data-pjax href="${appPath}/admin/permission/list"><i class="fa fa-star-half-o"></i> <span>Permission</span></a></li>--%>
				<%--</sec:authorize>--%>
				<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_ROLE_VIEW')">
					<li><a data-pjax href="${appPath}/admin/role/list"><i class="fa fa-users"></i> <span>Role</span></a></li>
				</sec:authorize>
				<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_ADMIN_VIEW')">
					<li><a data-pjax href="${appPath}/admin/admin/list"><i class="fa fa-user"></i> <span>User</span></a></li>
				</sec:authorize>
                <%--<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_AUDIT_VIEW')">--%>
                    <%--<li><a data-pjax href="${appPath}/admin/auditlog/list"><i class="fa fa-user"></i> <span>Audit Log</span></a></li>--%>
                <%--</sec:authorize>--%>
            </ul>
        </li>
        </sec:authorize>
        <%--&lt;%&ndash;<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_DEMO_VIEW')">&ndash;%&gt;--%>
        <%--<li><a data-pjax href="${appPath}/admin/gift/list"><i class="fa fa-puzzle-piece"></i> <span>Gift</span></a></li>--%>
        <%--&lt;%&ndash;</sec:authorize>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_DEMO_VIEW')">&ndash;%&gt;--%>
        <%--<li><a data-pjax href="${appPath}/admin/import/list"><i class="fa fa-puzzle-piece"></i> <span>Import Data</span></a></li>--%>
        <%--&lt;%&ndash;</sec:authorize>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_DEMO_VIEW')">&ndash;%&gt;--%>
        <%--<li><a data-pjax href="${appPath}/admin/report/list"><i class="fa fa-puzzle-piece"></i> <span>Report</span></a></li>--%>
        <%--</sec:authorize>--%>
        <%--<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_VERSION_VIEW')">--%>
        	<%--<li><a data-pjax href="${appPath}/admin/version/list"><i class="fa fa-puzzle-piece"></i> <span>Version Control</span></a></li>--%>
        <%--</sec:authorize>--%>
        <%--<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_PUSH_VIEW')">--%>
			<%--<li><a data-pjax href="${appPath}/admin/push/list"><i class="fa fa-puzzle-piece"></i> <span>Push</span></a></li>--%>
        <%--</sec:authorize>--%>
        <%--&lt;%&ndash; <li><a data-pjax href="${appPath}/admin/push/thread"><i class="fa fa-puzzle-piece"></i> <span>Thread</span></a></li> &ndash;%&gt;--%>
        <%--<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_DEMO_VIEW')">--%>
        	<%--<li><a data-pjax href="${appPath}/admin/demo/list"><i class="fa fa-puzzle-piece"></i> <span>Demo</span></a></li>--%>
        <%--</sec:authorize>--%>
        
		</ul>
</div>
