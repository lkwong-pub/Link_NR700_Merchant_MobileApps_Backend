<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true" %><%@ include file="/include.inc.jsp"%>{
	"statusCode":"300",
	"message":"<spring:message code="ERROR.CODE.${exception.code}"/>",
	"closeDialog":"",
	"formSubmit":"",
	"rel":"",
	"forwardUrl":""
}
