<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>
<!-- BEGIN PAGE HEADER-->
<div id="content-header">
	<h1><spring:message code="GLOBAL.THREAD"/></h1>
</div>
<div id="breadcrumb">
	<a data-pjax href="${indexPath}" title="<spring:message code="GLOBAL.HOME"/>" class="tip-bottom"><i class="fa fa-home"></i><spring:message code="GLOBAL.HOME"/></a>
	<a data-pjax href=""  class="current"><spring:message code="GLOBAL.THREAD"/></a>
</div>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-xs-12">
		
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"> <i class="fa fa-pencil-square-o"></i>
				</span>
					<ul class="nav nav-tabs">
						<li class="active"><a class="contentTab" data-toggle="tab" href="#tab1">Main</a></li>
					</ul>
			</div>
			<form data-pjax id="demoForm" class="form-horizontal" method="POST" action="${appPath}${updatePath}${id}" onsubmit="return ajaxSubmitCallback(this)" enctype="multipart/form-data" novalidate>
				<div class="widget-content nopadding tab-content">
					<div id="tab1" class="tab-pane active">
						<c:forEach var="thread" items="${threads}">
							<div class="form-group">
								<label class="col-sm-1 col-md-1 col-lg-1 control-label"><c:if test="${thread.key != 0}">ID: ${thread.key}</c:if></label>
								<div class="col-sm-9 col-md-9 col-lg-10">
										${thread.value}
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
