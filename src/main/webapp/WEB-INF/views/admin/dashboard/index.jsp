<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>
	
<!-- BEGIN PAGE HEADER-->
<div id="content-header">
	<h1>${ projectName }</h1>
	<%-- <div class="btn-group">
		<a class="btn btn-large" data-pjax title="<spring:message code="DEMO.ADD"/>" data-pjax href="${appPath}${updatePath}" target="dialog"><i class="fa fa-plus"></i></a>		
	</div> --%>
</div>
<div id="breadcrumb">
	<a data-pjax href="${indexPath }" class="current" title="<spring:message code="GLOBAL.HOME"/>" class="tip-bottom"><i class="fa fa-home"></i><spring:message code="GLOBAL.HOME"/></a>
</div>
<!-- END PAGE HEADER-->
<div class="row">

	<div class="col-xs-12 col-sm-6 col-lg-6">

		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"><i class="fa fa-arrow-right"></i></span>
				<h5>Welcome to ${ projectName }</h5>
			</div>
			<div class="collapse in" id="collapseOne">
				<div class="widget-content">Last Login Time: <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${administrator.lastLoginDatetime }"/></div>
			</div>
		</div>

	</div>
</div>