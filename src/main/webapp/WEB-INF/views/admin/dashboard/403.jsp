<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>
	
<!-- BEGIN PAGE HEADER-->
<div id="content-header">
	<h1>${ projectName }</h1>
	<%-- <div class="btn-group">
		<a class="btn btn-large" data-pjax title="<spring:message code="DEMO.ADD"/>" data-pjax href="${appPath}${updatePath}" target="dialog"><i class="fa fa-plus"></i></a>		
	</div> --%>
</div>
<div id="breadcrumb">
	<a data-pjax href="${indexPath }" class="current" title="<spring:message code="GLOBAL.HOME"/>" class="tip-bottom"><i class="fa fa-home"></i><spring:message code="GLOBAL.HOME"/></a>
</div>
<!-- END PAGE HEADER-->
<div class="row">

	<div class="col-xs-12 col-sm-6 col-lg-6">

		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"><i class="fa fa fa-bell"></i></span>
				<h5>Access Denied</h5>
			</div>
			<div class="widget-content">
				<div class="alert alert-danger alert-block">
					<h4 class="alert-heading">Error!</h4>
					You do not have permission to access.
				</div>
			</div>
		</div>

	</div>
</div>