<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js" lang="en">
<head>
<title>${ projectName }</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<%@ include file="/include.inc.jsp"%>
<%@ include file="/include.js.jsp"%>
<!--[if lt IE 9]>
	<script type="text/javascript" src="js/respond.min.js"></script>
<![endif]-->

</head>
<body data-color="grey" class="flat" onunload="">
	<div id="wrapper">
	
		<%@ include file="common/header.jsp"%>
		
		<!-- Search Bar -->
		<!-- <div class="clearfix"></div> -->
		
		<!-- BEGIN CONTAINER -->
		
			<%@ include file="common/sidebar.jsp"%>
			
			<div id="content"><c:import url="../${ content }.jsp"/></div>
			
		<%@ include file="common/footer.jsp"%>
	</div>
</body>
<div id="progressBar" class="progressBar"><img src="${appPath}/static/images/progressBar_m.gif" alt=""/><span>Loading ...</span></div>
<div id="dialog" class="modal fade"></div>

</html>