<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>

<!-- BEGIN PAGE HEADER-->
<div id="content-header">
	<h1><spring:message code="ROLE.TITLE"/></h1>
	<%-- <div class="btn-group">
		<a class="btn btn-large" title="<spring:message code="BMFUSER.ADDTITLE"/>" data-pjax href="${appPath}${updatePath}" target="dialog"><i class="fa fa-plus"></i></a>		
	</div> --%>
</div>
<div id="breadcrumb">
	<a data-pjax href="${indexPath}" title="<spring:message code="GLOBAL.HOME"/>" class="tip-bottom"><i class="fa fa-home"></i><spring:message code="GLOBAL.HOME"/></a>
	<a data-pjax href="${appPath}${listPath}" title="<spring:message code="ROLE.TITLE"/>" class="tip-bottom"><spring:message code="ROLE.TITLE"/></a>
	<a data-pjax href=""  class="current"><spring:message code="ROLE.PERMISSION"/></a>
</div>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-xs-12">
		
		<div class="widget-box">
			<form data-pjax id="demoForm" class="form-horizontal" method="${ method }" action="${appPath}${updatePath}${id}" onsubmit="return ajaxSubmitCallback(this)" enctype="multipart/form-data" novalidate>
				<div class="widget-content nopadding">
					<table class="table table-bordered table-striped table-hover data-table">
						<thead>
						<tr>
							<th>Section</th>
							<!-- <th>Permission</th> -->
							<c:forEach var="actionType" items="${actionTypeMap}">
									<th>${actionType.value }</th>
							</c:forEach>
						</tr>
						</thead>
						<tbody align="center">
							<c:forEach var="moduleType" items="${moduleTypeMap}">
							<tr>
								<td><label class="control-label"><spring:message code="${moduleType.value }"/></label></td>
								<c:forEach var="actionType" items="${actionTypeMap}">
									<td>
										<c:forEach var="permission" items="${permissions}">
											<c:if test="${permission.moduleTypeId == moduleType.key and permission.actionTypeId == actionType.key }">
												<input type="checkbox" name="permissionIds" 
												<c:forEach var="rolePermission" items="${role.permissions}">
													<c:if test="${rolePermission.id == permission.id }">
														checked
													</c:if>
												</c:forEach>
												value="${permission.id}"/> 
												<c:if test="${actionType.key == '5' }">${permission.name } <br/></c:if> 
											</c:if>
										</c:forEach>
									</td>
								</c:forEach>
							</tr>
							</c:forEach>
						</tbody>
						</table>
					<div class="modal-footer">
						<a data-pjax href="${appPath}${listPath}" class="btn btn-default btn-small" data-dismiss="modal"><spring:message code="GLOBAL.CANCEL"/></a>
						<button type="submit" class="btn btn-primary btn-small"><spring:message code="GLOBAL.SAVE"/></button>
					</div>
				</div>
			</form>
			
		</div>
	</div>
</div>
