<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>
<script>
$(function(){
	$('.datepicker').each(function(){
		$('#'+$(this).attr('id')).datetimepicker({
		format: 'YYYY/MM/DD'
		});	
	});
	$('.datetimepicker').each(function(){
		$('#'+$(this).attr('id')).datetimepicker({
		format: 'YYYY/MM/DD hh:mm'
		});	
	});
	
});
</script>
<!-- BEGIN PAGE HEADER-->
<div id="content-header">
	<h1><spring:message code="PUSHMESSAGE.TITLE"/></h1>
	<%-- <div class="btn-group">
		<a class="btn btn-large" title="<spring:message code="BMFUSER.ADDTITLE"/>" data-pjax href="${appPath}${updatePath}" target="dialog"><i class="fa fa-plus"></i></a>		
	</div> --%>
</div>
<div id="breadcrumb">
	<a data-pjax href="${indexPath}" title="<spring:message code="GLOBAL.HOME"/>" class="tip-bottom"><i class="fa fa-home"></i><spring:message code="GLOBAL.HOME"/></a>
	<a data-pjax href="${appPath}${listPath}" title="<spring:message code="PUSHMESSAGE.TITLE"/>" class="tip-bottom"><spring:message code="PUSHMESSAGE.TITLE"/></a>
	<a data-pjax href=""  class="current"><c:if test="${empty id}"><spring:message code="PUSHMESSAGE.ADD"/></c:if><c:if test="${not empty id}"><spring:message code="PUSHMESSAGE.UPDATE"/></c:if></a>
</div>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-xs-12">
		
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"> <i class="fa fa-pencil-square-o"></i>
				</span>
					<ul class="nav nav-tabs">
						<li class="active"><a class="contentTab" data-toggle="tab" href="#tab1">Main</a></li>
						<c:if test="${ isMultipleTable }">
						<c:if test="${not empty global['support.languages'] }">
							<c:set var="supportLanguages" value="${fn:split( global['support.languages'], ',')}" />
							<c:forEach var="lang" items="${supportLanguages }">
								<li><a class="contentTab" data-toggle="tab" href="#tab_${ lang }"><spring:message code="${ lang }"></spring:message></a></li>
							</c:forEach>
						</c:if>
						</c:if>
					</ul>
			</div>
			<form data-pjax id="demoForm" class="form-horizontal" method="${ method }" action="${appPath}${updatePath}${id}" onsubmit="return ajaxSubmitCallback(this)" enctype="multipart/form-data" novalidate>
				<div class="widget-content nopadding tab-content">
		
					<div id="tab1" class="tab-pane active">
						<%--Customized Fields --%>
						<c:if test="${ empty id }">
							<div class="form-group">
								<label class="col-sm-3 col-md-3 col-lg-2 control-label">Input Type</label>
								<div class="col-sm-9 col-md-9 col-lg-10">	
									 <select id="inputType" name="inputType" class="form-control">
										<option value="0" <c:if test="${'0' eq value}">selected</c:if>>All</option>
										<option value="1" <c:if test="${'1' eq value}">selected</c:if>>Member ID</option>
										<option value="2" <c:if test="${'2' eq value}">selected</c:if>>File</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-3 col-lg-2 control-label">Member ID</label>
								<div class="col-sm-9 col-md-9 col-lg-10">	
									 <input type="text" name="memberId" class="form-control input-sm"  placeholder="Member ID"
									 data-original-title="Member ID" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-3 col-lg-2 control-label">File</label>
								<div class="col-sm-9 col-md-9 col-lg-10">	
									 <input type="file" name="memberFile"/>
								</div>
							</div>
							<%-- <div class="form-group">
								<label class="col-sm-3 col-md-3 col-lg-2 control-label">Redirect Section</label>
								<div class="col-sm-9 col-md-9 col-lg-10">	
									 <select id="section" name="redirectType" class="form-control">
										<option value="1" <c:if test="${'1' eq value}">selected</c:if>>Section 1</option>
										<option value="2" <c:if test="${'2' eq value}">selected</c:if>>Section 2</option>
										<option value="3" <c:if test="${'3' eq value}">selected</c:if>>Section 3</option>
										<option value="4" <c:if test="${'4' eq value}">selected</c:if>>Section 4</option>
									</select>
								</div>
							</div> --%>
						</c:if>
						
						<%--Customized Fields --%>
						<c:forEach var="updateDetail" items="${updateDetails}">
							<c:set var="title" value="${ updateDetail.title }"/>
							<c:set var="type" value="${ updateDetail.type }"/>
							<c:set var="field" value="${ updateDetail.field }"/>
							<c:if test="${not empty item }">
								<c:choose>
								    <c:when test="${fn:contains(field, '.')}">
								       <c:set var="fields" value="${fn:split(field, '.')}"/>
								       <c:set var="value" value="${item[fields[0]][fields[1]] }" />
								    </c:when>
								    <c:otherwise>
								        <c:set var="value" value="${item[field] }" />
								    </c:otherwise>
								</c:choose>
							</c:if>
							<c:if test="${type == 'password' and empty id }">
							<div class="form-group">
								<label class="col-sm-3 col-md-3 col-lg-2 control-label">${title}</label>
								<div class="col-sm-9 col-md-9 col-lg-10">
									<input id="password" type="password" name="${field}" class="form-control input-sm" required placeholder="${title }"
									 data-original-title="${title }" value="${value}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-3 col-lg-2 control-label">Re-type ${title}</label>
								<div class="col-sm-9 col-md-9 col-lg-10">	
									 <input type="password" equalto="#password" name="new_${field}" class="form-control input-sm" required placeholder="Re-type ${title }"
									 data-original-title="${title }" value="${value}">
								</div>
							</div>
							</c:if>
							<c:if test="${type != 'password'}">
								<div class="form-group">
									<label class="col-sm-3 col-md-3 col-lg-2 control-label">${title}</label>
									<div class="col-sm-9 col-md-9 col-lg-10">
										<c:if test="${type == 'text' }">
											<input type="text" name="${field}" class="form-control input-sm" required placeholder="${title }"
											 data-original-title="${title }" value="${value}">
										</c:if>
										
										<c:if test="${type == 'textarea' }">
											<textarea rows="5" name="${field}" class="form-control">${value}</textarea>
										</c:if>
										<c:if test="${type == 'date' }">
											<div class="input-group input-group-sm datepicker" id="datepicker_${field}">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" name="${field}" value="<fmt:formatDate pattern="yyyy/MM/dd" value="${value}"/>" class="form-control">
											</div>
										</c:if>
										<c:if test="${type == 'datetime' }">
											<div class="input-group input-group-sm datetimepicker" id="datetimepicker_${field}">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" name="${field}" value="<fmt:formatDate pattern="yyyy/MM/dd HH:mm:ss" value="${value}"/>" class="form-control">
											</div>
										</c:if>
										<c:if test="${type == 'image' }">
											<input type="hidden" name="${field}" value="${value}"/>
											<input type="hidden" name="uploadFields" value="${field }"/>
											<c:if test="${not empty value }"><img width="200px" src ="${url}${uploadSubDirectory}${value}"/></c:if>
											<input type="file" name="multipartFiles"/>
										</c:if>
										<c:if test="${type == 'reference' }">
											<select name="${field }" class="form-control">
											<option value=""> Any</option>
											<c:forEach var="reference" items="${updateDetail.referenceMap}">
												<option value="${reference.key }" <c:if test="${reference.key eq value}">selected</c:if>>${reference.value }</option>
											</c:forEach>
											</select>
										</c:if>
										<c:if test="${type == 'roles' }">
											<form:select path="item.roles" items="${roleList}" itemLabel="name" itemValue="id" multiple="true" cssClass="form-control" />
										</c:if>
										<c:if test="${type =='status' }">
											<select id="${field }" name="status" class="form-control">
												<option value="0" <c:if test="${'0' eq value}">selected</c:if>><spring:message code="GLOBAL.ACTIVE"/></option>
												<option value="1" <c:if test="${'1' eq value}">selected</c:if>><spring:message code="GLOBAL.INACTIVE"/></option>
											</select>
										</c:if>
									</div>
								</div>
							</c:if>
						</c:forEach>
					</div>
					<c:if test="${ isMultipleTable }">
						<c:if test="${not empty updateContentDetails }">
							<c:if test="${not empty global['support.languages'] }">
								<c:set var="supportLanguages" value="${fn:split( global['support.languages'], ',')}" />
								<c:forEach var="lang" items="${supportLanguages }" varStatus="loop">
									<div id="tab_${lang}" class="tab-pane">
										<%--  Hidden Value --%>
										<form:hidden path="item.contents[${loop.index}].id" value="${item.contents[loop.index]['id'] }"/>
										<form:hidden path="item.contents[${loop.index}].locale" value="${lang}"/>
										<c:forEach var="updateContentDetail" items="${updateContentDetails}" >
											<c:set var="title" value="${ updateContentDetail.title }"/>
											<c:set var="type" value="${ updateContentDetail.type }"/>
											<c:set var="field" value="${ updateContentDetail.field }"/>
											
											<c:if test="${not empty item }">
												<c:set var="value" value="${item.contents[loop.index][field] }" />
											</c:if>
											<div class="form-group">
												<label class="col-sm-3 col-md-3 col-lg-2 control-label">${title}</label>
												<div class="col-sm-9 col-md-9 col-lg-10 ">
													<c:if test="${type == 'text' }">
														<form:input path="item.contents[${loop.index}].${ field }" class="form-control input-sm required"   placeholder="${title }"/>
													</c:if>
													<c:if test="${type == 'textarea' }">
														<form:textarea path="item.contents[${loop.index}].${ field }" rows="5" class="form-control"/>
													</c:if> 
													<c:if test="${type == 'image' }">
														<form:hidden path="item.contents[${loop.index}].${field}" value="${value}"/>
														<input type="hidden" name="uploadFields" value="${loop.index }_contents_${field }"/>
														<c:if test="${not empty value }"><img width="200px" src ="${url}${uploadSubDirectory}${value}"/></c:if>
														<input type="file" name="multipartFiles"/>
													</c:if>
												</div>
											</div>
										</c:forEach>
									</div>
								</c:forEach>
							</c:if>
						</c:if>
					</c:if>
					<div class="form-actions">
						<a data-pjax href="${appPath}${listPath}" class="btn btn-default btn-small" data-dismiss="modal"><spring:message code="GLOBAL.CANCEL"/></a>
						<button type="submit" class="btn btn-primary btn-small"><spring:message code="GLOBAL.SAVE"/></button>
					</div>
				</div>
			</form>
			
		</div>
	</div>
</div>
