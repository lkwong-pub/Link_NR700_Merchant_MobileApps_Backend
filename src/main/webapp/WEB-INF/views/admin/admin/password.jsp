<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>
<!-- BEGIN PAGE HEADER-->
<div id="content-header">
	<h1><spring:message code="ADMIN.TITLE"/></h1>
</div>
<div id="breadcrumb">
	<a data-pjax href="${indexPath}" title="<spring:message code="GLOBAL.HOME"/>" class="tip-bottom"><i class="fa fa-home"></i><spring:message code="GLOBAL.HOME"/></a>
	<a data-pjax href="${appPath}${listPath}" title="<spring:message code="ADMIN.TITLE"/>" class="tip-bottom"><spring:message code="ADMIN.TITLE"/></a>
	<a data-pjax href=""  class="current"><spring:message code="ADMIN.CHANGEPASSWORD"/></a>
</div>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-xs-12">
		
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"> <i class="fa fa-pencil-square-o"></i>
				</span>
					<ul class="nav nav-tabs">
						<li class="active"><a class="contentTab" data-toggle="tab" href="#tab1">Main</a></li>
					</ul>
			</div>
			<form data-pjax id="demoForm" class="form-horizontal" method="POST" action="${appPath}${updatePath}${id}" onsubmit="return ajaxSubmitCallback(this)" enctype="multipart/form-data" novalidate>
				<div class="widget-content nopadding tab-content">
		
					<div id="tab1" class="tab-pane active">
						<div class="form-group">
								<label class="col-sm-3 col-md-3 col-lg-2 control-label">Password</label>
								<div class="col-sm-9 col-md-9 col-lg-10">
									<input id="password" type="password" name="password" class="form-control input-sm" required placeholder="Password"
									 data-original-title="Password">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 col-md-3 col-lg-2 control-label">Re-type Password</label>
								<div class="col-sm-9 col-md-9 col-lg-10">	
									 <input type="password" equalto="#password" name="new_password" class="form-control input-sm" required placeholder="Re-type Password"
									 data-original-title="Password">
								</div>
							</div>
					</div>
					<div class="form-actions">
						<a data-pjax href="${appPath}${listPath}" class="btn btn-default btn-small" data-dismiss="modal"><spring:message code="GLOBAL.CANCEL"/></a>
						<button type="submit" class="btn btn-primary btn-small"><spring:message code="GLOBAL.SAVE"/></button>
					</div>
				</div>
			</form>
			
		</div>
	</div>
</div>
