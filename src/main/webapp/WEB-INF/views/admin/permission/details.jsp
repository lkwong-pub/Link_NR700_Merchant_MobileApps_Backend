<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>
<script>
$(function(){
	$('.datepicker').each(function(){
		$('#'+$(this).attr('id')).datetimepicker({
		format: 'YYYY/MM/DD'
		});	
	});
	$('.datetimepicker').each(function(){
		$('#'+$(this).attr('id')).datetimepicker({
		format: 'YYYY/MM/DD hh:mm:ss'
		});	
	});
	
});
</script>
<!-- BEGIN PAGE HEADER-->
<div id="content-header">
	<h1><spring:message code="PERMISSION.TITLE"/></h1>
	<%-- <div class="btn-group">
		<a class="btn btn-large" title="<spring:message code="BMFUSER.ADDTITLE"/>" data-pjax href="${appPath}${updatePath}" target="dialog"><i class="fa fa-plus"></i></a>		
	</div> --%>
</div>
<div id="breadcrumb">
	<a data-pjax href="${indexPath}" title="<spring:message code="GLOBAL.HOME"/>" class="tip-bottom"><i class="fa fa-home"></i><spring:message code="GLOBAL.HOME"/></a>
	<a data-pjax href="${appPath}${listPath}" title="<spring:message code="PERMISSION.TITLE"/>" class="tip-bottom"><spring:message code="PERMISSION.TITLE"/></a>
	<a data-pjax href=""  class="current"><c:if test="${id == '0' }"><spring:message code="PERMISSION.ADD"/></c:if><c:if test="${id != '0' }"><spring:message code="PERMISSION.UPDATE"/></c:if></a>
</div>
<!-- END PAGE HEADER-->

<div class="row">
	<div class="col-xs-12">
		
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"> <i class="fa fa-pencil-square-o"></i>
				</span>
					<ul class="nav nav-tabs">
						<li class="active"><a class="contentTab" data-toggle="tab" href="#tab1">Main</a></li>
						<c:if test="${ isMultipleTable }">
							<c:if test="${not empty global['support.languages'] }">
								<c:set var="supportLanguages" value="${fn:split( global['support.languages'], ',')}" />
								<c:forEach var="lang" items="${supportLanguages }">
									<li><a class="contentTab" data-toggle="tab" href="#tab_${ lang }"><spring:message code="${ lang }"></spring:message></a></li>
								</c:forEach>
							</c:if>
						</c:if>
					</ul>
			</div>
			<form data-pjax id="demoForm" class="form-horizontal" method="${ method }" action="${appPath}${updatePath}${id}" onsubmit="return ajaxSubmitCallback(this)" enctype="multipart/form-data" novalidate>
				<div class="widget-content nopadding tab-content">
		
					<div id="tab1" class="tab-pane active">
						<c:forEach var="updateDetail" items="${updateDetails}">
							<c:set var="title" value="${ updateDetail.title }"/>
							<c:set var="type" value="${ updateDetail.type }"/>
							<c:set var="field" value="${ updateDetail.field }"/>
							<c:if test="${not empty item }">
								<c:set var="value" value="${item[field] }" />
							</c:if>
							<div class="form-group">
								<label class="col-sm-3 col-md-3 col-lg-2 control-label">${title}</label>
								<div class="col-sm-9 col-md-9 col-lg-10">
									<c:if test="${type == 'text' }">
										<input type="text" name="${field}" class="form-control input-sm" required placeholder="${title }"
										 data-original-title="${title }" value="${value}">
									</c:if>
									<c:if test="${type == 'textarea' }">
										<textarea rows="5" name="${field}" class="form-control">${value}</textarea>
									</c:if>
									<c:if test="${type == 'date' }">
										<div class="input-group input-group-sm datepicker" id="datepicker_${field}">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											<input type="text" name="${field}" value="<fmt:formatDate pattern="yyyy/MM/dd" value="${value}"/>" class="form-control">
										</div>
									</c:if>
									<c:if test="${type == 'datetime' }">
										<div class="input-group input-group-sm datetimepicker" id="datetimepicker_${field}">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											<input type="text" name="${field}" value="<fmt:formatDate pattern="yyyy/MM/dd HH:mm:ss" value="${value}"/>" class="form-control">
										</div>
									</c:if>
									<c:if test="${type == 'image' }">
										<input type="hidden" name="${field}" value="${value}"/>
										<input type="hidden" name="uploadFields" value="${field }"/>
										<c:if test="${not empty value }"><img width="200px" src ="${url}${uploadSubDirectory}${value}"/></c:if>
										<input type="file" name="multipartFiles"/>
									</c:if>
									<c:if test="${type == 'reference' }">
										<select name="${field }" class="form-control">
										<option value=""> Any</option>
										<c:forEach var="reference" items="${updateDetail.referenceMap}">
											<option value="${reference.key }" <c:if test="${reference.key eq value}">selected</c:if>>${reference.value }</option>
										</c:forEach>
										</select>
									</c:if>
									<c:if test="${type =='status' }">
										<select id="${field }" name="status" class="form-control">
											<option value="0" <c:if test="${'0' eq value}">selected</c:if>><spring:message code="GLOBAL.ACTIVE"/></option>
											<option value="1" <c:if test="${'1' eq value}">selected</c:if>><spring:message code="GLOBAL.INACTIVE"/></option>
										</select>
									</c:if>
								</div>
							</div>
						</c:forEach>
					</div>
					<c:if test="${ isMultipleTable }">
						<c:if test="${not empty updateContentDetails }">
							<c:if test="${not empty global['support.languages'] }">
								<c:set var="supportLanguages" value="${fn:split( global['support.languages'], ',')}" />
								<c:forEach var="lang" items="${supportLanguages }" varStatus="loop">
									<div id="tab_${lang}" class="tab-pane">
										<%--  Hidden Value --%>
										<form:hidden path="item.contents[${loop.index}].id" value="${item.contents[loop.index]['id'] }"/>
										<form:hidden path="item.contents[${loop.index}].locale" value="${lang}"/>
										<c:forEach var="updateContentDetail" items="${updateContentDetails}" >
											<c:set var="title" value="${ updateContentDetail.title }"/>
											<c:set var="type" value="${ updateContentDetail.type }"/>
											<c:set var="field" value="${ updateContentDetail.field }"/>
											
											<c:if test="${not empty item }">
												<c:set var="value" value="${item.contents[loop.index][field] }" />
											</c:if>
											<div class="form-group">
												<label class="col-sm-3 col-md-3 col-lg-2 control-label">${title}</label>
												<div class="col-sm-9 col-md-9 col-lg-10 ">
													<c:if test="${type == 'text' }">
														<form:input path="item.contents[${loop.index}].${ field }" class="form-control input-sm required"   placeholder="${title }"/>
													</c:if>
													<c:if test="${type == 'textarea' }">
														<form:textarea path="item.contents[${loop.index}].${ field }" rows="5" class="form-control"/>
													</c:if> 
													<c:if test="${type == 'image' }">
														<form:hidden path="item.contents[${loop.index}].${field}" value="${value}"/>
														<input type="hidden" name="uploadFields" value="${loop.index }_contents_${field }"/>
														<c:if test="${not empty value }"><img width="200px" src ="${url}${uploadSubDirectory}${value}"/></c:if>
														<input type="file" name="multipartFiles"/>
													</c:if>
												</div>
											</div>
										</c:forEach>
									</div>
								</c:forEach>
							</c:if>
						</c:if>
					</c:if>
					<div class="form-actions">
						<a data-pjax href="${appPath}${listPath}" class="btn btn-default btn-small" data-dismiss="modal"><spring:message code="GLOBAL.CANCEL"/></a>
						<button type="submit" class="btn btn-primary btn-small"><spring:message code="GLOBAL.SAVE"/></button>
					</div>
				</div>
			</form>
			
		</div>
	</div>
</div>
