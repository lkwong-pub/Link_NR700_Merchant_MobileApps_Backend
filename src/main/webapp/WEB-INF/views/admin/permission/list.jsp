<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>
<script>
$(function(){
	$('.datepicker').each(function(){
		$('#'+$(this).attr('id')).datetimepicker({
		format: 'YYYY/MM/DD'
		});	
	});
	$('.datetimepicker').each(function(){
		$('#'+$(this).attr('id')).datetimepicker({
		format: 'YYYY/MM/DD hh:mm:ss'
		});	
	});
});
</script>	
<!-- BEGIN PAGE HEADER-->
<div id="content-header">
	<h1><spring:message code="PERMISSION.TITLE"/></h1>
	<div class="btn-group">
		<a class="btn btn-large" data-pjax title="<spring:message code="PERMISSION.ADD"/>"  href="${appPath}${updatePath}"><i class="fa fa-plus"></i></a>		
	</div>
</div>
<div id="breadcrumb">
	<a data-pjax href="${indexPath }" title="<spring:message code="GLOBAL.HOME"/>" class="tip-bottom"><i class="fa fa-home"></i><spring:message code="GLOBAL.HOME"/></a>
	<a data-pjax href=""  class="current"><spring:message code="PERMISSION.TITLE"/></a>
</div>
<!-- END PAGE HEADER-->

<form class="form-horizontal" data-pjax id="pagerForm" method="get" action="${appPath}${listPath}" onsubmit="return false">
	<input type="hidden" id="_pageNum" name="page" value="${pageList.number + 1}" />
	<input type="hidden" id="_pageSize" name="size" value="${pageList.size}" />
	<c:if test="${isSearchable }">
		<div class="col-xs-12">
			<div class="widget-box collapsible">
				<div class="widget-title">
					<a href="#collapseOne" data-toggle="collapse" class="collapsed">
						<span class="icon"><i class="fa fa-th-list"></i></span>
						<h5>Search</h5>
					</a>
				</div>
				<div class="collapse" id="collapseOne">
					<c:forEach var="searchDetail" items="${searchDetails}">
						<c:set var="type" value="${searchDetail.type }"/>
						<c:set var="field" value="${searchDetail.field }"/>
						<c:set var="title" value="${searchDetail.title }"/>
						<c:set var="object" value="${param }"/>
						<c:set var="value" >${object[field] }</c:set>
						<div class="widget-content">
							<div class="row">
								<label class="search-label col-md-1"><spring:message code="${title }"/></label>
								
								<c:if test="${type == 'text' }">
									<div class="col-md-3">
										<input type="text" name="${field}" class="form-control input-sm" placeholder="${title }"
									 	data-original-title="${title }" value="${value}">
									</div>
								</c:if>
								<c:if test="${type == 'date' }">
									<div class="col-md-3">
										<div class="input-group input-group-sm datepicker" id="datepicker_${field}">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											<input type="text" name="${field}" value="${value}" class="form-control">
										</div>
									</div>
								</c:if>
								<c:if test="${type == 'dateRange' }">
									<div class="col-md-3">
										<c:set var="fieldStart" >${ field }_start</c:set>
										<c:set var="valueStart" >${object[fieldStart] }</c:set>
										<div class="input-group input-group-sm datepicker" id="datepicker_${fieldStart}">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											<input type="text" name="${fieldStart}" value="${valueStart}" class="form-control">
										</div> 
									</div> 
									<div class="col-md-1">to</div>
									<div class="col-md-3">
										<c:set var="fieldEnd" >${ field }_end</c:set>
										<c:set var="valueEnd" >${object[fieldEnd] }</c:set>
										<div class="input-group input-group-sm datepicker" id="datepicker_${fieldEnd}">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											<input type="text" name="${fieldEnd}" value="${valueEnd}" class="form-control">
										</div>
									</div>
								</c:if>
								<c:if test="${type == 'int' }">
									<div class="col-md-3">
										<input type="text" name="${field}" class="form-control input-sm" placeholder="${title }"
									 	data-original-title="${title }" value="${value}">
									</div>
								</c:if>
								<c:if test="${type == 'intRange' }">
									<div class="col-md-3">
										<c:set var="fieldStart" >${ field }_start</c:set>
										<c:set var="valueStart" >${object[fieldStart] }</c:set>
										<input type="text" name="${fieldStart}" class="form-control input-sm" placeholder="${title }"
										 data-original-title="${title }" value="${valueStart}"> 
										</div>
									<div class="col-md-1">
									 to
									</div>
									<div class="col-md-3">
										<c:set var="fieldEnd" >${ field }_end</c:set>
										<c:set var="valueEnd" >${object[fieldEnd] }</c:set>
										<input type="text" name="${fieldEnd}" class="form-control input-sm" placeholder="${title }"
										 data-original-title="${title }" value="${valueEnd}">
									</div>
								</c:if>
								<c:if test="${type == 'reference' }">
									<div class="col-md-3">
										<select name="${field }" class="form-control required">
										<option value="">Any</option>
										<c:forEach var="reference" items="${searchDetail.referenceMap}">
											<option value="${reference.key }" <c:if test="${reference.key eq value}">selected</c:if>>${reference.value }</option>
										</c:forEach>
										</select>
									</div>
								</c:if>
								<c:if test="${type =='status' }">
									<div class="col-md-3">
										<select name="status" class="form-control">
											<option value="" >Any</option>
											<option value="0" <c:if test="${'0' eq param.status}">selected</c:if>><spring:message code="GLOBAL.ACTIVE"/></option>
											<option value="1" <c:if test="${'1' eq param.status}">selected</c:if>><spring:message code="GLOBAL.INACTIVE"/></option>
										</select>
									</div>
								</c:if>
							</div>
						</div>
					</c:forEach>	
					<div class="widget-content">	
						<div class="row">
							<button type="submit" class="btn btn-dark-green btn-sm"><spring:message code="GLOBAL.SEARCH"/></button>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</c:if>
</form>

<!-- post wrapper -->
<div class="row">
	<div class="col-xs-12">
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon">
				<i class="fa fa-th"></i>
				</span>
				<h5><spring:message code="PERMISSION.TITLE"/></h5>
			</div>
			<div class="widget-content nopadding">
				<table class="table table-bordered table-striped table-hover data-table">
					<thead>
					<tr>
						<c:forEach var="listDetail" items="${listDetails}">
								<th><spring:message code="${listDetail.title}"/></th>
						</c:forEach>
						<th><spring:message code="GLOBAL.ACTION"/></th>
					</tr>
					</thead>
					<tbody align="center">
						<c:forEach var="item" items="${pageList.content}">
							<tr> 
								<c:forEach var="listDetail" items="${listDetails}">
									<c:set var="type" value="${listDetail.type }"/>
									<c:if test="${type == 'multipleText' }">
										<c:set var="fieldDetails" value="${fn:split( listDetail.field, '.')}" />
										<c:set var="value" value="${item[fieldDetails[0]][0][fieldDetails[1]]}"/>
									</c:if>
									<c:if test="${type != 'multipleText' }">
										<c:set var="value" value="${item[listDetail.field]}"/>
									</c:if>
									
									<td>
									<c:if test="${not empty value }">
										<c:if test="${type =='status' }">
											<c:choose>
												<c:when test="${item.status eq 0}">
													<c:set var="changeStatus">1</c:set>
													<c:set var="message"><spring:message code="GLOBAL.STATUS.INACTIVE"/></c:set>
													<c:set var="style">btn-success</c:set>
													<c:set var="status"><spring:message code="GLOBAL.ACTIVE"/></c:set>
												</c:when>
												<c:when test="${item.status eq 1}">
													<c:set var="changeStatus">0</c:set>
													<c:set var="message"><spring:message code="GLOBAL.STATUS.ACTIVE"/></c:set>
													<c:set var="style">btn-warning</c:set>
													<c:set var="status"><spring:message code="GLOBAL.INACTIVE"/></c:set>
												</c:when>
												<c:otherwise>
													<c:set var="changeStatus">0</c:set>
													<c:set var="message"><spring:message code="GLOBAL.STATUS.ACTIVE"/></c:set>
													<c:set var="style">btn-danger</c:set>
													<c:set var="status">Unknown Status</c:set>	
												</c:otherwise>
											</c:choose>
											<a href="${appPath}${statusPath}${item.id}?status=${changeStatus}" class="btn ${style} btn-xs" target="ajaxTodo" method="POST" title="${message}">${status }</a>
										</c:if>
										<c:if test="${type == 'text' }">${value}</c:if>
										<c:if test="${type == 'multipleText' }">${value}</c:if>
										
										<c:if test="${type == 'date' }"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${value}"/></c:if>
										<c:if test="${type == 'datetime' }"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${value}"/></c:if>
										<c:if test="${type == 'image' }"><img width="200px" src="${url}${uploadSubDirectory}${value}"/></c:if>
										<c:if test="${type == 'reference' }">
											<c:set var="referenceMap" value="${listDetail.referenceMap}"/>
											<c:set var="value" >${item[listDetail.field]}</c:set>
											${referenceMap[value]}	
										</c:if>
									</c:if>
									</td>
								</c:forEach>
								<td>
									<div class="btn-group">
										<a class="btn btn-warning btn-sm" data-pjax title="<spring:message code="PERMISSION.UPDATE"/>" href="${appPath}${updatePath}${item.id}"><i class="fa fa-edit"></i></a>
										<a class="btn btn-danger btn-sm" target="ajaxTodo" method="DELETE" title="<spring:message code="PERMISSION.DELETE"/>" href="${appPath}${deletePath}${item.id}"><i class="fa fa-trash-o"></i></a>
									</div>
								</td>
							</tr>	
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
			
		<!-- ./ post wrapper -->
		<c:import url="/WEB-INF/views/admin/common/pageBar.jsp"></c:import> 
	</div>
</div>
