<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/static/js/fileHandling/ajaxfileupload.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/fileHandling/HttpRequest2.js"></script>
<script>
    $(function(){
        $('.datepicker').each(function(){
            $('#'+$(this).attr('id')).datetimepicker({
                format: 'YYYY/MM/DD'
            });
        });
        $('.datetimepicker').each(function(){
            $('#'+$(this).attr('id')).datetimepicker({
                format: 'YYYY/MM/DD hh:mm:ss'
            });
        });
    });
</script>
<!-- BEGIN PAGE HEADER-->
<div id="content-header">
    <h1><spring:message code="IMPORT_DATA.TITLE"/></h1>
    <div class="btn-group">
        <sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_GIFT.ADD')">
            <a class="btn btn-large" data-pjax title="<spring:message code="IMPORT_DATA.ADD"/>"  href="${appPath}${updatePath}"><i class="fa fa-plus"></i></a>
        </sec:authorize>
    </div>
</div>
<div id="breadcrumb">
    <a data-pjax href="${indexPath }" title="<spring:message code="GLOBAL.HOME"/>" class="tip-bottom"><i class="fa fa-home"></i><spring:message code="GLOBAL.HOME"/></a>
    <a data-pjax href=""  class="current"><spring:message code="IMPORT_DATA.TITLE"/></a>
</div>
<!-- END PAGE HEADER-->

<div class="row">
    <div class="col-xs-12">

        <div class="widget-box">
            <div class="widget-title">
				<span class="icon"> <i class="fa fa-pencil-square-o"></i>
				</span>
                <ul class="nav nav-tabs">
                    <li class="active"><a class="contentTab" data-toggle="tab" href="#tab1">Main</a></li>
                    <c:if test="${ isMultipleTable }">
                        <c:if test="${not empty global['support.languages'] }">
                            <c:set var="supportLanguages" value="${fn:split( global['support.languages'], ',')}" />
                            <c:forEach var="lang" items="${supportLanguages }">
                                <li><a class="contentTab" data-toggle="tab" href="#tab_${ lang }"><spring:message code="${ lang }"></spring:message></a></li>
                            </c:forEach>
                        </c:if>
                    </c:if>
                </ul>
            </div>
            <form data-pjax id="demoForm" class="form-horizontal" method="POST" action="${appPath}${submitPath}" onsubmit="return ajaxSubmitCallback(this)" enctype="multipart/form-data" novalidate>
                <div class="widget-content nopadding tab-content">

                    <div id="tab1" class="tab-pane active">
                        <c:forEach var="updateDetail" items="${updateDetails}">
                            <c:if test="${updateDetail.isMandatory == true}">
                                <c:set var="required" value="required"/>
                            </c:if>
                            <c:if test="${not empty id and updateDetail.isEditable == false}">
                                <c:set var="disabled" value="disabled"/>
                            </c:if>
                            <c:set var="title" value="${ updateDetail.title }"/>
                            <c:set var="type" value="${ updateDetail.type }"/>
                            <c:set var="field" value="${ updateDetail.field }"/>
                            <c:if test="${not empty item }">
                                <c:choose>
                                    <c:when test="${fn:contains(field, '.')}">
                                        <c:set var="fields" value="${fn:split(field, '.')}"/>
                                        <c:set var="value" value="${item[fields[0]][fields[1]] }" />
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="value" value="${item[field] }" />
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-lg-2 control-label">${title}</label>
                                <div class="col-sm-9 col-md-9 col-lg-10">
                                    <c:if test="${type == 'text' }">
                                        <input type="text" name="${field}" class="form-control input-sm ${required}" required placeholder="${title }" data-original-title="${title }" value="${value}" ${disabled}>
                                    </c:if>
                                    <c:if test="${type == 'textarea' }">
                                        <textarea rows="5" name="${field}" class="form-control ${required}" ${disabled}>${value}</textarea>
                                    </c:if>
                                    <c:if test="${type == 'date' }">
                                        <div class="input-group input-group-sm datepicker" id="datepicker_${field}">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" name="${field}" value="<fmt:formatDate pattern="yyyy/MM/dd" value="${value}"/>" class="form-control ${required}" ${disabled}>
                                        </div>
                                    </c:if>
                                    <c:if test="${type == 'datetime' }">
                                        <div class="input-group input-group-sm datetimepicker" id="datetimepicker_${field}">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" name="${field}" value="<fmt:formatDate pattern="yyyy/MM/dd HH:mm:ss" value="${value}"/>" class="form-control ${required}" ${disabled}>
                                        </div>
                                    </c:if>
                                    <c:if test="${type == 'image' or type == 'file'}">
                                        <%--<input type="hidden" name="${field}" value="${value}"/>--%>
                                        <%--<input type="hidden" name="uploadFields" value="${field }"/>--%>
                                        <%--<c:if test="${not empty value }"><img width="200px" src ="${url}${uploadSubDirectory}${value}"/></c:if>--%>
                                        <%--<input type="file" name="multipartFiles" class="${required}" ${disabled}/>--%>

                                        <input type="file" id="importFile" onchange="uploadFiles(this,'')" name="importFile" class="form-control "/>
                                        <input type="hidden" id="importFileId" name="importFileId" value="" />
                                        <div id="importFileProgressNumber"></div>
                                        <div id="importFileShow"></div>

                                    </c:if>
                                    <c:if test="${type == 'reference' }">
                                        <select name="${field }" class="form-control ${required}" ${disabled}>
                                            <option value=""> Any</option>
                                            <c:forEach var="reference" items="${updateDetail.referenceMap}">
                                                <option value="${reference.key }" <c:if test="${reference.key eq value}">selected</c:if>>${reference.value }</option>
                                            </c:forEach>
                                        </select>
                                    </c:if>
                                    <c:if test="${type =='status' }">
                                        <select id="${field }" name="status" class="form-control ${required}" ${disabled}>
                                            <option value="0" <c:if test="${'0' eq value}">selected</c:if>><spring:message code="GLOBAL.ACTIVE"/></option>
                                            <option value="1" <c:if test="${'1' eq value}">selected</c:if>><spring:message code="GLOBAL.INACTIVE"/></option>
                                        </select>
                                    </c:if>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <c:if test="${ isMultipleTable }">
                        <c:if test="${not empty updateContentDetails }">
                            <c:if test="${not empty global['support.languages'] }">
                                <c:set var="supportLanguages" value="${fn:split( global['support.languages'], ',')}" />
                                <c:forEach var="lang" items="${supportLanguages }" varStatus="loop">
                                    <div id="tab_${lang}" class="tab-pane">
                                            <%--  Hidden Value --%>
                                        <form:hidden path="item.contents[${loop.index}].id" value="${item.contents[loop.index]['id'] }"/>
                                        <form:hidden path="item.contents[${loop.index}].locale" value="${lang}"/>
                                        <c:forEach var="updateContentDetail" items="${updateContentDetails}" >
                                            <c:if test="${updateContentDetail.isMandatory == true}">
                                                <c:set var="required" value="required"/>
                                            </c:if>
                                            <c:if test="${not empty id and updateContentDetail.isEditable == false}">
                                                <c:set var="disabled" value="true"/>
                                            </c:if>
                                            <c:set var="title" value="${ updateContentDetail.title }"/>
                                            <c:set var="type" value="${ updateContentDetail.type }"/>
                                            <c:set var="field" value="${ updateContentDetail.field }"/>

                                            <c:if test="${not empty item }">
                                                <c:set var="value" value="${item.contents[loop.index][field] }" />
                                            </c:if>
                                            <div class="form-group">
                                                <label class="col-sm-3 col-md-3 col-lg-2 control-label">${title}</label>
                                                <div class="col-sm-9 col-md-9 col-lg-10 ">
                                                    <c:if test="${type == 'text' }">
                                                        <form:input path="item.contents[${loop.index}].${ field }" class="form-control input-sm ${required}" disabled="${disabled}" placeholder="${title }" />
                                                    </c:if>
                                                    <c:if test="${type == 'textarea' }">
                                                        <form:textarea path="item.contents[${loop.index}].${ field }" rows="5" class="form-control ${required}" disabled="${disabled}"/>
                                                    </c:if>
                                                    <c:if test="${type == 'image' or type == 'file'}">
                                                        <form:hidden path="item.contents[${loop.index}].${field}" value="${value}"/>
                                                        <input type="hidden" name="uploadFields" value="${loop.index }_contents_${field }"/>
                                                        <c:if test="${not empty value }"><img width="200px" src ="${url}${uploadSubDirectory}${value}"/></c:if>
                                                        <input type="file" name="multipartFiles" class="${required}" disabled=${disabled} />
                                                    </c:if>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </c:forEach>
                            </c:if>
                        </c:if>
                    </c:if>
                    <div class="form-actions">
                        <%--<a data-pjax href="${appPath}${listPath}" class="btn btn-default btn-small" data-dismiss="modal"><spring:message code="GLOBAL.CANCEL"/></a>--%>
                        <button type="submit" class="btn btn-primary btn-small"><spring:message code="GLOBAL.SAVE"/></button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<!-- post wrapper -->
<form class="form-horizontal" data-pjax id="pagerForm" method="get" action="${appPath}${listPath}" onsubmit="return false">
    <input type="hidden" id="_pageNum" name="page" value="${pageList.number + 1}" />
    <input type="hidden" id="_pageSize" name="size" value="${pageList.size}" />
</form>
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-title">
				<span class="icon">
				<i class="fa fa-th"></i>
				</span>
                <h5><spring:message code="IMPORT_BATCH.TITLE"/></h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered table-striped table-hover data-table">
                    <thead>
                    <tr>
                        <c:forEach var="listDetail" items="${listDetails}">
                            <th><spring:message code="${listDetail.title}"/></th>
                        </c:forEach>
                        <th><spring:message code="GLOBAL.ACTION"/></th>
                    </tr>
                    </thead>
                    <tbody align="center">
                    <c:forEach var="item" items="${pageList.content}" varStatus="contentLoop">
                        <tr>
                            <c:forEach var="listDetail" items="${listDetails}">
                                <c:set var="value" value = "" />
                                <c:set var="type" value="${listDetail.type }"/>

                                <c:if test="${type == 'custom' }">
                                    <c:set var="fieldDetails" value="${fn:split( listDetail.field, '.')}" />
                                    <%--<c:set var="value" value="${item[fieldDetails[0]][0][fieldDetails[1]]}"/>--%>

                                    <c:set var="value" value="${item[fieldDetails[0]]}"/>
                                    <%--<c:forEach var="listItem" items="" varStatus="i">--%>
                                        <%--<c:set var="value" value="${ value }${ item[fieldDetails[0]][i.index][fieldDetails[1]]}"/>--%>
                                    <%--</c:forEach>--%>

                                </c:if>
                                <c:if test="${type == 'multipleText' }">
                                    <c:set var="value" value="${item[fieldDetails[0]][0][fieldDetails[1]]}"/>
                                </c:if>
                                <c:if test="${type != 'multipleText' && type != 'custom'}">
                                    <c:set var="value" value="${item[listDetail.field]}"/>
                                </c:if>

                                <%--<c:if test="${listDetail.title == 'Note' }">--%>
                                    <%--<c:set var="value" value="${item[listDetail.field]}"/>--%>
                                <%--</c:if>--%>

                                <td>
                                    <c:if test="${not empty value }">
                                        <c:if test="${type =='status' }">
                                            <c:choose>
                                                <c:when test="${item.status eq 0}">
                                                    <c:set var="style">btn-success</c:set>
                                                    <c:set var="status">Done</c:set>
                                                </c:when>
                                                <c:when test="${item.status eq 1}">
                                                    <c:set var="style">btn-warning</c:set>
                                                    <c:set var="status">Done - Warning</c:set>
                                                </c:when>
                                                <c:when test="${item.status eq 2}">
                                                    <c:set var="style">btn-warning</c:set>
                                                    <c:set var="status">Importing</c:set>
                                                </c:when>
                                                <c:when test="${item.status eq 3}">
                                                    <c:set var="style">btn-danger</c:set>
                                                    <c:set var="status">Failed</c:set>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:set var="style">btn-danger</c:set>
                                                    <c:set var="status">Unknown Status</c:set>
                                                </c:otherwise>
                                            </c:choose>
                                            <a class="btn ${style} btn-xs" >${ status }</a>
                                        </c:if>
                                        <c:if test="${type == 'text' }">${value}</c:if>
                                        <c:if test="${type == 'multipleText' }">${value}</c:if>

                                        <c:if test="${type == 'date' }"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${value}"/></c:if>
                                        <c:if test="${type == 'datetime' }"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${value}"/></c:if>
                                        <c:if test="${type == 'image' }"><img width="200px" src="${url}${uploadSubDirectory}${value}"/></c:if>
                                        <c:if test="${type == 'reference' }">
                                            <c:set var="referenceMap" value="${listDetail.referenceMap}"/>
                                            <c:set var="value" >${item[listDetail.field]}</c:set>
                                            ${referenceMap[value]}
                                        </c:if>

                                        <c:if test = "${ type == 'custom' }">
                                            <c:if test="${item.status ne 0}">
                                                <a class="btn btn-info btn-xs" href="#collapse${contentLoop.index}" data-toggle="collapse">Show</a>
                                                <div class="collapse" id="collapse${contentLoop.index}">
                                                    <table>
                                                        <c:forEach var="value" items="${item[fieldDetails[0]]}" varStatus="i">

                                                            <tr><td>${ item[fieldDetails[0]][i.index][fieldDetails[1]] }</td></tr>
                                                                <%--<ul>--%>
                                                                    <%--<li>${ item[fieldDetails[0]][i.index][fieldDetails[1]] }</li>--%>
                                                                <%--</ul>--%>
                                                            <%--<c:set var="value" value="${ value }${ item[fieldDetails[0]][i.index][fieldDetails[1]]}"/>--%>
                                                        </c:forEach>
                                                    </table>
                                                </div>
                                            </c:if>
                                        </c:if>
                                    </c:if>
                                </td>
                            </c:forEach>
                            <td>
                                <div class="btn-group">
                                    <%--<sec:authorize access="hasRole('ROLE_SUPERADMIN') or hasRole('PERMISSION_GIFT.DELETE')">--%>
                                        <a class="btn btn-info btn-sm" title="<spring:message code="REPORT.DOWNLOAD"/>" href="${appPath}${downloadPath}${item.fileId}"><i class="fa fa-download"></i></a>
                                    <%--</sec:authorize>--%>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- ./ post wrapper -->
        <c:import url="/WEB-INF/views/admin/common/pageBar.jsp"></c:import>
    </div>
</div>
<script>
    function uploadFiles(obj,num) {

        var fileId = obj.id;
        var fileName = obj.name;
        var divName ='';
        var hiddenFileRelId = '';
        var hiddenFileName='';
        var hiddenFilePath='';
        var progressNumber='';
        var fileShow='';

        if (fileName == 'importFile') {
            hiddenFileRelId='importFileId';
            progressNumber = 'importFileProgressNumber';
            fileShow = 'importFileShow';
        }

        if (!HttpRequest2.isSupport()) {
            console.log("Please use modern browsers which supported HTML5")
        } else {
            var uploadfiles = document.getElementById(fileId).files;
            if (uploadfiles.length == 0 || uploadfiles[0] == null) {
                console.log("Please choose a file.");
                /* document.getElementById('chooseFileError').innerHTML = "Please choose the file."; */
                $("#chooseFileError").show();
            } else {

                var req = new HttpRequest2(
                    {
                        isAutoSend : true,
                        method : 'POST',
                        url : '${pageContext.request.contextPath}${uploadFilePath}',
                        params : {
                            uploadfile : uploadfiles[0],
                        },

                        onProgress : function(theReq, evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded * 100 / evt.total).toFixed(2);
                                if (percentComplete > 0|| percentComplete < 100) {
                                    console.log("Uploading： " + percentComplete+ " %");
                                    document.getElementById(progressNumber+ num).innerHTML = "Upload completed : "+ percentComplete + " %, progressing...";
                                    $(".display").each(function() {
                                        $(this).attr("disabled", true);
                                    });
                                }
                                if (percentComplete == 100) {
                                    console.log("Upload Success： " + percentComplete+ " %");
                                    $(".display").each(function() {
                                        $(this).attr("disabled", true);
                                    });
                                }
                            }
                        },

                        onComplete : function(theReq, evt) {
                            console.log("complete");
                            var callres = eval("("+ theReq.xhr.responseText + ")");
                            console.log(callres);
                            document.getElementById(progressNumber + num).innerHTML = "Upload completed!";

                            /* var index='';
                            if(divName!='one'){
                                index = $('.form-group.' + divName + '').length;
                            } */
                            $("#" + hiddenFileRelId + num + "").val('');
                            $("#" + hiddenFileName + num + "").val(callres.name);
                            $("#" + hiddenFilePath + num + "").val(callres.path);
                            //

                            var result = '{"path":"'+callres.path+'","name":"'+callres.name+'","fileIs":"'+callres.fileId+'"}';

                            switch (fileName) {
                                case 'importFile':
                                    $("#importFileId").val(callres.fileId);
                                    break;
                            }

                            $("#" + progressNumber + num + "").hide();
                            document.getElementById(fileShow + num).innerHTML = "File: <a href='${basePath}${downloadPath}" + callres.fileId + "' >"+ callres.name + "</a>";

                            $(".display").each(function() {
                                $(this).attr("disabled", false);
                            });
                        },
                        onError : function(theReq, evt) {
                            console.log("error");
                        },
                        onAbort : function(theReq, evt) {
                            console.log("abort upload");
                        },
                        onStatusChange : function(theReq, status) {
                        }
                    });
                req2 = req;
            }
        }
    }
</script>