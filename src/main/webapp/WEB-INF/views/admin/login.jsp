<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><%@ 
	page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %><%@ 
	include file="/include.inc.jsp"
%><html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- Meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- End of Meta -->
	
	<title>${ projectName } Login</title>
	
	<!-- Libraries -->
	<link rel="stylesheet" type="text/css" href="${appPath}/static/unicorn/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="${appPath}/static/unicorn/css/font-awesome.css"/>		
	<link href="${appPath}/static/unicorn/css/unicorn-login.css" rel="stylesheet" type="text/css"/>
	<link href="${appPath}/static/css/login_custom.css" rel="stylesheet" type="text/css"/>	
	
	<script type="text/javascript" src="${appPath}/static/js/jquery/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="${appPath}/static/js/jquery/jquery-ui-1.10.4.custom.js"></script>
	<script type="text/javascript" src="${appPath}/static/js/jquery/jquery.validate.js"></script>		
	<script src="${appPath}/static/unicorn/js/respond.min.js" type="text/javascript"></script>
	<script src="${appPath}/static/unicorn/js/unicorn.login.js" type="text/javascript"></script>

</head>
<body class="login" onunload="">

	<div id="container">
		<div id="logo">
			<img src="${appPath}/static/images/logo.png" alt="" />
		</div>


		<div id="loginbox">
			<form id="loginform" method="post" action="${appPath}/admin/login">
				
				<c:choose>
					<c:when test="${empty error}"><p>Enter username and password to continue.</p></c:when>
					<c:when test="${not empty error}"><p>Login Failed.</p></c:when>
				</c:choose>
				<div class="input-group input-sm">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input class="form-control" type="text" id="username" name="username" placeholder="Username" />
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock"></i></span>
					<input class="form-control" type="password" id="password" name="password" placeholder="Password" />
				</div>
				<div class="form-actions clearfix">
					<input type="submit" class="btn btn-block btn-primary btn-default" value="Login" />
				</div>
			</form>
		</div>

	</div>
	
</body>
</html>