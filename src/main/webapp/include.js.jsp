
		<!-- Begin Base -->
		<script type="text/javascript" src="${appPath}/static/js/jquery/jquery-1.10.2.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/jquery/jquery-ui-1.10.4.custom.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/jquery/jquery.validate.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/jquery/jquery.dataTables.js"></script>
		
		<script type="text/javascript" src="${appPath}/static/unicorn/js/select2.js"></script>
		<script type="text/javascript" src="${appPath}/static/unicorn/js/jquery.sparkline.js"></script>
		<script type="text/javascript" src="${appPath}/static/unicorn/js/jquery.nicescroll.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/jquery/pagination/jquery.pagination.js"></script>
		
		<script type="text/javascript" src="${appPath}/static/unicorn/js/bootstrap.js"></script>
		<!--[if lte IE 8]>
		<script type="text/javascript" src="${appPath}/static/unicorn/js/bootstrap-ie.js"></script>
		<![endif]-->
		
		<script type="text/javascript" src="${appPath}/static/unicorn/js/unicorn.js"></script><%-- 
		<script type="text/javascript" src="${appPath}/static/unicorn/js/unicorn.dashboard.js"></script>
		<script type="text/javascript" src="${appPath}/static/unicorn/js/unicorn.tables.js"></script> --%>
		
		<script type="text/javascript" src="${appPath}/static/js/bootstrap-v3-datetimepicker/js/moment.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/bootstrap-v3-datetimepicker/js/bootstrap-datetimepicker.js"></script>
		
		<script type="text/javascript" src="${appPath}/static/js/bootbox/bootbox.min.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/jquery/toastr/toastr.js"></script>
		
		<script type="text/javascript" src="${appPath}/static/js/pjax/jquery.pjax.js"></script>
		
		<script type="text/javascript" src="${appPath}/static/js/nui/nui.core.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/nui/nui.dialog.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/nui/nui.ajax.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/nui/nui.ui.js"></script>
		
		
		<script type="text/javascript" src="${appPath}/static/js/jstree/dist/jstree.min.js"></script>
		<script type="text/javascript" src="${appPath}/static/js/ckeditor_standard/ckeditor.js"></script>
		
		
		<!-- End Base -->
		
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link rel="stylesheet" type="text/css" href="${appPath}/static/unicorn/css/bootstrap.css"/>
		<link rel="stylesheet" type="text/css" href="${appPath}/static/unicorn/css/font-awesome.css"/>

		<link rel="stylesheet" type="text/css" href="${appPath}/static/unicorn/css/fullcalendar.css"/>
		<link rel="stylesheet" type="text/css" href="${appPath}/static/unicorn/css/jquery.jscrollpane.css"/>
		<link rel="stylesheet" type="text/css" href="${appPath}/static/js/jquery/toastr/toastr.css"/>
		<link rel="stylesheet" type="text/css" href="${appPath}/static/unicorn/css/select2.css"/>
		<link rel="stylesheet" type="text/css" href="${appPath}/static/unicorn/css/unicorn.css"/>
		
		<link rel="stylesheet" type="text/css" href="${appPath}/static/js/bootstrap-v3-datetimepicker/css/bootstrap-datetimepicker.css"/>
		
		<link rel="stylesheet" type="text/css" href="${appPath}/static/css/core.css"/>
		<link rel="stylesheet" type="text/css" href="${appPath}/static/js/jquery/pagination/pagination.css"/>
		
		<link rel="stylesheet" href="${appPath}/static/js/jstree/dist/themes/default/style.min.css" />
		
		<link rel="stylesheet" type="text/css" href="${appPath}/static/css/baseui/baseui.css"/>
		<script type="text/javascript" src="${appPath}/static/js/baseui/baseui.js"></script>
		
		<!-- END GLOBAL MANDATORY STYLES -->
