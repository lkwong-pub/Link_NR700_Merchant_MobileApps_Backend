/**
 * 
 */

$(function(){
	NUI.init("${appPath}/login");
	
	initAjaxTodo();
	
	//Initial PJAX
	$(document).pjax('a[data-pjax]', '#content');

	//PageBar Handling
	$(document).on('submit', '#pagerForm', function(event) {
	  $.pjax.submit(event, '#content', {replace:false, push:false})
	});
	
	//SideBar Handling 
	$(document).on('pjax:complete', function() {
		loadInitPage();
		$(".progressBar").hide();
	});

	$(document).on('pjax:error', function() {
		loadInitPage();
		$(".progressBar").hide();
	});

	//Form Validation Handling
//	$("body").on("click", ".contentTab", function(event) {
//		var validator = $("form").validate();
//	    var valid = true;
//	    var $inputs = $(".tab-pane.active").find("input,select");
//	    
//	    $inputs.each(function() {
//	        if (!validator.element(this)) {
//	            valid = false;
//	        }
//	    });
//	    if (valid) {
//	        $("#tabs").tabs("select", this.hash);
//	    }
//	    else{
//	    	event.stopPropagation();
//	    	event.preventDefault();
//	    }
//	});
	
});

function initAjaxTodo(){
	$(document).on("click", "a[target=ajaxTodo]", function(){
		if($.fn.ajaxTodo){
			$(this).ajaxTodo();
		}
	});
}

function ajaxTodo(url, method, callback){
	var $callback = callback || NUI.ajaxDone;
	if (! $.isFunction($callback)) $callback = eval('(' + callback + ')');
	$.ajax({
		type: method || 'POST',
		url:url,
		dataType:"json",
		cache: false,
		success: $callback,
		error: NUI.ajaxError
	});
}

$.fn.extend({
	ajaxTodo:function(){
		return this.each(function(){
			var $this = $(this);
				var url = unescape($this.attr("href")).replaceTmById();				
				NUI.debug(url);
				if (!url.isFinishedTm()) {
					toastr["error"]($this.attr("warn") || NUI.msg("alertSelectMsg"),"Error");
					return false;
				}
				var title = $this.attr("title");
				var method = $this.attr("method");
				
				if (title) {
					bootbox.confirm(title, function(result) {
			            if(result){
			            	ajaxTodo(url, method, $this.attr("callback"));
			            }
			         });
				} else {
					ajaxTodo(url, method, $this.attr("callback"));
				}
				event.preventDefault();
		});
	},
	
});