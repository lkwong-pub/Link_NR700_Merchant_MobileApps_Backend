﻿$(document).ready(function() {
	//remove "click this to activate" in IE7
	$("object:not(.activated), embed:not(.activated)").each(function() {
		this.outerHTML = this.outerHTML;
	});
	
	configureTextAreaMaxLength();
	
    //setup styles
    setupStyles();
})


/*  cause memory leak in IE6
$(window).resize(function() {
    var iheight = (document.body.clientHeight > document.body.scrollHeight)?document.body.clientHeight:document.body.scrollHeight;
    $(".bgright").height(iheight-50);
    $(".rightcontent").height(iheight-51);
})
*/

function fixHeight() {
	$(".rightcontent").height($(document).height()-45);
}
function setupStyles() {
    if (location.href.indexOf("?print=1")<0) {
        $(".bgright").height($(document).height()-50);
        $(".rightcontent").height($(document).height()-45);
		setTimeout(fixHeight, 200);
    }

    //setup style for buttons
    $(".formbutton").each(function() {
        var sText = $(this).text();
        $(this).empty();
        $(this).append("<span class='head'><span class='tail'><span class='text'>" + sText + "</span></span></span>");
        $(this).hover(function(){
                $(this).find("span").each(function() {
                    $(this).css("background-image", $(this).css("background-image").replace(".gif", "_o.gif"));
                });
            }, 
            function(){
                $(this).find("span").each(function() {
                    $(this).css("background-image", $(this).css("background-image").replace("_o.gif", ".gif"));
                });
            }
        );
    })

    //setup style for menu tags
    $(".menu li a").not($(".htmlEditor .menu li a")).each(function() {
        var sText = $(this).text();
        $(this).empty();
        $(this).append("<span class='head'><span class='tail'><span class='text'>" + sText + "</span></span></span>");
    })
    
    //setup style for list tables
    $(".rightcontent table.list th").addClass("datahead");
    $(".rightcontent table.list tr:even:gt(0)").not($(".rightcontent table.list tr table tr")).addClass("datarow");
    
	//max. the height with td
    $("table.recorddetail td").each(function() {
    	$(this).find("input.textbox").height($(this).height());
    });
}

function configureTextAreaMaxLength(){
    $("textarea[jQueryMaxLength]").each(function() {
        var maxLength = $(this).attr("jQueryMaxLength");
        $(this)[0].onkeypress = function (){
            return this.value.length < maxLength;
        };
    });
}