﻿function FixWcmsUrl(inStrHTML){
    var strHTML = new String(inStrHTML);
/*    
    strHTML = strHTML.replace(/ src="..\//gi, ' src="/');
    strHTML = strHTML.replace(/ href="..\//gi, ' href="/');
    strHTML = strHTML.replace(/ data="..\//gi, ' data="/');
    strHTML = strHTML.replace(/ value="..\//gi, ' value="/');
*/
    return strHTML;
}

function HTMLDecode(inStrHTML){
    var strHTML = new String(inStrHTML);
    strHTML = strHTML.replace(/&#39;/gi, "'");
    strHTML = strHTML.replace(/&quot;/gi, '"');
    strHTML = strHTML.replace(/&gt;/gi, '>');
    strHTML = strHTML.replace(/&lt;/gi, '<');
    strHTML = strHTML.replace(/&amp;/gi, '&');
    return strHTML;
}

function setSubmitTarget(target){
    document.forms[0].target = target;
    setTimeout("document.forms[0].target='';", 0);
}

function setSubmitTargetAndURL(target,param){
    document.forms[0].target = target;
    var temp_action=document.forms[0].action;
    document.forms[0].action=param;
    setTimeout("document.forms[0].target='';document.forms[0].action='" + temp_action + "';", 0);
}


function Change_Year(FormYear,FormMonth,FormDay){
	var i;
	var year=FormYear;
	var month=FormMonth;
	var day = FormDay;
	var Date_count;
	var A_month = new Array(12);
	A_month[0] = "All"
	A_month[1] = "Jan"
	A_month[2] = "Feb"
	A_month[3] = "Mar"
	A_month[4] = "Apr"
	A_month[5] = "May"
	A_month[6] = "Jun"
	A_month[7] = "Jul"
	A_month[8] = "Aug"
	A_month[9] = "Sep"
	A_month[10] = "Oct"
	A_month[11] = "Nov"
	A_month[12] = "Dec"
    
	if (year[year.selectedIndex].value == 0){
		FormMonth.size = 1;
		FormMonth.options.length = 1;
		FormMonth.options[0].text = "All";
		FormMonth.options[0].value = 0;
		FormMonth.options[0].selected = true;

		FormDay.size = 1;
		FormDay.options.length = 1;
		FormDay.options[0].text = "All";
		FormDay.options[0].value = 0;
		FormDay.options[0].selected = true;
	
	}else{
		FormMonth.size = 1;
		FormMonth.options.length = 13;
		for(i=0;i<13;i++)
		{
			if (i == 0) {
			FormMonth.options[i].text = "All";
			}
			else{
			FormMonth.options[i].text = A_month[i];
			}
			FormMonth.options[i].value = i;
		}
		FormMonth.options[0].selected = true;

		FormDay.size = 1;
		FormDay.options.length = 1;
		FormDay.options[0].text = "All";
		FormDay.options[0].value = 0;
		FormDay.options[0].selected = true;
	}

}


function Change_Month(FormYear,FormMonth,FormDay){
	var i;
	var year=FormYear;
	var month=FormMonth;
	var day = FormDay;
	var Date_count

	if (month[month.selectedIndex].value == 0){
		FormDay.size = 1;
		FormDay.options.length = 1;
		FormDay.options[0].text = "All";
		FormDay.options[0].value = 0;
		FormDay.options[0].selected = true;
	}else{
	    if(year.selectedIndex == 0){
    		FormMonth.options[0].selected = true;
	        return;
	    }
	    
		Date_count = Choose_Dates(FormYear,FormMonth,FormDay);
		FormDay.size = 1;
		FormDay.options.length = Date_count + 1;
		for(i=0;i<Date_count + 1;i++)
		{
			if (i == 0) {
			FormDay.options[i].text = "All";
			}
			else{
			FormDay.options[i].text = i;
			}
			FormDay.options[i].value = i;
		}
		FormDay.options[0].selected = true;
	}

}

function Choose_Dates(FormYear,FormMonth,FormDay){
	var i;
	var year,month,day;
	var iyear,imonth,iday;
	var obj_year=FormYear;
	var obj_month=FormMonth;
	var obj_day = FormDay;
	var Feb_days;

	year = obj_year[obj_year.selectedIndex].value;
	month = obj_month[obj_month.selectedIndex].value;

	iyear=eval(year);
		  imonth=eval(month);

		  if ((iyear%4)==0){
			if ((iyear%100)==0){
				if ((iyear%400)==0){
				    Feb_days = 29;
			    }else{
				    Feb_days = 28;
			    }
			}else{
			    Feb_days = 29;
			}
		  }else{
			  Feb_days = 28;
		  }

	var A_dates = new Array(12);
	A_dates[0] = 0;
	A_dates[1] = 31;
	A_dates[2] = Feb_days;
	A_dates[3] = 31;
	A_dates[4] = 30;
	A_dates[5] = 31;
	A_dates[6] = 30;
	A_dates[7] = 31;
	A_dates[8] = 31;
	A_dates[9] = 30;
	A_dates[10] = 31;
	A_dates[11] = 30;
	A_dates[12] = 31;
	
	return A_dates[imonth];
}


function Check_Change_Day(FormYear,FormMonth,FormDay){
	var year=FormYear;
	var month=FormMonth;
	var day = FormDay;

    if(month.selectedIndex == 0){
   		FormDay.options[0].selected = true;
    }

}
