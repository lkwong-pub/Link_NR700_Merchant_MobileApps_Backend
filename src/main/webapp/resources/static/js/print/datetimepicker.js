﻿﻿Number.prototype.PendingZero = function(digit) {
	var result = this + '';
	for (_aaa = result.length; _aaa < digit; _aaa++)
		result = '0' + result;
	return result;
}

$(document).ready(function() {
	createCalendar();
	
	$("input.datetime").each(function() {
		var sFormat = $(this).attr("format");
		var oDate = new Date();
		if (sFormat == undefined || sFormat == "") {
			sFormat = "yyyy/MM/dd HH:mm:ss";
			$(this).attr("format", sFormat);
		}
		if ($(this).val() == "")
		    $(this).val(toFormatStr(oDate, sFormat));
		$(this).focus(function() {
			updateCalendar(this.id);
		});
		$(this).keyup(function() {
			updateCalendar(this.id);
		});
		$(this).blur(function() {
			if ($("#datetimepicker").attr("isOver") != "1")
				$("#datetimepicker").hide();
		});
	});
});

function createCalendar() {
	if ($("#datetimepicker").size() == 0) {
		$("<div id='datetimepicker'></div>").appendTo("body");
		$("#datetimepicker").hide();
	}
	var oPicker = $("#datetimepicker");
	oPicker.empty();

	var sHTML = '<select id="ddlMonth"></select> <select id="ddlYear"></select>';
	sHTML += '<table id="pickerDate" BORDER=0 CELLSPACING=0 cellpadding=0>';
	sHTML += '<tr>';
	sHTML += '<td class="WeekName Holiday">S</td>';
	sHTML += '<td class="WeekName">M</td>';
	sHTML += '<td class="WeekName">T</td>';
	sHTML += '<td class="WeekName">W</td>';
	sHTML += '<td class="WeekName">T</td>';
	sHTML += '<td class="WeekName">F</td>';
	sHTML += '<td class="WeekName">S</td>';
	sHTML += '</tr>';
	for (i = 0; i < 6; i++) {
		sHTML += '<tr>';
		for (j = 0; j < 7; j++) 
			sHTML += '<td class="emptycell">&nbsp;</td>';
		sHTML += '</tr>';
	}
	sHTML += '</table>';
	sHTML += '<table id="pickerTime" BORDER=0 CELLSPACING=0 cellpadding=0>';
	sHTML += '<tr>';
	sHTML += '<td><select id="ddlHour"></select></td>';
	sHTML += '<td>:</td>';
	sHTML += '<td><select id="ddlMinute"></select></td>';
	sHTML += '<td>:</td>';
	sHTML += '<td><select id="ddlSecond"></select></td>';
	sHTML += '</tr>';
	sHTML += '</table>';
	sHTML += '<div class="btn"><a href="#" class="btnUpdate">Update</a>&nbsp<a href="#" class="btnCancel">Cancel</a></div>';
	oPicker.append(sHTML);
	oPicker.find(".btnUpdate").click(function() {
		var oTextBox = $("#" + oPicker.attr("currTextboxID"));
		var sFormat = oTextBox.attr("format");
		var oDate = new Date;
		if ($("#pickerDate").css("display") != "none")
		{
			oDate.setFullYear(parseFloat($("#ddlYear").val()));
			oDate.setMonth(parseFloat($("#ddlMonth").val())-1);
			oDate.setDate(parseFloat($("#pickerDate .selectedDay").text()));
		}
		if ($("#pickerTime").css("display") != "none")
		{
			oDate.setHours(parseFloat($("#ddlHour").val()));
			oDate.setMinutes(parseFloat($("#ddlMinute").val()));
			oDate.setSeconds(parseFloat($("#ddlSecond").val()));
		}
		var sValue = toFormatStr(oDate, sFormat);
		oTextBox.val(sValue);
		oPicker.hide();
		return false;
	});
	oPicker.find(".btnCancel").click(function() {
		oPicker.hide();
		return false;
	});
	oPicker.hover(function() {
		$(this).attr("isOver", 1);
	}, function() {
		$(this).attr("isOver", 0);
	})
	$("#ddlYear, #ddlMonth").change(function() {
		updateCalendar();
	});
	$("#ddlHour, #ddlMinute, #ddlSecond").change(function() {
		updateTime();
	});
}

function updateTime() {
	var oPicker = $("#datetimepicker");
	oPicker.show();
	var inDate = new Date();
	inDate.setHours(parseFloat($("#ddlHour").val()));
	inDate.setMinutes(parseFloat($("#ddlMinute").val()));
	inDate.setSeconds(parseFloat($("#ddlSecond").val()));
	oPicker.attr("currDate", inDate);
}

//press the inTextBox.id only, or null
function updateCalendar(inTextBoxID) {
	var oPicker = $("#datetimepicker");
	var inDate = new Date();
	if (inTextBoxID != undefined && inTextBoxID != "")
	{
		var oTextBox = $("#" + inTextBoxID);
		var oldTextboxID = oPicker.attr("currTextboxID");
		oPicker.attr("currTextboxID", inTextBoxID);
		inDate = parseDate(oTextBox.val(), oTextBox.attr("format"));
		var iOffset = oTextBox.offset();
		oPicker.css("left", iOffset.left);
		oPicker.css("top", iOffset.top + oTextBox.height() + 2);
		if (oPicker.attr("currDate") == inDate.toString() && oldTextboxID == inTextBoxID) {
			oPicker.show();
			return;
		}
	}
	else {
		if ($("#pickerDate").css("display") != "none")
		{
			inDate.setFullYear(parseFloat($("#ddlYear").val()));
			inDate.setMonth(parseFloat($("#ddlMonth").val())-1);
	
			var iLastDate = new Date(inDate.getFullYear(), inDate.getMonth() + 1, 0);
			iLastDate = iLastDate.getDate();
			var selectedDay = parseFloat($("#pickerDate .selectedDay").text());
			if (selectedDay > iLastDate) selectedDay = iLastDate;
	
			inDate.setDate(selectedDay);
		}
		if ($("#pickerTime").css("display") != "none")
		{
			inDate.setHours(parseFloat($("#ddlHour").val()));
			inDate.setMinutes(parseFloat($("#ddlMinute").val()));
			inDate.setSeconds(parseFloat($("#ddlSecond").val()));
		}
	}
	
	var oTextBox = $("#" + oPicker.attr("currTextboxID"));
	var bHaveCalendar = /[yyyy|MM|dd]/g.test(oTextBox.attr("format"));
	var bHaveTime = /[HH|mm|ss]/g.test(oTextBox.attr("format"));
	bHaveCalendar?$("#pickerDate, #ddlMonth, #ddlYear").show():$("#pickerDate, #ddlMonth, #ddlYear").hide();
	bHaveTime?$("#pickerTime").show():$("#pickerTime").hide();

	oPicker.attr("currDate", inDate);

	var iYear = inDate.getFullYear();
	var iMonth = inDate.getMonth();
	var iDate = inDate.getDate();
	
	if (bHaveCalendar)
	{
		var sHTML = '<tr>';
		sHTML += '<td class="WeekName Holiday">S</td>';
		sHTML += '<td class="WeekName">M</td>';
		sHTML += '<td class="WeekName">T</td>';
		sHTML += '<td class="WeekName">W</td>';
		sHTML += '<td class="WeekName">T</td>';
		sHTML += '<td class="WeekName">F</td>';
		sHTML += '<td class="WeekName">S</td>';
		sHTML += '</tr>';
	
		var iFirstDate = new Date(iYear, iMonth, 1).getDay();
		var iLastDate = new Date(iYear, iMonth + 1, 0);
		iLastDate = iLastDate.getDate();
		var iDayCounter = 0;
		var iTotalCounter = 0;
		for (i = 0; i < 6 && iDayCounter < iLastDate; i++) {
			sHTML += '<tr>';
			for (j = 0; j < 7; j++) {
				if (iTotalCounter >= iFirstDate && iDayCounter < iLastDate)
					sHTML += '<td class="daycell' + ((j==0)?' Holiday':'') + ((++iDayCounter == iDate)?' selectedDay':'') + '">' + iDayCounter + '</td>';
				else
					sHTML += '<td class="emptycell' + ((j==0)?' Holiday':'') + '">&nbsp;</td>';
				iTotalCounter++;
			}
			sHTML += '</tr>';
		}
		$("#pickerDate").html(sHTML);
	
		setDropDownList("ddlYear", 1900, 2100, iYear, 4);
		setDropDownList("ddlMonth", 1, 12, iMonth+1, 2);
	}
	
	if (bHaveTime)
	{
		setDropDownList("ddlHour", 0, 23, inDate.getHours(), 2);
		setDropDownList("ddlMinute", 0, 59, inDate.getMinutes(), 2);
		setDropDownList("ddlSecond", 0, 59, inDate.getSeconds(), 2);
	}

	oPicker.show();

	$("#pickerDate .daycell").click(function() {
		$("#pickerDate .daycell").removeClass("selectedDay");
		$(this).addClass("selectedDay");
		return false;
	});
	$("#pickerDate .daycell").hover(function() {
		$(this).addClass("highlighted");
	}, function() {
		$(this).removeClass("highlighted");
	});
}

function setDropDownList(sID, iFrom, iTo, iCurr, iPending) {
	var oTarget = $("#" + sID);
	if (oTarget.html() == '') {
		var sHTML = '';
		for(i = iFrom; i <= iTo; i++)
			sHTML += '<option value ="' + i + '" ' + (i==iCurr?'selected':'') + '>' + i.PendingZero(iPending) + '</option>';
		oTarget.html(sHTML);
	}
	oTarget.val(iCurr.PendingZero(iPending) + "");
}

function parseDate(str, format) {
	var dDate = new Date();
	var iYear = toParse(str, format, "yyyy");
	if (!isNaN(iYear)) dDate.setFullYear(iYear);
	var iMonth = toParse(str, format, "MM");
	if (!isNaN(iMonth)) dDate.setMonth(iMonth-1);
	var iDay = toParse(str, format, "dd");
	if (!isNaN(iDay)) dDate.setDate(iDay);
	var iHour = toParse(str, format, "HH");
	if (!isNaN(iHour)) dDate.setHours(iHour);
	var iMinute = toParse(str, format, "mm");
	if (!isNaN(iMinute)) dDate.setMinutes(iMinute);
	var iSecond = toParse(str, format, "ss");
	if (!isNaN(iSecond)) dDate.setSeconds(iSecond);
	if (isNaN(dDate)) dDate = new Date();
	return dDate;
}

function toParse(str, format, arg) {
	var idx = format.indexOf(arg);
	if (idx != -1) {
		return parseFloat(str.substr(idx, arg.length));
	}
	return NaN;
}

function toFormatStr(inDate, sFormat) {
	if (inDate == undefined || isNaN(inDate)) inDate = new Date;
	if (sFormat == undefined || sFormat == "") sFormat = "yyyy/MM/dd HH:mm:ss";
	var iYear = inDate.getFullYear();
	var iMonth = inDate.getMonth() + 1;
	var iDay = inDate.getDate();
	var iHour = inDate.getHours();
	var iMinute = inDate.getMinutes();
	var iSecond = inDate.getSeconds();
	return sFormat.replace(/yyyy/, iYear.PendingZero(4))
		.replace(/MM/, iMonth.PendingZero(2))
		.replace(/dd/, iDay.PendingZero(2))
		.replace(/HH/, iHour.PendingZero(2))
		.replace(/mm/, iMinute.PendingZero(2))
		.replace(/ss/, iSecond.PendingZero(2));
}
