/**
 * @author Lannerk
 * 2016-10-18 IN GT
 */
HttpRequest2 = function(config){
	var me = this;
	me.url = config.url;
	me.method = config.method||'POST';
	me.params = config.params;
	me.isAutoSend = config.isAutoSend||false;
	me.onProgress = config.onProgress;
	me.onComplete = config.onComplete;
	me.onError = config.onError;
	me.onAbort = config.onAbort;
	me.onStatusChange = config.onStatusChange;
	
	me.status = 0;
	
	me.xhr = new XMLHttpRequest();
	me.formData = new FormData();

	if (me.params!=null) {
		for (var key in me.params) {
			me.formData.append(key, me.params[key]);
		}			
	}
	me.xhr.upload.addEventListener("progress", function(evt){

        console.log(me.status);
		if(me.status!=1){
			me.status =1;
			if(me.onStatusChange!=null){
				me.onStatusChange(me,me.status)
			}
		}
		if(me.onProgress!=null){
			me.onProgress(me,evt)
		}
	},false);
	me.xhr.addEventListener("load", function(evt){
		if(me.status!=2){
			me.status =2;
			if(me.onStatusChange!=null){
				me.onStatusChange(me,me.status)
			}
		}
		if(me.onComplete!=null){
			me.onComplete(me,evt)
		}
	},false);
	me.xhr.addEventListener("error", function(evt){
		if(me.status!=-2){
			me.status =-2;
			if(me.onStatusChange!=null){
				me.onStatusChange(me,me.status)
			}
		}
		if(me.onError!=null){
			me.onError(me,evt)
		}
	},false);
	me.xhr.addEventListener("abort", function(evt){
		if(me.status!=-1){
			me.status =-1;
			if(me.onStatusChange!=null){
				me.onStatusChange(me,me.status)
			}
		}
		if(me.onAbort!=null){
			me.onAbort(me,evt)
		}
	},false);
	


	me.send=function(){
		if(this.status!=0){
			this.status =0;
			if(this.onStatusChange!=null){
				this.onStatusChange(this,this.status)
			}
		}
		this.xhr.open(this.method, this.url);
		this.xhr.send(this.formData); 		
	};
	
	me.abort=function(){
		this.xhr.abort();
	};
	
	if(me.isAutoSend){
		me.send();
	}
}


HttpRequest2.isSupport=function(){
	if(window.FormData){
		return true;
	}
	return false;
}







/**
 * @author Lannerk
 
Ext.define('Eglib.tools.EGXMLHttpRequest2', {
    requires:[
               'Ext.util.Observable'
           ],
    mixins:{
               observable: 'Ext.util.Observable'
    },  
	alternateClassName: 'EGXMLHttpRequest2',
	url:null,
	method:'POST',
	params:null,
	isAutoSend:false,
	constructor: function (config) {	
		var me = this;
		me.mixins.observable.constructor.call(this, config);   
		this.callParent(arguments);
		me.addEvents('progress');
		me.addEvents('complate');
		me.addEvents('error');
		me.addEvents('abort');
		me.addEvents('statusChange');
		me.status = 0;
		me.xhr = new XMLHttpRequest();
		me.formData = new FormData();
		if(me.params!=null){
			for(var key in me.params){
				me.formData.append(key, me.params[key]);
			}			
		}
		me.xhr.upload.addEventListener("progress", function(evt){
			if(me.status!=1){
				me.status =1;
				me.fireEvent('statusChange',me,me.status);
			}
			me.fireEvent('progress',me,evt);
		},false);
		me.xhr.addEventListener("load", function(evt){
			if(me.status!=2){
				me.status =2;
				me.fireEvent('statusChange',me,me.status);
			}
			me.fireEvent('complate',me,evt);
		},false);
		me.xhr.addEventListener("error", function(evt){
			if(me.status!=-2){
				me.status =-2;
				me.fireEvent('statusChange',me,me.status);
			}
			me.fireEvent('error',me,evt);
		},false);
		me.xhr.addEventListener("abort", function(evt){
			if(me.status!=-1){
				me.status =-1;
				me.fireEvent('statusChange',me,me.status);
			}
			me.fireEvent('abort',me,evt);
		},false);
		
		if(me.isAutoSend){
			me.send();
		}

	},
	
	send:function(){
		var me = this;
		if(me.status!=0){
			me.status =0;
			me.fireEvent('statusChange',me,me.status);
		}
		me.xhr.open(me.method, me.url);
		this.xhr.send(this.formData); 		
	},
	
	abort:function(){
		this.xhr.abort();
	},
	
	statics: {
		isSupport:function(){
			if(window.FormData){
				return true;
			}
			return false;
		}
	}
 });

*/